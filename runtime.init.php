<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-25 20:40
	* @file:		Runtime loader
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	RUNTIME LOADER


	namespace WebCore;


	// Define path to the root of project
	define( 'WEBCORE',	__DIR__ );


	// Load WebCore library from the system core to manage runtime
	include WEBCORE."/library/webcore.lib.php";


	// Register autoload aliases
	WebCore::Alias( 'webcore', WEBCORE."/library" );
	WebCore::Alias( "assetic", WEBCORE."/library/assetic" );
	WebCore::Alias( "fxmlrpc", WEBCORE."/library/fxmlrpc" );
	WebCore::Alias( "tracy", WEBCORE."/library/tracy" );

	// Preload most basic classess to the enviroment
	WebCore::Using("WebCore/Debug/Debug");
	WebCore::Using("WebCore/Debug/Profiler");
	WebCore::Using("WebCore/Security/Shield");
	WebCore::Using("WebCore/User/UAC");
	WebCore::Using("WebCore/User/Visitor");
	WebCore::Using("WebCore/Web/Smarty/Smarty");


	// Start core profiling
	Debug\Profiler::Start(0);


	// If we could not register WebCore services
	if ( !WebCore::Register( ) )
	{
		throw new \Exception("Failed to preload WebCore services");
	}


	// Create instance of the visitor
	$visitor = User\Visitor::Factory( );

	// If the visitor is not authorized to visit this page
	if ( !Security\Shield::Authorize( $visitor ) )
	{
		// Lockdown the page access
		Security\Shield::Lockdown( );
	}


	Debug\Profiler::Tick(0, 'Runtime loaded');


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

