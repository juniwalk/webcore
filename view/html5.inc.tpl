{* Smarty *}
{*

	Author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	Contact:	info@dpoint.cz
	Created:	2012-06-13 18:36
	File:		WebCore > Layout template
	Copyright:	(c) 2013 Design Point & Web7

	// HTML5 Layout

*}<!DOCTYPE HTML>
<html dir="{$lang->X('LANG_DIR')}" lang="{$lang->X('LANG_CODE')}">
<head>

	<!-- website -->
	<meta charset="{$lang->X('LANG_CHARSET')}" />
	<meta http-equiv="Content-Type" content="text/html;charset={$lang->X('LANG_CHARSET')}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- application -->
	<meta name="contact" content="{$config->APP_CONTACT}" />
	<meta name="author" content="{$config->APP_AUTHOR}" />
	<meta name="title" content="{$site->title} :: {$page->title}{if isset($view)} &raquo; {$view->title}{/if}" />
	<meta name="keywords" content="{$site->keywords}" />
	<meta name="description" content="{$site->description}" />
	<meta name="robots" content="index, follow" />
	<meta name="generator" content="WebCore" />

	<!-- title -->
	<title>{if $site->debug}[debug] {/if}{$site->title} :: {$page->title}{if isset($view)} &raquo; {$view->title}{/if}</title>

{block name="head"}


	<!--
	// INFO

		Here will come HTML header
		of this site using extending

	\\
	-->


{/block}

</head>
<body class="ux-{$page->alias}{if isset($view)}-{$view->alias}{/if}">

<!-- UI :: Wrapper -->
<section class="ui-wrapper">
{block name="body"}
{block name="content"}


	<!--
	// INFO

		Here will come HTML content
		of this site using extending

	\\
	-->


{/block}
{/block}
</section>
<!-- /Wrapper -->

</body>
</html>