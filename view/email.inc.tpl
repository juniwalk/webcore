{* Smarty *}
{*

	Author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	Contact:	info@dpoint.cz
	Created:	2014-03-05 14:03
	File:		WebCore > Layout template
	Copyright:	(c) 2014 Design Point & Web7

	// HTML5 Email Layout

*}<!DOCTYPE HTML>
<html dir="{$lang->X('LANG_DIR')}" lang="{$lang->X('LANG_CODE')}">
<head>

	<!-- website -->
	<meta charset="{$lang->X('LANG_CHARSET')}" />
	<meta http-equiv="Content-Type" content="text/html;charset={$lang->X('LANG_CHARSET')}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- title -->
	<title>{$subject}</title>

{block name="head"}


	<!--
	// INFO

		Here will come HTML header
		of this site using extending

	\\
	-->


{/block}

</head>
<body>

<!-- UI :: Wrapper -->
<section class="ui-wrapper">
{block name="body"}
{block name="content"}


	<!--
	// INFO

		Here will come HTML content
		of this site using extending

	\\
	-->


{/block}
{/block}
</section>
<!-- /Wrapper -->

</body>
</html>