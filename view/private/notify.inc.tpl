{* Smarty *}
{*

	Author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	Contact:	info@dpoint.cz
	Created:	2012-06-13 18:36
	File:		WebCore > Notify template
	Copyright:	(c) 2012 Design Point & Web7

*}
{nocache}
{if $model->notify( )}
	<!-- UI :: Notify -->
	<div class="ui-notify {$model->notify( true )}">
		<div class="ui-icon"></div>
		<div class="ui-text" title="{$model->notify( )}">{$model->notify( )}</div>
	</div>
	<!-- /Notify -->
{/if}
{/nocache}