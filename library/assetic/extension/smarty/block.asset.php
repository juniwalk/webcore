<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-6-22 9:29
	* @file:		Asset plugin
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ASSET PLUGIN


	use \WebCore\Web\Assets;


	/**
	 *
	 * Asset entry block
	 *
	 * @param	array	$params		List of parameters
	 * @param	string	$content	Content of the block
	 * @param	object	$template	Internal template
	 * @param	bool	$opening	Is this the opening tag?
	 * @return	string
	 * @see		smarty_block_asset
	**/
	function smarty_block_asset ( array $params, $content, $template, &$opening )
	{
		// Default parameters
		static $default = array
		(
			'type'		=> null,	// Asset type
			'src'		=> null,	// Source path
		);


		// Compile list of parameters by merging default values
		// with the values provided from the method call from Smarty
		$params = (object) array_merge( $default, $params );

		// handle only on closing tag
		if ( $opening == true )
		{
			return null;
		}


		// Access internal Asset manager
		$assets = Assets::Factory( );

		// Switch the type of the asset
		switch ( strToLower( $params->type ) )
		{
			// Standard assets
			case Assets::FILE:
			case Assets::HTTP:
			case Assets::GLOB:

				// If the source is empty
				if ( empty( $params->src ) )
				{
					return null;
				}


				// Insert the asset as is into holder
				$assets->add( $params->type, $params->src );

			break;

			// Asset from the content
			case Assets::CONTENT:

				// Clear the white space from content
				$content = trim( $content );

				// If there is no content
				if ( empty( $content ) )
				{
					return null;
				}


				// Save the asset into the holder
				$assets->add( $params->type, $content );

			break;

			// Bundle of Assets
			case Assets::BUNDLE:

				throw new \Exception("Inserting bundles is not implemented yet");

			break;

			// Unknown asset type
			default:

				throw new \Exception("Unknown asset type '{$params->type}'");

			break;
		}


		// Do not print anything
		return null;
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

