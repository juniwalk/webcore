<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-6-22 9:29
	* @file:		Assets plugin
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ASSETS PLUGIN


	use \WebCore\Web\Assets;


	/**
	 *
	 * Asset management block
	 *
	 * @param	array	$params		List of parameters
	 * @param	string	$content	Content of the block
	 * @param	object	$template	Internal template
	 * @param	bool	$opening	Is this the opening tag?
	 * @return	string
	 * @see		smarty_block_assets
	**/
	function smarty_block_assets ( array $params, $content, $template, &$opening )
	{
		// Default parameters
		static $default = array
		(
			'type'		=> null,		// Output type
			'output'	=> "./cache",	// Output folder
			'filters'	=> [ ],			// List of filters
			'debug'		=> true,		// Debugging mode
		);


		// Compile list of parameters by merging default values
		// with the values provided from the method call from Smarty
		$params = (object) array_merge( $default, $params );

		// Chekc fi the given type of assets is supported
		if ( !in_array( $params->type, [ 'css', 'js' ] ) )
		{
			throw new \Exception("Unknown asset type '{$params->type}'");
		}


		// Access internal Asset manager
		$assets = Assets::Factory( $opening );
		$assets->setType( $params->type );

		// If the debug should be enabled
		if ( $params->debug )
		{
			// Turn it on for the package
			$assets->setDebug( true );
		}

		// If we are not closing the tag yet
		if ( $opening == true )
		{
			return null;
		}


		// Compile assets and get final path
		$asset = $assets->compile
		(
			$params->output,	// Build path
			$params->filters	// List of filters
		);

		// Switch the type of Asset
		switch ( $params->type )
		{
			// Stylesheet
			case "css":

				$asset = "<link href=\"{$asset}\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />";

			break;

			// Javascript
			case "js":

				$asset = "<script src=\"{$asset}\" type=\"text/javascript\"></script>";

			break;
		}


		// Print compiled Asset
		return $asset;
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

