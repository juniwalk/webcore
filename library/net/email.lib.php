<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-03-05 12:59
	* @file:		Email library
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	EMAIL LIBRARY


	namespace WebCore\Net;


	use \WebApp;
	use \WebCore\Core\Exceptions;
	use \WebCore\Security;
	use \WebCore\Web;


	final class Email
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Properties
		**/
		protected $driver	= null;
		protected $template	= null;



		/**
		 *
		 * Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		protected function __construct ( )
		{
			// If the PHPMailer Autoloader is not included
			if ( !function_exists("PHPMailerAutoload") )
			{
				// Include it to the runtime so we can work with PHP Mailer
				include_once __DIR__."/phpmailer/PHPMailerAutoload.php";
			}


			// Create instance of the driver
			$this->driver = new \PHPMailer( true );
			$this->driver->CharSet = 'utf-8';
			$this->driver->WordWrap = 100;	// Set word wrap to 100 characters
			$this->driver->IsHTML( true );	// Set email format to HTML
		}


		/**
		 *
		 * Prepare new email
		 *
		 * @param	string	$subject	Message subject
		 * @param	string	$template	Path to template
		 * @return	self
		 * @see		Factory
		**/
		public static function Factory ( $subject, $template )
		{
			// Access needed object instances
			$lang = Web\Lang::Factory( );
			$site = Web\Site::Factory( );
			$menu = Web\Menu\Menu::Factory( );
			$config = WebApp\Config::Factory( );

			// If the subject is not a string
			if ( !is_string( $subject ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $subject, 1 );
			}

			// If the subject is not a string
			if ( !is_string( $template ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $template, 2 );
			}

			// If the subject has been translated
			if ( $lang->translated( $subject ) )
			{
				// Get the translation of the subject
				$subject = $lang->X( $subject );
			}


			// Prepare instance
			$email = new self( );

			// If we were unable to load the template
			if ( !$email->template( $template ) )
			{
				throw new \Exeption("Unable to access email template");
			}


			// Set the email subject to the object
			$email->driver->Subject = $subject;
			$email->assign( 'lang', $lang );
			$email->assign( 'site', $site );
			$email->assign( 'subject', $subject );
			$email->assign( 'config', $config );
			$email->assign( 'menu', $menu );

			return $email;
		}



		/**
		 *
		 * Set the From header
		 *
		 * @param	string	$email	Email address
		 * @param	string	$name	User name
		 * @return	self
		 * @see		from
		**/
		public function from ( $email, $name = null )
		{
			// Assign email and name as a From header
			$this->driver->From = $email;
			$this->driver->FromName = $name;

			return $this;
		}


		/**
		 *
		 * Add the To header
		 *
		 * @param	string	$email	Email address
		 * @param	string	$name	User name
		 * @return	self
		 * @see		to
		**/
		public function to ( $email, $name = null )
		{
			// Insert the To address to the driver object
			$this->driver->AddAddress( $email, $name );

			return $this;
		}


		/**
		 *
		 * Add the ReplyTo header
		 *
		 * @param	string	$email	Email address
		 * @param	string	$name	User name
		 * @return	self
		 * @see		reply
		**/
		public function reply ( $email, $name = null )
		{
			// Insert the ReplyTo address to the driver object
			$this->driver->AddReplyTo( $email, $name );

			return $this;
		}


		/**
		 *
		 * Add attachment
		 *
		 * @param	string	$file	Path to the file
		 * @param	string	$name	File name
		 * @return	self
		 * @see		attachment
		**/
		public function attachment ( $file, $name = null, $type = null )
		{
			// Default attachment method
			$method = "AddAttachment";

			// If the source is not file
			if ( @ !is_file( $file ) )
			{
				// Use method to create attachment from string
				$method = "addStringAttachment";
			}


			// Insert email Attachment to the driver object
			$this->driver->$method( $file, $name, 'base64', $type );

			return $this;
		}


		/**
		 *
		 * Add embed image
		 *
		 * @param	string	$file	Path to the file
		 * @param	string	$cid	Content Id
		 * @param	string	$name	File name
		 * @return	self
		 * @see		image
		**/
		public function image ( $file, $cid, $name = null )
		{
			$this->driver->addEmbeddedImage( $file, $cid, $name );

			return $this;
		}


		/**
		 *
		 * Assign new value
		 *
		 * @param	string	$key	Value name
		 * @param	mixed	$value	Value contents
		 * @return	self
		 * @see		assign
		**/
		public function assign ( $key, $value )
		{
			// Assign new value to the template object
			$this->template->assign( $key, $value );

			return $this;
		}



		/**
		 *
		 * Send the email
		 *
		 * @param	void
		 * @return	bool
		 * @see		send
		**/
		public function send ( )
		{
			// Fetch compiled template html string
			$html = $this->template->fetch( );

			// If the template is not string
			if ( !is_string( $html ) )
			{
				return false;
			}


			// Assign template string
			$this->driver->Body = $html;

			// Call the SEND method of the driver
			return $this->driver->send( );
		}



		/**
		 *
		 * Preview the email
		 *
		 * @param	void
		 * @return	bool
		 * @see		preview
		**/
		public function preview ( )
		{
			// Fetch compiled template html string
			return $this->template->fetch( );
		}


		/**
		 *
		 * Get the email error
		 *
		 * @param	void
		 * @return	string
		 * @see		error
		**/
		public function error ( )
		{
			return $this->driver->ErrorInfo;
		}


		/**
		 *
		 * Access the driver
		 *
		 * @param	void
		 * @return	PHPMailer
		 * @see		driver
		**/
		public function &driver ( )
		{
			return $this->driver;
		}



		/**
		 *
		 * SMTP Authentication
		 *
		 * @param	Security\Auth	$auth	Authenticator
		 * @return	bool
		 * @see		smtp
		**/
		public function smtp ( Security\Auth $auth )
		{
			// If the authenticator is invalid
			if ( !$auth->validate( ) )
			{
				return false;
			}


			// Enable sending through SMTP server
			$this->driver->IsSMTP( );				// Set mailer to use SMTP
			$this->driver->SMTPSecure = $auth->scheme;	// Enable encryption, 'ssl' also accepted
			$this->driver->Host = $auth->host;			// Specify main and backup server
			$this->driver->Port = $auth->port;			//Set port on which we will comunicate

			// Authentication credentials for the SMTP server
			$this->driver->SMTPAuth = true;				// Enable SMTP authentication
			$this->driver->Username = $auth->user;		// SMTP username
			$this->driver->Password = $auth->pass;		// SMTP password

			return true;
		}


		/**
		 *
		 * Load template file
		 *
		 * @param	string	$tpl	Template name
		 * @return	bool
		 * @see		template
		**/
		protected function template ( $tpl )
		{
			// Access the smarty object
			$smarty = \Smarty::Factory( );

			// If the template is not a string
			if ( !is_string( $tpl ) )
			{
				return false;
			}


			// Create new template handler using the smarty object
			$this->template = $smarty->createTemplate( $tpl );

			// If the template is not an object
			if ( !is_object( $this->template ) )
			{
				return false;
			}


			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

