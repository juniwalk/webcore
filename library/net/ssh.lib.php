<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-9-9 13:10
	* @file:		SSH library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	SSH LIBRARY


	namespace WebCore\Net;


	use \WebCore\Core;
	use \WebCore\Core\Exceptions;
	use \WebCore\Security;


	final class SSH
	{
		/**
		 * Version holder
		**/
		const VERSION	= 1;


		/**
		 * Connection
		**/
		private $link	= null;
		private $sftp	= null;


		/**
		 * Properties
		**/
		private $auth	= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	Security\Auth	$auth	Auth Instance
		 * @return	void
		 * @throws	Exception
		 * @see		__construct
		**/
		public function __construct ( Security\Auth $auth )
		{
			// If the connection is invalid
			if ( !$auth->validate( ) )
			{
				throw new \Exception("{$this}: Invalid auth credentials for connector");
			}


			// Open connection to the SSH server using provided credentials
			$this->link = ssh2_connect( $auth->host, $auth->port );

			// If we have failed to connect
			if ( !is_resource( $this->link ) )
			{
				throw new \Exception("{$this}: Unable to connect to '{$auth->host}:{$auth->port}'");
			}


			// Try to authenticate with the server on created connection link
			$response = ssh2_auth_password( $this->link, $auth->user, $auth->pass );

			// If the auth has failed
			if ( $response == false )
			{
				throw new \Exception("{$this}: Authentication failed for user '{$auth->user}'");
			}


			// Establish sFTP connection through SSH
			$this->sftp = ssh2_sftp( $this->link );

			// Assign authenticator
			// to the class property
			$this->auth = $auth;
		}


		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			// Close connection if
			// there is any opened
			return $this->close( );
		}


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			return __CLASS__;
		}


		/**
		 *
		 * Are we connected?
		 *
		 * @param	void
		 * @return	bool
		 * @see		connected
		**/
		public function connected ( )
		{
			// If there is no SSH connection
			if ( !is_resource( $this->link ) )
			{
				return false;
			}

			// If there is no sFTP connection
			if ( !is_resource( $this->sftp ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Server GUID
		 *
		 * @param	void
		 * @return	string
		 * @see		guid
		**/
		public function guid ( )
		{
			// If there is no connection
			if ( !$this->connected( ) )
			{
				return null;
			}


			// Return MD5 finger print of the server
			$guid = ssh2_fingerprint( $this->link );
			$guid = Core\Crypto::GUID( $guid );

			return $guid;
		}


		/**
		 *
		 * Execute command
		 *
		 * @param	string	$cmd	Command to execute
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		execute
		**/
		public function execute ( $cmd )
		{
			// If the command is not string
			if ( !is_string( $cmd ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $cmd, 1 );
			}

			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Execute given command on the SSH
			return ssh2_exec( $this->link, $cmd );
		}



		/**
		 *
		 * Download file from server
		 *
		 * @param	string	$file	File to download
		 * @param	string	$path	Path of the file
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		download
		**/
		public function download ( $file, $path )
		{
			// If the file already exists
			if ( !file_exists( $path ) )
			{
				return false;
			}

			// If the filename is not string
			if ( !is_string( $file ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $file, 1 );
			}

			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Execute downlod command on the resource link
			return ssh2_scp_recv( $this->link, $file, $path );
		}


		/**
		 *
		 * Upload file to server
		 *
		 * @param	string	$file	File to upload
		 * @param	string	$path	Path of the file
		 * @param	int		$mode	Permissions
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		upload
		**/
		public function upload ( $file, $path, $mode = 0644 )
		{
			// If the file does not exists
			if ( !file_exists( $file ) )
			{
				return false;
			}

			// If the path is not string
			if ( !is_string( $path ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $path, 2 );
			}

			// If the mode is not integer
			if ( !is_integer( $mode ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'integer', $mode, 3 );
			}

			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Execute upload command on the resource link
			return ssh2_scp_send( $this->link, $file, $path, $mode );
		}



		/**
		 *
		 * Change file mode (permissions)
		 *
		 * @param	string	$file	Path to file
		 * @param	int		$mode	File permissions
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		chmod
		**/
		public function chmod ( $file, $mode = 0644 )
		{
			// If the filename is not string
			if ( !is_string( $file ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $file, 1 );
			}

			// If the mode is not integer
			if ( !is_integer( $mode ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'integer', $mode, 2 );
			}

			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Execute chmod command on the resource link
			return ssh2_sftp_chmod( $this->sftp, $file, $mode );
		}



		/**
		 *
		 * Closes SSH connection
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		public function close ( )
		{
			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}

			// If we are not connected
			if ( !ssh2_exec( $this->link, 'exit;' ) )
			{
				return false;
			}


			// Clear the link
			$this->link = null;

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

