<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2011-3-24 13:05
	* @file:		cURL library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	cURL LIBRARY


	namespace WebCore\Net;


	use \WebCore\Core\Exceptions;
	use \WebCore\IO\File;


	final class cURL
	{
		/**
		 * Version holder
		**/
		const VERSION		= 2;


		/**
		 * cURL handler
		**/
		private $link		= null;


		/**
		 * Debugger
		**/
		private $debug		= null;
		private $error		= array( );


		/**
		 * Request properties
		**/
		private $url		= null;
		private $post		= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	\WebCore\Net\URL	$url	cURL link
		 * @return	void
		 * @throws	Exception
		 * @see		__construct
		**/
		public function __construct ( URL &$url = null )
		{
			// Initialize cURL Session
			$this->link	= curl_init( );

			// If the initialization has failed
			if ( !$this->connected( ) )
			{
				throw new \Exception("Unable to initialite cURL Session");
			}


			// Disable debug defaultly
			$this->debug( false );

			// If there is no URL
			if ( !is_null( $url ) )
			{
				$this->url = clone $url;
			}


			// Set some default options for this cURL session
			$this->option( CURLOPT_RETURNTRANSFER, true );
		}


		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			$this->close( );
		}


		/**
		 *
		 * Get cURL Version
		 *
		 * @param	void
		 * @return	string
		 * @see		Version
		**/
		public static function Version ( )
		{
			// Access the data from cURL
			$data = curl_version( );

			// If there are no data
			if ( empty( $data ) )
			{
				return false;
			}


			// Return version number
			return $data['version'];
		}


		/**
		 *
		 * Get cURL Protocols
		 *
		 * @param	void
		 * @return	array
		 * @see		Protocols
		**/
		public static function Protocols ( )
		{
			// Access the data from cURL
			$data = curl_version( );

			// If there are no data
			if ( empty( $data ) )
			{
				return false;
			}


			// Return list of protocols
			return $data['protocols'];
		}



		/**
		 *
		 * Is cURL initialized?
		 *
		 * @param	void
		 * @return	bool
		 * @see		connected
		**/
		public function connected ( )
		{
			return is_resource( $this->link );
		}


		/**
		 *
		 * Debugging
		 *
		 * @param	bool	$value	Debug state
		 * @return	bool
		 * @see		debug
		**/
		public function debug ( $value )
		{
			// Assign debug state
			$this->debug = $value;

			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Set debugging options to true so we can debug
			$this->option( CURLOPT_HEADER,		$value );
			$this->option( CURLOPT_VERBOSE,		$value );
			$this->option( CURLINFO_HEADER_OUT,	$value );

			return true;
		}


		/**
		 *
		 * Set new option
		 *
		 * @param	int		$key	cURL key
		 * @param	mixed	$value	cURL value for key
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		option
		**/
		public function option ( $key, $value )
		{
			// If option key is not integer
			if ( !is_integer( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'integer', $key, 1 );
			}

			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}

			// If the value is instance of the URL Class
			if ( $value instanceof URL )
			{
				// Save the Url to the holder
				return $this->url( $value );
			}


			// Set new cURL option to the connection link
			$response = curl_setopt( $this->link, $key, $value );

			// If setting option has failed
			if ( $response === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Assign URL instance
		 *
		 * @param	URL		$value	URL Instance
		 * @return	bool
		 * @see		url
		**/
		public function url ( URL $value )
		{
			if ( false )
			{
				return false;
			}


			$this->url = clone $value;

			return true;
		}



		/**
		 *
		 * Add GET parameter
		 *
		 * @param	string	$key	GET key name
		 * @param	mixed	$value	Parameter value
		 * @return	void
		 * @throws	InvalidArgument
		 * @see		GET
		**/
		public function GET ( $key, $value )
		{
			// Insert GET parameter to the URL
			$this->url->set( $key, $value );
		}


		/**
		 *
		 * Add POST parameter
		 *
		 * @param	string	$key	POST key name
		 * @param	mixed	$value	Parameter value
		 * @return	void
		 * @throws	InvalidArgument
		 * @see		POST
		**/
		public function POST ( $key, $value )
		{
			// If the key is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If the value is not scalar
			if ( !is_scalar( $value ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'scalar', $value, 2 );
			}


			// Insert POST parameter
			$this->post[ $key ] = $value;
		}


		/**
		 *
		 * Add another FILE to the request
		 *
		 * @param	string	$key		POST key name
		 * @param	string	$path		Path to the file
		 * @param	string	$mimetype	Mime-Type for file
		 * @return	void
		 * @throws	InvalidArgument
		 * @see		FILE
		**/
		public function FILE ( $key, $path, $mimetype = null )
		{
			// If the key is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// There is no such file
			if ( !is_file( $path ) )
			{
				throw new \Exception("File '{$path}' does not exists");
			}

			// If the mime is not empty
			if ( !empty( $mimetype ) )
			{
				//$mimetype = ";type={$mimetype}";
			}


			// Insert file into the list of POST params
			//$this->post[ $key ] = "@{$path}{$mimetype}";
			$this->post[ $key ] = new \cURLFile( $path, $mimetype, basename( $path ) );
		}


		/**
		 *
		 * Upload the file
		 *
		 * @param	File	$file	Instance of File
		 * @return	bool
		 * @see		upload
		**/
		public function upload ( File $file )
		{
			// If connection has failed
			if ( !$this->connected( ) )
			{
				return false;
			}

			// If the file could not be rewinded
			if ( !$file->rewind( ) )
			{
				//return false;
			}


			// Assign needed options for file upload
			$this->option( CURLOPT_RETURNTRANSFER, true );
			$this->option( CURLOPT_UPLOAD, true );
			$this->option( CURLOPT_PUT, true );

			// Insert information about the file into cURL
			$this->option( CURLOPT_INFILE, $file->link );
			$this->option( CURLOPT_INFILESIZE, $file->size );

			return true;
		}


		/**
		 *
		 * Execute cURL operation
		 *
		 * @param	void
		 * @return	mixed
		 * @see		execute
		**/
		public function execute ( )
		{
			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Build URL object into string and set cURL option
			$this->option( CURLOPT_URL, (string) $this->url );

			// If there are any POST data
			if ( !empty( $this->post ) )
			{
				// Insert post fields into the option list and enable POST
				$this->option( CURLOPT_POST,		true );
				$this->option( CURLOPT_POSTFIELDS,	$this->post );
			}


			// Execute cURL operation and get response
			$response = curl_exec( $this->link );

			// If there was an error
			if ( $response === false )
			{
				return $this->error( );
			}

			// If there is response
			if ( !empty( $response ) )
			{
				return $response;
			}


			return true;
		}


		/**
		 *
		 * Get connection informations
		 *
		 * @param	void
		 * @return	array
		 * @see		info
		**/
		public function info ( $option = null )
		{
			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}

            if ( empty( $option ) )
            {
                return curl_getinfo( $this->link );
            }

			// Get list of connection information
			return curl_getinfo( $this->link, $option );
		}


		/**
		 *
		 * Checks whether an error has occured
		 *
		 * @param	void
		 * @return	bool
		 * @see		error
		**/
		public function error ( )
		{
			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Gather information about error
			$code = curl_errno( $this->link );
			$text = curl_error( $this->link );

			// If there is no error
			if ( empty( $code ) )
			{
				return false;
			}


			// Save the error into the error log
			$this->error[ $code ] = $text;

			return true;
		}


		/**
		 *
		 * Close connection
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		public function close ( )
		{
			// If there is no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Close the cURL connection
			curl_close( $this->link );

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

