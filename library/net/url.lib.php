<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2011-3-24 13:05
	* @file:		URL library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	URL LIBRARY


	namespace WebCore\Net;


	use \WebCore\Core\Exceptions;
	use \WebCore\Data;
	use \WebCore\Web;


	final class URL extends Data\DSN
	{
		/**
		 * Version holder
		**/
		const VERSION	= 2;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	string	$url		URL string
		 * @param	bool	$excend		Extend current URL?
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( $url, $extend = true )
		{
			// Assign default values to the object
			$this->scheme = self::Scheme( );
			$this->host = $_SERVER['HTTP_HOST'];
			$this->path = $_SERVER['SCRIPT_NAME'];

			// If the request query string is not empty
			if ( !empty( $_SERVER['QUERY_STRING'] ) )
			{
				// Parse the query string into default Url object data
				parent::__construct("?{$_SERVER['QUERY_STRING']}");
			}

			// Do not extend current URL
			if ( $extend == false )
			{
				// Perform complete clear
				$this->clear( true );
			}


			// Call the parent constructor
			parent::__construct( $url );
		}


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			return (string) $this->build( );
		}


		/**
		 *
		 * Create new instance for current Url
		 *
		 * @param	void
		 * @return	self
		 * @see		Current
		**/
		public static function Current ( )
		{
			// Return new Url object
			return new self( null );
		}


		/**
		 *
		 * Create URL instance from string
		 *
		 * @param	string	$url	URL string
		 * @return	self
		 * @see		From
		**/
		public static function From ( $url )
		{
			// Return new Url object
			return new self( $url, false );
		}


		/**
		 *
		 * Access current URL Scheme
		 *
		 * @param	void
		 * @return	string
		 * @see		Scheme
		**/
		public static function Scheme ( )
		{
			return $_SERVER['REQUEST_SCHEME'];
		}


		/**
		 *
		 * Get referer URL
		 *
		 * @param	void
		 * @return	WebCore\Net\URL
		 * @see		Referer
		**/
		public static function Referer ( )
		{
			// If there is no referrer set in the headers
			if ( !array_key_exists( 'HTTP_REFERER', $_SERVER ) )
			{
				return null;
			}


			// Access the referer value from
			// headers and create URL object
			$url = self::From( $_SERVER['HTTP_REFERER'] );
			$url->remove('notify');

			return $url;
		}


		/**
		 *
		 * Get redirect URL
		 *
		 * @param	void
		 * @return	WebCore\Net\URL
		 * @see		Redirect
		**/
		public static function Redirect ( )
		{
			// If redirect Url is not defined in reqeust
			if ( !Web\Request::Query( 'redirect', $url ) )
			{
				return false;
			}


			// Access the redirect value from
			// query and create URL object
			$url = self::From( $url );
			$url->remove('notify');

			return $url;
		}



		/**
		 *
		 * Set value into the query
		 *
		 * @param	string	$key	Key of the query
		 * @param	mixed	$value	Value of the query
		 * @return	void
		 * @throws	InvalidArgument
		 * @see		set
		**/
		public function set ( $key, $value )
		{
			// If the key is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If the value is resource
			if ( is_resource( $value ) )
			{
				throw new \Exception("");
			}

			// If the value is null
			if ( is_null( $value ) )
			{
				// Set to empty string
				$value = '';
			}

			// If the value is an object
			if ( is_object( $value ) )
			{
				// Convert object to array
				$value = (array) $value;
			}


			// Insert new query value into list
			$this->query[ $key ] = $value;
		}


		/**
		 *
		 * Get the value from the query
		 *
		 * @param	string	$key	Key of the query
		 * @return	string
		 * @see		get
		**/
		public function get ( $key )
		{
			// Check if the key is presented
			if ( !$this->has( $key ) )
			{
				return null;
			}


			// Return the value of query
			return $this->query[ $key ];
		}


		/**
		 *
		 * Remove value from the query
		 *
		 * @param	string	$key	Key of the query
		 * @return	bool
		 * @see		remove
		**/
		public function remove ( $key )
		{
			// Check if the key is presented
			if ( !$this->has( $key ) )
			{
				return false;
			}


			// Remove the key from the query
			unset( $this->query[ $key ] );

			return true;
		}


		/**
		 *
		 * Checks value from the query
		 *
		 * @param	string	$key	Key of the query
		 * @return	bool
		 * @see		has
		**/
		public function has ( $key )
		{
			// If the query is not instantied
			if ( !is_array( $this->query ) )
			{
				return false;
			}


			// Check the existece of the key in the query
			return array_key_exists( $key, $this->query );
		}


		/**
		 *
		 * Query values
		 *
		 * @param	void
		 * @return	array
		 * @see		query
		**/
		public function query ( )
		{
			return $this->query;
		}


		/**
		 *
		 * Clear the URL
		 *
		 * @param	void
		 * @return	void
		 * @see		clear
		**/
		public function clear ( $completely = false )
		{
			// If the URL should be cleared completely
			if ( $completely == true )
			{
				// Clear parts of the URL
				$this->scheme	= null;
				$this->host		= null;
				$this->port		= null;
				$this->user		= null;
				$this->pass		= null;
				$this->path		= null;
				$this->fragment	= null;
			}


			// CLear the query holder
			$this->query = array( );
		}


		/**
		 *
		 * Build URL string
		 *
		 * @param	void
		 * @return	string
		 * @see		build
		**/
		protected function build ( )
		{
			// If there is no scheme defined
			if ( empty( $this->scheme ) )
			{
				// Get current scheme from the request
				$this->scheme = static::Scheme( );
			}


			// Build the URL in DSN Class
			return parent::build( );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

