<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-26 16:47
	* @file:		Debug task library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DEBUG TASK LIBRARY


	namespace WebCore\Tasks;


	use \WebCore\Core;
	use \WebCore\Debug as XDEBUG;	// Collision with class name


	final class Debug extends Core\Task
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;



		/**
		 *
		 * Task preparation
		 *
		 * @param	XDEBUG\Log	$log	Logger instance
		 * @return	bool
		 * @see		prepare
		**/
		public function prepare ( XDEBUG\Log &$log )
		{
			// Enable debugger and return success
			XDEBUG\Debug::Enable( ); return true;
		}


		/**
		 *
		 * Task execution
		 *
		 * @param	XDEBUG\Log	$log	Logger instance
		 * @return	bool
		 * @see		execute
		**/
		public function execute ( XDEBUG\Log &$log = null )
		{
			// Dump the task instance and exit
			XDEBUG\Debug::Shutdown( $this );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

