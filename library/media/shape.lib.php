<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-12-10 11:30
	* @file:		Shape library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	SHAPE LIBRARY


	namespace WebCore\Media;


	class Shape extends \ImagickDraw
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

