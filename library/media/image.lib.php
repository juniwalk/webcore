<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-12-5 15:13
	* @file:		Image library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	IMAGE LIBRARY


	namespace WebCore\Media;


	use \WebCore\IO\File;


	class Image extends \Imagick
	{
		/**
		 * Version holder
		**/
		const VERSION					= 1;


		/**
		 * Resource holder
		**/
		const WATERMARK_CENTER			= 1;
		const WATERMARK_ROTATED			= 2;
		const WATERMARK_TOP_LEFT		= 3;
		const WATERMARK_TOP_RIGHT		= 4;
		const WATERMARK_BOTTOM_LEFT		= 5;
		const WATERMARK_BOTTOM_RIGHT	= 6;


		/**
		 * Resource holder
		**/
		protected $file					= null;



		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	bool
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			return parent::clear( );
		}


#region Instance


		/**
		 *
		 * Create Image
		 *
		 * @param	int		$width			Image width
		 * @param	int		$height			Image height
		 * @param	string	$background		Background color
		 * @return	self
		 * @see		Create
		**/
		public static function Create ( $width, $height, Color $background = null )
		{
			// If there is no backgroun
			if ( is_null( $background ) )
			{
				// Set the background to transparent
				$background = Color::Transparent( );
			}


			// Create Image instance
			$image = new self( );
			$image->file = File::Create("php://memory");
			$image->canvas( $width, $height, $background );

			// If the image was not loaded
			if ( !$image->valid( ) )
			{
				return false;
			}


			return $image;
		}


		/**
		 *
		 * Open Image
		 *
		 * @param	mixed	$file		File to open
		 * @param	array	$options	List of options
		 * @return	self
		 * @see		Open
		**/
		public static function Open ( File $file, array $options = array( ) )
		{
			// Create Image instance
			$image = new self( );
			$image->file = &$file;

			// iterate through the list of options
			foreach ( $options as $key => $value )
			{
				$image->option( $key, $value );
			}


			// Load file from the file stream handler
			try {
				$image->readImageFile( $file->link );
			} catch (\Exception $e) {
				return false;
			}

			// If the image was not loaded
			if ( !$image->valid( ) )
			{
				return false;
			}


			// Set the name of the file into the IMagick
			$image->setImageFilename( $file->name );

			return $image;
		}


		/**
		 *
		 * Open Image
		 *
		 * @param	string	$path		Path to file
		 * @param	array	$options	List of options
		 * @return	self
		 * @see		OpenFile
		**/
		public static function OpenFile ( $path, array $options = array( ) )
		{
			// If the path is not a string
			if ( !is_string( $path ) )
			{
				return false;
			}


			// Try to open the file
			$file = File::Open( $path );

			// We have failed to open the file
			if ( $file === false )
			{
				return false;
			}


			// Call the method to open the Image
			return static::Open( $file, $options );
		}


		/**
		 *
		 * Open Image
		 *
		 * @param	string	$content	Image content
		 * @param	array	$options	List of options
		 * @return	self
		 * @see		OpenBlob
		**/
		public static function OpenBlob ( $content, array $options = array( ) )
		{
			// If the path is not a string
			if ( !is_string( $content ) )
			{
				return false;
			}


			// Create Image instance
			$image = new self( );

			// iterate through the list of options
			foreach ( $options as $key => $value )
			{
				$image->option( $key, $value );
			}


			// Load file from the file stream handler
			$image->readImageBlob( $content );

			// If the image was not loaded
			if ( !$image->valid( ) )
			{
				return false;
			}


			return $image;
		}


#endregion

#region Drawing


		/**
		 *
		 * Draw shape into the image
		 *
		 * @param	Shape	$shape	Shape to be drawn
		 * @return	bool
		 * @see		draw
		 * @alias	Imagick::drawImage
		**/
		public function draw ( Shape $shape )
		{
			return parent::drawImage( $shape );
		}


		/**
		 *
		 * Set the compression quality
		 *
		 * @param	int		$value		Quality setting
		 * @return	bool
		 * @see		quality
		 * @alias	Imagick::setImageCompressionQuality
		**/
		public function quality ( $value )
		{
			return parent::setImageCompressionQuality( $value );
		}


		/**
		 *
		 * Trim color on the edges
		 *
		 * @param	int		$tolerance		Color tolerance
		 * @return	bool
		 * @see		trim
		 * @alias	Imagick::trimImage
		**/
		public function trim ( $tolerance )
		{
			return parent::trimImage( $tolerance );
		}


		/**
		 *
		 * Create thumbnail from the Image
		 *
		 * @param	int		$width	Thumbnail width
		 * @param	int		$height	Thumbnail height
		 * @return	bool
		 * @see		thumbnail
		 * @alias	Imagick::thumbnailImage
		**/
		public function thumbnail ( $width, $height, $fit = true )
		{
			return parent::thumbnailImage( $width, $height, $fit );
		}


		/**
		 *
		 * Draw watermark over image
		 *
		 * @param	File	$source		Source image
		 * @param	int		$align		Align of the image
		 * @param	Color	$color		Watermark color
		 * @param	float	$opacity	Image opacity
		 * @return	bool
		 * @see		watermark
		**/
		public function watermark ( File $source, $align = null, Color $color = null, $opacity = 0.4 )
		{
			// Define options for the Watermark image before opening
			$params = [ 'background' => "none", 'density' => 300 ];

			// Switch the position of image
			switch ( $align )
			{
				// Centered watermark is 2.25/4 of original image
				case static::WATERMARK_CENTER;
				case static::WATERMARK_ROTATED;
				default:	// CENTER is default

					// Make the watermark 2.25/4 of the image
					$width = $this->width( ) * 2.25/4;

				break;

				// Cornered watermark is 1.25/4 of original image
				case static::WATERMARK_TOP_LEFT;
				case static::WATERMARK_TOP_RIGHT;
				case static::WATERMARK_BOTTOM_LEFT;
				case static::WATERMARK_BOTTOM_RIGHT;

					// Make the watermark 1.25/4 of the image
					$width = $this->width( ) * 1.25/4;

				break;
			}


			// Open source Image into memory
			$watermark = static::Open( $source, $params );

			// If we have failed to open Image
			if ( $watermark === false )
			{
				return false;
			}


			// Set image scale and color for watermark
			$watermark->scaleImage( $width, 0 );

			// If we have been given color for watermark
			if ( $color instanceof Color )
			{
				// Colorize the shape to that color
				$watermark->colorize( $color );
			}


			// Get the color space of the watermark Image
			$colors = $watermark->getImageColorspace( );

			// If the watermark has greater color space then Image
			if ( $colors > parent::getImageColorspace( ) )
			{
				// Extends the color space using watermark value
				parent::setImageColorspace( $colors );
			}

			// Switch the position of image
			switch ( $align )
			{
				// Make the image centered and if
				// desired so, rotate it -45 degrees
				case static::WATERMARK_CENTER;
				case static::WATERMARK_ROTATED;
				default:	// CENTER is default

					// If we should also rotate the image
					if ( $align == static::WATERMARK_ROTATED )
					{
						// Don't forget to rotate the image
						$watermark->rotateImage( "none", -45 );
					}


					// Compute {x,y} positions of the watermark image
					$y = ( $this->height( ) - $watermark->height( ) ) /2;
					$x = ( $this->width( ) - $width ) /2;

				break;

				// Position the watermark to TOP LEFT
				case static::WATERMARK_TOP_LEFT;

					// Compute {x,y} positions for TOP LEFT
					// settings with 3% margin of original image
					$x = $this->width( ) * 0.03;
					$y = $this->height( ) * 0.03;

				break;

				// Position the watermark to TOP RIGHT
				case static::WATERMARK_TOP_RIGHT;

					// Compute {x,y} positions for TOP RIGHT
					// settings with 3% margin of original image
					$x = $this->width( ) * 0.97 - $watermark->width( );
					$y = $this->height( ) * 0.03;

				break;

				// Position the watermark to BOTTOM LEFT
				case static::WATERMARK_BOTTOM_LEFT;

					// Compute {x,y} positions for BOTTOM LEFT
					// settings with 3% margin of original image
					$x = $this->width( ) * 0.03;
					$y = $this->height( ) * 0.97 - $watermark->height( );

				break;

				// Position the watermark to BOTTOM RIGHT
				case static::WATERMARK_BOTTOM_RIGHT;

					// Compute {x,y} positions for BOTTOM RIGHT
					// settings with 3% margin of original image
					$x = $this->width( ) * 0.97 - $watermark->width( );
					$y = $this->height( ) * 0.97 - $watermark->height( );

				break;
			}


			// Set watermark opacity to given value
			$watermark->setOpacity( (float) $opacity );

			// Perform the image composition with
			// gathered information about the task
			$response = parent::compositeImage
			(
				$watermark,				// Watermark instance
				static::COMPOSITE_OVER,	// Overlay mode
				$x, $y,					// Position
				static::CHANNEL_ALL		// Channels to use
			);

			// If we could not composite images
			if ( $response === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Colorize image
		 *
		 * @param	Color	$color
		 * @return	bool
		 * @see		colorize
		**/
		public function colorize ( Color $color )
		{
			// Define channel modes
			static $mode = array
			(
				'extract'	=> self::ALPHACHANNEL_EXTRACT,
				'schape'	=> self::ALPHACHANNEL_SHAPE,
			);


			// If we have failed to set alpha channel to EXTRACT mode
			if ( !parent::setImageAlphaChannel( $mode['extract'] ) )
			{
				return false;
			}


			// Set the background color of the image
			parent::setImageBackgroundColor( $color );

			// If we have failed to set alpha channel to SHAPE mode
			if ( !parent::setImageAlphaChannel( $mode['schape'] ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Set image opacity
		 *
		 * @param	float	$opacity	// Value between 0 and 1
		 * @param	int		$operator	Evaluation operator
		 * @return	bool
		 * @see		setOpacity
		**/
		public function setOpacity ( $opacity, $operator = self::EVALUATE_DIVIDE )
		{
			// If the opacity is not float
			if ( !is_float( $opacity ) )
			{
				return false;
			}


			// Evaluate the image for opacity
			return parent::evaluateImage
			(
				$operator,				// Operator
				1 / $opacity,			// New value
				static::CHANNEL_ALPHA	// Channel to evalueate
			);
		}


#endregion

#region Properties


		/**
		 *
		 * Get image width
		 *
		 * @param	void
		 * @return	int
		 * @see		width
		 * @alias	Imagick::getImageWidth
		**/
		public function width ( )
		{
			return parent::getImageWidth( );
		}


		/**
		 *
		 * Get image height
		 *
		 * @param	void
		 * @return	int
		 * @see		height
		 * @alias	Imagick::getImageHeight
		**/
		public function height ( )
		{
			return parent::getImageHeight( );
		}


		/**
		 *
		 * Get image dimensions
		 *
		 * @param	void
		 * @return	array
		 * @see		dimensions
		 * @alias	Imagick::getImageGeometry
		**/
		public function dimensions ( )
		{
			return parent::getImageGeometry( );
		}


		/**
		 *
		 * Get image resolution
		 *
		 * @param	void
		 * @return	array
		 * @see		resolution
		 * @alias	Imagick::getImageResolution
		**/
		public function resolution ( )
		{
			return parent::getImageResolution( );
		}


		/**
		 *
		 * Get image name
		 *
		 * @param	void
		 * @return	string
		 * @see		name
		 * @alias	Imagick::getImageFileName
		**/
		public function name ( )
		{
			return parent::getImageFileName( );
		}


		/**
		 *
		 * Get image format
		 *
		 * @param	void
		 * @return	string
		 * @see		format
		 * @alias	Imagick::getImageFormat
		**/
		public function format ( )
		{
			return strToLower( parent::getImageFormat( ) );
		}


		/**
		 *
		 * Get image size
		 *
		 * @param	void
		 * @return	int
		 * @see		size
		 * @alias	Imagick::getImageLength
		**/
		public function size ( )
		{
			// Get the length of the image
			$length = parent::getImageLength( );

			// If the length could not be get and there is a file
			if ( empty( $length ) AND is_object( $this->file ) )
			{
				// Get the length from the file
				$length = $this->file->size;
			}


			return $length;
		}


#endregion

#region Operations


		/**
		 *
		 * Create new canvas
		 *
		 * @param	int		$width			Image width
		 * @param	int		$height			Image height
		 * @param	string	$background		Background color
		 * @return	bool
		 * @see		canvas
		 * @alias	Imagick::newImage
		**/
		public function canvas ( $width, $height, Color $background )
		{
			// Try to clear the data
			if ( !$this->clear( ) )
			{
				return false;
			}


			// Create new image canvas in the class instance
			return parent::newImage( $width, $height, $background, 'JPEG' );
		}


		/**
		 *
		 * Set new option
		 *
		 * @param	string	$key	Option key
		 * @param	mixed	$value	Option value
		 * @return	bool
		 * @see		option
		 * @alias	Imagick::setOption
		**/
		public function option ( $key, $value )
		{
			return parent::setOption( $key, $value );
		}


		/**
		 *
		 * Strip EFIX data from Image
		 *
		 * @param	void
		 * @return	bool
		 * @see		strip
		 * @alias	Imagick::stripImage
		**/
		public function strip ( )
		{
			return parent::stripImage( );
		}


		/**
		 *
		 * Rename the image
		 *
		 * @param	string	$value	Image name
		 * @return	bool
		 * @see		rename
		 * @alias	Imagick::setImageName
		**/
		public function rename ( $value )
		{
			return parent::setImageName( $value );
		}


		/**
		 *
		 * Save Image as
		 *
		 * @param	string	$path		Path to a new file
		 * @param	bool	$overwrite	Overwrite existing files
		 * @param	mixed	$file		[out] Saved file instance
		 * @return	bool
		 * @see		save
		**/
		public function save ( $path, $overwrite = false, &$file = null )
		{
			// Try to create new file on given path
			$file = File::Create( $path, $overwrite );

			// If the could not create new file
			if ( $file === false )
			{
				return false;
			}

			// Write the data into the image file handler
			if ( !parent::writeImageFile( $file->link ) )
			{
				return false;
			}


			// Reaload file stats
			$file->stats( );

			return true;
		}


		/**
		 *
		 * Write image data
		 *
		 * @param	void
		 * @return	bool
		 * @see		write
		**/
		public function write ( )
		{
			// Write the data into the image file handler
			return parent::writeImageFile( $this->file->link );
		}


		/**
		 *
		 * Output image to browser
		 *
		 * @param	string	$path		Path to a new file
		 * @param	bool	$overwrite	Overwrite existing files
		 * @param	mixed	$file		[out] Saved file instance
		 * @return	bool
		 * @see		save
		**/
		public function output ( )
		{
			// Get the format of the output image
			// and calculate final image length
			$format = parent::getImageFormat( );
			$length = parent::getImageLength( );

			// Create date format for headers
			$date = "D, d M Y H:i:s \G\M\T";

			// If the Image format is empty
			if ( empty( $format ) )
			{
				return false;
			}


			// Call functions to prepare download
			session_write_close( );	// End PHP session handler
			ob_end_clean( );		// Exit and clear the buffer

			// Send appropriate headers for file output
			Header( "Connection: close" );
			Header( "Last-Modified: ".date( $date, $this->file->modified ) );
			Header( "Cache-Control: public" );
			Header( "Expires: ".date( $date, strToTime("+1 day") ) );
			Header( "Content-Type: image/{$format}" );
			//Header( "Content-Length: {$length}" );

			// Print the image
			echo $this; exit;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

