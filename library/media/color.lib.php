<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-12-10 11:30
	* @file:		Color library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	COLOR LIBRARY


	namespace WebCore\Media;


	class Color extends \ImagickPixel
	{
		/**
		 * Version holder
		**/
		const VERSION	= 1;


		/**
		 * Color channels
		**/
		// RGB palette
		const RED		= \Imagick::COLOR_RED;
		const GREEN		= \Imagick::COLOR_GREEN;
		const BLUE		= \Imagick::COLOR_BLUE;

		// CMYK palette
		const CYAN		= \Imagick::COLOR_CYAN;
		const MAGENTA	= \Imagick::COLOR_MAGENTA;
		const YELLOW	= \Imagick::COLOR_YELLOW;
		const BLACK		= \Imagick::COLOR_BLACK;

		// OAF palettes
		const OPACITY	= \Imagick::COLOR_OPACITY;
		const ALPHA		= \Imagick::COLOR_ALPHA;
		const FUZZ		= \Imagick::COLOR_FUZZ;



		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	bool
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			return parent::destroy( );
		}


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			return parent::getColorAsString( );
		}


#region Colors


		/**
		 *
		 * <<color>> Transparent
		 *
		 * @param	void
		 * @return	self
		 * @see		Transparent
		**/
		public static function Transparent ( )
		{
			return new static("none");
		}


		/**
		 *
		 * <<color>> Black
		 *
		 * @param	void
		 * @return	self
		 * @see		Black
		**/
		public static function Black ( )
		{
			return new static("black");
		}


		/**
		 *
		 * <<color>> White
		 *
		 * @param	void
		 * @return	self
		 * @see		White
		**/
		public static function White ( )
		{
			return new static("white");
		}


		/**
		 *
		 * <<color>> Lime
		 *
		 * @param	void
		 * @return	self
		 * @see		Lime
		**/
		public static function Lime ( )
		{
			return new static("#a3c400");
		}


		/**
		 *
		 * <<color>> Green
		 *
		 * @param	void
		 * @return	self
		 * @see		Green
		**/
		public static function Green ( )
		{
			return new static("#5fa917");
		}


		/**
		 *
		 * <<color>> Emerald
		 *
		 * @param	void
		 * @return	self
		 * @see		Emerald
		**/
		public static function Emerald ( )
		{
			return new static("#008900");
		}


		/**
		 *
		 * <<color>> Teal
		 *
		 * @param	void
		 * @return	self
		 * @see		Teal
		**/
		public static function Teal ( )
		{
			return new static("#00aba9");
		}


		/**
		 *
		 * <<color>> Cyan
		 *
		 * @param	void
		 * @return	self
		 * @see		Cyan
		**/
		public static function Cyan ( )
		{
			return new static("#1ba1e2");
		}


		/**
		 *
		 * <<color>> Cobalt
		 *
		 * @param	void
		 * @return	self
		 * @see		Cobalt
		**/
		public static function Cobalt ( )
		{
			return new static("#004fee");
		}


		/**
		 *
		 * <<color>> Indigo
		 *
		 * @param	void
		 * @return	self
		 * @see		Indigo
		**/
		public static function Indigo ( )
		{
			return new static("#6a00fe");
		}


		/**
		 *
		 * <<color>> Violet
		 *
		 * @param	void
		 * @return	self
		 * @see		Violet
		**/
		public static function Violet ( )
		{
			return new static("#aa00ff");
		}


		/**
		 *
		 * <<color>> Pink
		 *
		 * @param	void
		 * @return	self
		 * @see		Pink
		**/
		public static function Pink ( )
		{
			return new static("#f471d0");
		}


		/**
		 *
		 * <<color>> Magenta
		 *
		 * @param	void
		 * @return	self
		 * @see		Magenta
		**/
		public static function Magenta ( )
		{
			return new static("#d80073");
		}


		/**
		 *
		 * <<color>> Crimson
		 *
		 * @param	void
		 * @return	self
		 * @see		Crimson
		**/
		public static function Crimson ( )
		{
			return new static("#a20025");
		}


		/**
		 *
		 * <<color>> Red
		 *
		 * @param	void
		 * @return	self
		 * @see		Red
		**/
		public static function Red ( )
		{
			return new static("#e41400");
		}


		/**
		 *
		 * <<color>> Orange
		 *
		 * @param	void
		 * @return	self
		 * @see		Orange
		**/
		public static function Orange ( )
		{
			return new static("#fa6800");
		}


		/**
		 *
		 * <<color>> Amber
		 *
		 * @param	void
		 * @return	self
		 * @see		Amber
		**/
		public static function Amber ( )
		{
			return new static("#efa309");
		}


		/**
		 *
		 * <<color>> Yellow
		 *
		 * @param	void
		 * @return	self
		 * @see		Yellow
		**/
		public static function Yellow ( )
		{
			return new static("#d8c100");
		}


		/**
		 *
		 * <<color>> Brown
		 *
		 * @param	void
		 * @return	self
		 * @see		Brown
		**/
		public static function Brown ( )
		{
			return new static("#825a2c");
		}


		/**
		 *
		 * <<color>> Olive
		 *
		 * @param	void
		 * @return	self
		 * @see		Olive
		**/
		public static function Olive ( )
		{
			return new static("#6d8764");
		}


		/**
		 *
		 * <<color>> Steel
		 *
		 * @param	void
		 * @return	self
		 * @see		Steel
		**/
		public static function Steel ( )
		{
			return new static("#637587");
		}


		/**
		 *
		 * <<color>> Mauve
		 *
		 * @param	void
		 * @return	self
		 * @see		Mauve
		**/
		public static function Mauve ( )
		{
			return new static("#76608a");
		}


		/**
		 *
		 * <<color>> Taupe
		 *
		 * @param	void
		 * @return	self
		 * @see		Taupe
		**/
		public static function Taupe ( )
		{
			return new static("#7a3b3f");
		}


#endregion
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

