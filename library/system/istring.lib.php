<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-02-07 13:32
	* @file:		IString library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ISTRING LIBRARY


	namespace WebCore\System;


	abstract class IString
	{
		/**
		 * Version holder
		**/
		const VERSION				= 1;


		/**
		 * Accent translation
		**/
		const ACCENT_FROM			= 'ăåãâàáäçčďêèéěëïîìíñňõôòðóöřșşšțţťûùúůüÿýžÅÃÂÀÁÄÇČÐĎÊÈÉĚËÎÌÍÏÑŇÕÔÒÓÖŘȘŞŠȚŤÛÙÚŮÜÝŸŽ';
		const ACCENT_TO				= 'aaaaaaaccdeeeeeiiiinnoooooorssstttuuuuuyyzAAAAAACCDDEEEEEIIIINNOOOOORSSSTTUUUUUYYZ';


		/**
		 * Properties
		**/
		protected static $filename	= array
		(
			'Œ' => "OE",	'œ' => "oe",	'Æ' => "AE",	'æ' => "ae",
			'¥' => "Y",		'€' => "E",		'µ' => "u",		'Ø' => "O",
			'ø' => "o",		'ß' => "B",		'/' => "-",		' ' => "-",
			',' => null,	'"' => null,	'_' => '-',		'=' => '-',
			'+' => '-',		'(' => null,	')' => null,	'&' => null,
			'#' => null,	'\\' => null,	'.' => null,	';' => null,
			'|' => null,	'!' => null,	'<' => null,	'>' => null,
			'{' => null,	'}'	=> null,	'[' => null,	']' => null,
			'%' => null,	'*' => null,	'^'	=> null,	'$'	=> "S",
			'@'	=> "a",		'¦'	=> null,	'`' => null,	'¬' => null,
			'?' => null,	':' => null,
		);



#region Methods


		/**
		 *
		 * Create safe filename from the string
		 *
		 * @param	string	$value	Strinch which will be converted
		 * @param	string	$ext	File extension
		 * @param	bool	$lower	Use lowercase name
		 * @return	string
		 * @see		Filename
		**/
		public static function Filename ( $value, $ext = null, $lower = false )
		{
			// If the value is not a string
			if ( !is_string( $value ) )
			{
				return false;
			}


			// Remove any accent characters
			$value = self::RemoveAccent( $value );
			$value = self::Replace( $value, self::$filename );
			$value = preg_replace( '/([^a-z0-9\\040\\.\\-\\_\\\\]+)/i', null, $value );
			$value = preg_replace( '/([--]+)/i', '-', $value );
			$value = self::Trim( $value, "\r\n\t _-" );

			// Explode the name and remove the last word
			$string = explode( "-", $value );
			$string = array_unique( $string );

			// If the length is below 250
			if ( strlen( $value ) > 250 )
			{
				// Remove last word
				array_pop( $string );
			}


			// Create back the filename
			$value = implode( "-", $string );

			// If there is a extension
			if ( is_string( $ext ) )
			{
				$value = "{$value}.{$ext}";
			}

			// If we need lowercase name
			if ( $lower == true )
			{
				// Use the function to make it lower
				$value = strToLower( $value );
			}


			// Return new name
			return $value;
		}


#region Helpers


		/**
		 *
		 * Replace all occurrences of the search string with the replacement string
		 *
		 * @param	string	$value		String we will work with
		 * @param	array	$search		Search array
		 * @param	bool	$regexp		Do we want to use Regular Expressions?
		 * @return	string
		 * @see		Replace
		**/
		public static function Replace ( $value, array $search, $regexp = false )
		{
			// If the value is not a string
			if ( !is_string( $value ) )
			{
				// Error
			}

			// If there is no replacement
			if ( empty( $search ) )
			{
				// Error
			}


			// get the list for replace
			$replace = array_values( $search );
			$search = array_keys( $search );

			// Replace the values from the lists in the value
			return str_replace( $search, $replace, $value );
		}


		/**
		 *
		 * Translate characters in the string
		 *
		 * @param	string	$value		String to be translated
		 * @param	mixed	$search		Array or String of characters
		 * @param	mixed	$replace	Array or String of characters
		 * @return	string
		 * @see		Translate
		**/
		public static function Translate ( $value, $search, $replace )
		{
			// If the value is not a string
			if ( !is_string( $value ) )
			{
				// Error
			}

			// If the search value is string
			if ( is_string( $search ) )
			{
				// Get the list of characters
				$search = self::Split( $search );
			}

			// If the replace value is string
			if ( is_string( $replace ) )
			{
				// Get the list of characters
				$replace = self::Split( $replace );
			}


			// Replace each character with its companion in second list
			return str_replace( $search, $replace, $value );
		}


		/**
		 *
		 * Replace any accent characters with non-accent ones
		 *
		 * @param	string	$value	String to be stripped of accent
		 * @return	string
		 * @see		RemoveAccent
		**/
		public static function RemoveAccent ( $value )
		{
			// Replace any accent characters with non-accent ones
			return self::Translate( $value, self::ACCENT_FROM, self::ACCENT_TO );
		}


		/**
		 *
		 * Trim the string
		 *
		 * @param	string	$value	String to be trimmed
		 * @param	string	$chars	Characters to trim
		 * @return	string
		 * @see		Trim
		**/
		public static function Trim ( $value, $chars = "\t\r\n " )
		{
			// Trim the stirng
			return trim( $value, $chars );
		}


#endregion

#region Helpers


		/**
		 *
		 * Split the string into array
		 *
		 * @param	string	$value	String to be splitted
		 * @return	string
		 * @see		Split
		**/
		protected static function Split ( $value )
		{
			// Split the string into the list of characters without spaces
			return preg_split( '~~u', $value, null, PREG_SPLIT_NO_EMPTY );
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

