<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-10-20 12:43
	* @file:		IArray library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	IARRAY LIBRARY


	namespace WebCore\System;


	abstract class IArray
	{
		/**
		 * Version holder
		**/
		const VERSION				= 1;


#region Methods


		/**
		 *
		 * Apply a user supplied function to every member of an array
		 *
		 * @param	array		$subject	Input array
		 * @param	callable	$method		Callback method
		 * @param	mixed		$params		Parameters
		 * @return	bool
		 * @see		Walk
		**/
		public static function Walk ( array &$array, callable $method, $params = null )
		{
			// If the array is empty
			if ( empty( $array ) )
			{
				return false;
			}

			// Perform while loop until the end of array
			while ( list( $key, $value ) = each( $array ) )
			{
				// Call the callback method on the key/value
				$result = $method( $key, $array[ $key ], $params );

				// If the callback has failed
				if ( $result === false )
				{
					return false;
				}
			}


			return true;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

