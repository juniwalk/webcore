<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-2-1 11:51
	* @file:		WebCore library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	REQUEST LIBRARY


	namespace WebCore\Web;


	use \WebCore\Core;
	use \WebCore\Core\Exceptions;
	use \WebCore\Debug;
	use \WebCore\Net;
	use \WebCore\Security;


	class Request
	{
		/**
		 * Version holder
		**/
		const VERSION			= 2;



		/**
		 *
		 * Is the request from AJAX?
		 *
		 * @param	void
		 * @return	bool
		 * @see		AJAX
		**/
		public static function AJAX ( )
		{
			return array_key_exists( 'HTTP_X_REQUESTED_WITH', $_SERVER );
		}


		/**
		 *
		 * Access path info
		 *
		 * @param	void
		 * @return	string
		 * @see		Path
		**/
		public static function Path ( )
		{
			// If there is no PATH_INFO in the server variable
			if ( !array_key_exists( 'PATH_INFO', $_SERVER ) )
			{
				return false;
			}


			// Access given path info with request mode
			$path = $_SERVER['PATH_INFO'];
			$mode = $_SERVER['REQUEST_METHOD'];

			// If the path is empty
			if ( empty( $path ) )
			{
				return false;
			}


			// Remove any trailing slash and return patj
			return rtrim( "{$mode}:{$path}", '/' );
		}


		/**
		 *
		 * Get the host IP address
		 *
		 * @param	void
		 * @return	string
		 * @see		Host
		**/
		public static function Host ( )
		{
			// Return the host IP address
			return $_SERVER["REMOTE_ADDR"];
		}


		/**
		 *
		 * Get the server IP address
		 *
		 * @param	void
		 * @return	string
		 * @see		Server
		**/
		public static function Server ( )
		{
			// Return the server IP address
			return $_SERVER["SERVER_ADDR"];
		}


		/**
		 *
		 * Set new cookie
		 *
		 * @param	string	$key		Name of the cookie
		 * @param	string	$value		Value for the stored cookie
		 * @param	int		$expire		Relative expiration time
		 * @param	string	$domain		Default cookie domain
		 * @return	bool
		 * @see		Cookie
		**/
		public static function Cookie ( $key, $value, $expire = 86400, $domain = null )
		{
			// If the key is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If the value is not scalar
			if ( !is_scalar( $value ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'scalar', $value, 2 );
			}

			// If expiration time isn't integer
			if ( !is_numeric( $expire ) )
			{
				$expire = 86400;
			}


			// Prepare expire time for cookie
			$expire = time( ) + $expire;

			// If there is no domain
			if ( empty( $domain ) )
			{
				return setcookie( $key, $value, $expire );
			}


			// Set the cookie with all needed values
			return setcookie( $key, $value, $expire, '/', $domain );
		}


		/**
		 *
		 * Access value from GET or POST superglobal
		 *
		 * @param	string	$key		Key to search for
		 * @param	mixed	$value		Value container
		 * @param	mixed	$default	Default value
		 * @return	bool
		 * @see		Query
		**/
		public static function Query ( $key, &$value, $default = null )
		{
			// Set the value to default
			$value = $default;

			// If the key is not string
			if ( !is_scalar( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'scalar', $key, 1 );
			}

			// If there is no such value in the request
			if ( !array_key_exists( $key, $_REQUEST ) )
			{
				return false;
			}


			// Access the value from request
			$value = $_REQUEST[ $key ];

			// If the Integrity check of the value has failed
			if ( !Security\Check::Integrity( $value ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Access value from GET superglobal
		 *
		 * @param	string	$key		Key to search for
		 * @param	mixed	$value		Value container
		 * @param	mixed	$default	Default value
		 * @return	bool
		 * @see		GET
		**/
		public static function GET ( $key, &$value, $default = null )
		{
			// Set the value to default
			$value = $default;

			// If the key does not exists in GET
			if ( !array_key_exists( $key, $_GET ) )
			{
				return false;
			}


			// Access GET value
			$value = $_GET[ $key ];

			// If the Integrity check of the value has failed
			if ( !Security\Check::Integrity( $value ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Access value from POST superglobal
		 *
		 * @param	string	$key		Key to search for
		 * @param	mixed	$value		Value container
		 * @param	mixed	$default	Default value
		 * @return	bool
		 * @see		POST
		**/
		public static function POST ( $key, &$value, $default = null )
		{
			// Set the value to default
			$value = $default;

			// If the key does not exists in POST
			if ( !array_key_exists( $key, $_POST ) )
			{
				return false;
			}


			// Access POST value
			$value = $_POST[ $key ];

			// If the Integrity check of the value has failed
			if ( !Security\Check::Integrity( $value ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Access file from FILES superglobal
		 *
		 * @param	string	$key		Key to search for
		 * @param	mixed	$value		Value container
		 * @param	mixed	$default	Default value
		 * @return	bool
		 * @see		FILE
		**/
		public static function FILE ( $key, &$value, $default = null )
		{
			// Assign default value
			$value = $default;

			// If the key does not exists in FILES
			if ( !array_key_exists( $key, $_FILES ) )
			{
				return false;
			}


			// Access FILE value
			$value = $_FILES[ $key ];

			// Switch the error code
			switch ( $value['error'] )
			{
				// Everything is OK
				case UPLOAD_ERR_OK:

					return true;

				break;

				// There was a problem with file size
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:

					//throw new \Exception("");

				break;

				// If no file was uploaded
				case UPLOAD_ERR_NO_FILE:
				case UPLOAD_ERR_PARTIAL:

					// Assign default value
					$value = $default;

				break;

				case UPLOAD_ERR_NO_TMP_DIR:
				case UPLOAD_ERR_CANT_WRITE:
				case UPLOAD_ERR_EXTENSION:

					//throw new \Exception("");

				break;
			}


			return false;
		}


		/**
		 *
		 * Get the access time
		 *
		 * @param	void
		 * @return	\WebCore\Core\Date
		 * @see		Time
		**/
		public static function Time ( )
		{
			// Get the request start time
			$time = $_SERVER['REQUEST_TIME'];

			// If the time is not numeric
			if ( !is_numeric( $time ) )
			{
				return Core\Date::TIMESTAMP_EMPTY;
			}


			// Return the request time
			return new Core\Date( $time );
		}


		/**
		 *
		 * Redirector
		 *
		 * @param	Net\URL	$url		Redirect URL
		 * @param	bool	$override	Debug override
		 * @return	void
		 * @see		Redirect
		**/
		public static function Redirect ( Net\URL &$url, $override = false )
		{
			// Stop the profiler
			Debug\Profiler::Stop(0, 'Page rendered');

			// If debugging is enabled and not overriden
			if ( Debug\Debug::Enabled( ) AND !$override )
			{
				// Print standard HTML link to empty page for continuum
				echo "<a href=\"{$url}\">Continue redirecting</a>"; exit;
			}


			// Clear output buffer
			ob_end_clean( );

			// Check sent headers
			if ( !headers_sent( ) )
			{
				// Redirect to the given Url using header
				header( "Location: {$url}", true, 302 ); exit;
			}


			// TODO: Display redirect template from Smarty
			throw new \Exception("Unable to redirect, because headers have been already sent");
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

