<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-12-5 15:23
	* @file:		Model interface
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	MODEL INTERFACE


	namespace WebCore\Web\Pager;


	interface Model
	{
		/**
		 *
		 * Get the list of page items
		 *
		 * @param	void
		 * @return	array
		 * @see		pages
		**/
		public function pages ( );


		/**
		 *
		 * Get the list of size per page
		 *
		 * @param	void
		 * @return	array
		 * @see		perpage
		**/
		public function perpage ( );


		/**
		 *
		 * Total number of items
		 *
		 * @param	void
		 * @return	int
		 * @see		count
		**/
		public function count ( );


		/**
		 *
		 * Base URL of the page
		 *
		 * @param	void
		 * @return	URL
		 * @see		url
		**/
		public function url ( );
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

