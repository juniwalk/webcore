<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-25 20:40
	* @file:		Site library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	SITE LIBRARY


	namespace WebCore\Web;


	use \WebApp;
	use \WebCore\Core\Exceptions;
	use \WebCore\Data;
	use \WebCore\Debug\Debug;
	use \WebCore\DB;


	final class Site
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * WebCore Properties
		**/
		public $id				= null;
		public $alias			= null;
		public $title			= null;
		public $keywords		= null;
		public $description		= null;
		public $url				= null;
		public $access			= null;
		public $shield			= null;
		public $debug			= null;
		public $verbose			= null;
		public $disabled		= null;
		public $params			= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( ) { }


		/**
		 *
		 * Factory
		 *
		 * @param	void
		 * @return	self
		 * @see		Factory
		**/
		public static function &Factory ( )
		{
			// Instance holder
			static $class = null;

			$site = WebApp\Config::SITE_ID;

			if ( empty( $site ) )
			{
				$site = 0;
			}

			// If there is no instance
			if ( is_null( $class ) )
			{
				// Create new instance
				$class = new self( );
				$class->load( $site );
			}


			// Return instance
			return $class;
		}


		/**
		 *
		 * Build configuration
		 *
		 * @param	void
		 * @return	bool
		 * @see		configurate
		**/
		public function configurate ( )
		{
			// Access instance of the Site's config
			$config = &WebApp\Config::Factory( );

			// If there are no config data
			if ( empty( $this->params ) )
			{
				return true;
			}

			// Send config data into the config object
			if ( !$config->populate( $this->params ) )
			{
				return false;
			}


			return true;
		}



		/**
		 *
		 * Load site data
		 *
		 * @param	void
		 * @return	bool
		 * @see		load
		**/
		private function load ( $site )
		{
			// Access the WebCore database
			$db = DB\DB::Get('webcore');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}

			// If the site Id is not a integer
			if ( !is_integer( $site ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'integer', $site, 1 );
			}


			// Build SQL query to select the properties from the site configuration
			$select = DB\SQL::Set("SELECT * FROM `site` WHERE `id` = {$site};");

			// If we were unable to select config
			if ( !$db->query( $select ) )
			{
				return false;
			}


			// Access the config data
			$site = $db->results(0);

			// Iterate through each setting
			foreach ( $site as $key => $value )
			{
				// Switch the key
				switch ( $key )
				{
					case 'id':

						$value = (int) $value;

					break;

					case 'shield':
					case 'debug':
					case 'verbose':
					case 'disabled':

						$value = (bool) $value;

					break;

					case 'params':

						// If there are no values
						if ( empty( $value ) )
						{
							break;
						}


						// Read INI data into an array
						$value = Data\INI::Read( $value, true );

					break;
				}


				// Assign value to the WebCore
				$this->$key = $value;
			}

			// Check the debug state
			switch ( $this->debug )
			{
				// If the debug is disabled
				default: case false:

					Debug::Disable( );

				break;

				// Debug is enabled
				case true:

					Debug::Enable( );

				break;
			}


			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

