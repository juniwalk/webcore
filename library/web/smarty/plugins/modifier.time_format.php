<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-12-4 12:21
	* @file:		Time_Format plugin
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	TIME_FORMAT PLUGIN


	/**
	 *
	 * Format UNIX timestamp into readable string {@usage: $var|time_format:true:"nothing"}
	 *
	 * @param	int		$value		Timestamp
	 * @param	bool	$value		Smart display?
	 * @param	mixed	$default	Default value on error
	 * @return	string
	 * @see		smarty_modifier_time_format
	**/
	function smarty_modifier_time_format ( $value, $smart = true, $default = null, $base = 0 )
	{
		// List of label we want to display in the final output string
		$labels = [ /*'days' => "dní",*/ 'h' => "hod", 'i' => "min" ];

		// If the value is not integer
		if ( !is_numeric( $value ) )
		{
			// Try to convert it into time
			$value = strToTime( $value );
		}

		// If there is no value
		if ( empty( $value ) )
		{
			return $default;
		}


		// Compute time difference between zero and given number of seconds
		$time = ( new \DateTime("@{$base}") )->diff( new \DateTime("@{$value}") );

		// If there is no time
		if ( empty( $time ) )
		{
			return $default;
		}


		// Prepare output string
		$output = null;

		// Walk through each label we want to show
		foreach ( $labels as $key => $format )
		{
			// Get the current value of label
			$value = $time->{ $key };

			// Convert days into hours
			if ($key == 'h') {
				$value += $time->days * 24;
			}

			// If the value is empty and smart display is enabled
			if ( $smart AND empty( $value ) )
			{
				continue;
			}


			// Get the value and format into the output
			$output .= "{$value} {$format} ";
		}


		// Return trimmed output
		return trim( $output );
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

