<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-11-6 10:07
	* @file:		Round plugin
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ROUND PLUGIN


	/**
	 *
	 * Rounds float number {@usage: $var|round}
	 *
	 * @param	string	$value	Entry value
	 * @return	string
	 * @see		smarty_modifier_round
	**/
	function smarty_modifier_round ( $value, $length = 2 )
	{
		// if the value is not numeric
		if ( !is_numeric( $value ) )
		{
			return $value;
		}


		// get the value as a float
		$value = floatval( $value );

		// If the value is empty
		if ( empty( $value ) )
		{
			return $value;
		}

		// If the length is not numeric
		if ( !is_numeric( $length ) )
		{
			$length = 2;
		}


		// Round the value to specified length
		return round( $value, $length );
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

