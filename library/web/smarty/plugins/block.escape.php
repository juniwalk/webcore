<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-10-22 10:41
	* @file:		Escape plugin
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ESCAPE PLUGIN


	/**
	 *
	 * Escape block of HTML code
	 *
	 * @param
	 * @return	string
	 * @see		smarty_block_escape
	**/
	function smarty_block_escape ( $params, $content, $template, &$repeat )
	{
		// If the content is empty
		// eg.: opening tag of the block
		if ( empty( $content ) )
		{
			return null;
		}


		return htmlentities( $content );
		\Tracy\Debugger::Dump( func_get_args( ) );
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

