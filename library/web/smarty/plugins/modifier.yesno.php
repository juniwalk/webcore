<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-9-3 13:17
	* @file:		YesNo plugin
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	YESNO PLUGIN


	/**
	 *
	 * Turn True/False into On/Off string {@usage: $var|yesno}
	 *
	 * @param	string	$value	Entry value
	 * @return	string
	 * @see		smarty_modifier_yesno
	**/
	function smarty_modifier_yesno ( $value, $yes = 'GENERAL_BOOL_YES', $no = 'GENERAL_BOOL_NO' )
	{
		// Access Lang instance
		$lang = \WebCore\Web\Lang::Factory( );

		// Check the value in boolean
		switch ( (bool) $value )
		{
			// True
			case true:

				$output = $yes;

			break;

			// False (or default)
			default: case false:

				$output = $no;

			break;
		}

		// If the output has been translated
		if ( $lang->translated( $output ) )
		{
			// Get the transalted value
			$output = $lang->X( $output );
		}


		return $output;
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

