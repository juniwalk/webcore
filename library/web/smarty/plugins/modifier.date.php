<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-11-15 13:14
	* @file:		Date plugin
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DATE PLUGIN


	/**
	 *
	 * Turn True/False into On/Off string {@usage: $var|yesno}
	 *
	 * @param	string	$value	Entry value
	 * @return	string
	 * @see		smarty_modifier_date
	**/
	function smarty_modifier_date ( $value, $format = "Y-m-d H:i:s" )
	{
		// Save the original value
		$original = $value;

		// If the value is not numeric
		if ( !is_numeric( $value ) )
		{
			// Get the unix timestamp from string
			$value = strToTime( $value );
		}

		// If the converting failed
		if ( $value == false )
		{
			return $original;
		}

		// If the format is not a string
		if ( !is_string( $format ) )
		{
			return $original;
		}


		// Return formatted date
		return date( $format, $value );
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

