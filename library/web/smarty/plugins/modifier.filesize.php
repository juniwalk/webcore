<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-06-30 0:17
	* @file:		FileSize plugin
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	YESNO PLUGIN


	/**
	 *
	 * Format file size {@usage: $file->size|filesize}
	 *
	 * @param	int		$value	File size in bytes
	 * @return	string
	 * @see		smarty_modifier_filesize
	**/
	function smarty_modifier_filesize ( $size, $decimals = null )
	{
		// Definition of sizes
		$sizes = [ 'B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' ];

		// If the value is not numeric or it is empty
		if ( !is_numeric( $size ) OR empty( $size ) )
		{
			return "n/a";
		}


		// Compute factor for the size
		$factor = floor( log( $size, 1024 ) );

		// If decimals are not numeric
		if ( !is_numeric( $decimals ) )
		{
			// Set default value
			$decimals = 2;
		}


		// Format the value
		return sprintf
		(
			"%.{$decimals}f {$sizes[$factor]}",	// Format
			$size / pow( 1024, $factor )		// Value
		);

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

