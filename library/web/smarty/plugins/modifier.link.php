<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-10-3 10:24
	* @file:		Link plugin
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	LINK PLUGIN


	/**
	 *
	 * Turn True/False into On/Off string {@usage: $url|link:"Odkaz"}
	 *
	 * @param	string	$value	Entry value
	 * @return	string
	 * @see		smarty_modifier_link
	**/
	function smarty_modifier_link ( $url, $title = null, array $params = [ ] )
	{
		// Access Lang instance
		$lang = \WebCore\Web\Lang::Factory( );

		// If the URL is incompatible
		if ( !is_string( $url ) )
		{
			return null;
		}

		// If the URL is malformed so we can't use it
		if ( !parse_url( $url, PHP_URL_SCHEME ) )
		{
			return null;
		}

		// If there is no title
		if ( is_null( $title ) )
		{
			// Use URL instead
			$title = $url;
		}

		// If the title is language anchor
		if ( $lang->translated( $title ) )
		{
			// Translate the title here
			$title = $lang->X( $title );
		}

		// Iterate through the params and parse them
		foreach ( $params as $key => &$value )
		{
			$value = "{$key}=\"{$value}\"";
		}


		return "<a href=\"". $url ."\" ". implode( " ", $params ) .">". $title ."</a>";
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

