<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-04-10 11:31
	* @file:		Phone plugin
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	PHONE PLUGIN


	/**
	 *
	 * Formats number into phone number {@usage: $var|phone:"&nbsp;":3}
	 *
	 * @param	string	$value		Entry value
	 * @param	string	$char		Character between chunks
	 * @param	int		$chunks		Chunks size
	 * @return	string
	 * @see		smarty_modifier_phone
	**/
	function smarty_modifier_phone ( $value, $char = null, $chunks = null )
	{
		// If chte character is not string
		if ( !is_string( $char ) )
		{
			$char = '&nbsp;';
		}

		// If chte chink size is not numeric
		if ( !is_numeric( $chunks ) )
		{
			$chunks = 3;
		}


		// Remove any spaces from the phone number
		$value = str_replace( ' ', null, $value );
		$value = trim( $value );

		// If the value is empty
		if ( empty( $value ) )
		{
			return $value;
		}


		// Format the phone number
		$value = str_split( $value, $chunks );
		$value = implode( $char, $value );

		return $value;
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

