<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-7-25 13:08
	* @file:		Dump plugin
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DUMP PLUGIN


	use \Tracy\Debugger;


	/**
	 *
	 * Dump infomatinou about variable {@usage: $var|dump}
	 *
	 * @param	mixed	$value	Any variable
	 * @return	void
	 * @see		smarty_modifier_dump
	**/
	function smarty_modifier_dump ( $value )
	{
		Debugger::Dump( $value ); return;
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

