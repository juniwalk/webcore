<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-10-15 15:37
	* @file:		Numeric plugin
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	YESNO PLUGIN


	/**
	 *
	 * Format number {@usage: $var|numeric}
	 *
	 * @param	string	$value	Entry value
	 * @return	string
	 * @see		smarty_modifier_numeric
	**/
	function smarty_modifier_numeric ( $value, $leading = 0, $decimal = ',', $thousand = ' ' )
	{
		return number_format( $value, $leading, $decimal, $thousand );
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

