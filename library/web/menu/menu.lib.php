<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2010-5-23 11:47
	* @file:		Menu library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	MENU LIBRARY


	namespace WebCore\Web\Menu;


	use \WebApp;
	use \WebCore\Core\Exceptions;
	use \WebCore\DB;
	use \WebCore\Web;


	final class Menu
	{
		/**
		 * Version holder
		**/
		const VERSION		= 2;


		/**
		 * Page defaults
		**/
		public $default		= null;
		public $page		= null;
		public $view		= null;


		/**
		 * Database source
		**/
		private $source		= 'webapp';


		/**
		 * Holder for the items
		**/
		private $items		= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( )
		{
			// Access instance of the WebApp Config
			$config = &WebApp\Config::Factory( );

			// If there is no default page defined in configuration
			if ( !property_exists( $config, 'PAGE_DEFAULT' ) )
			{
				throw new \Exception("Default page is not configured");
			}


			// Assign name of the default page
			$this->default = $config->PAGE_DEFAULT;

			// Access request query and get name of the page/view
			Web\Request::Query( 'page', $this->page, $this->default );
			Web\Request::Query( 'view', $this->view, null );
		}



		/**
		 *
		 * Factory method
		 *
		 * @param	mixed	$context	MySQL context for source
		 * @return	self
		 * @see		Factory
		**/
		public static function &Factory ( )
		{
			// Create instance holder
			static $class = null;

			// If there is no instance
			if ( is_null( $class ) )
			{
				// Create new instance
				$class = new self( );
			}


			// Return instance
			return $class;
		}


		/**
		 *
		 * Access current item
		 *
		 * @param	void
		 * @return	Item
		 * @see		Current
		**/
		public static function &Current ( )
		{
			// Access self instance
			$self = &self::Factory( );

			// If there is no such item
			if ( !$self->has( $self->page ) )
			{
				return $self;
			}


			// Access the item of the page
			$item = $self->get( $self->page );

			// if there is only one child
			if ( $item->count( ) <= 1 )
			{
				// Clear the childs
				$item->clear( );
			}

			// If there is child view of the page
			if ( $item->has( $self->view ) )
			{
				// Get the object of the child view
				$item = $item->get( $self->view );
			}

			// If there is no item
			if ( $item === false )
			{
				return $self;
			}


			return $item;
		}


		/**
		 *
		 * Get the list of items
		 *
		 * @param	void
		 * @return	array
		 * @see		Pages
		**/
		public static function Pages ( )
		{
			return self::Factory( )->items;
		}


		/**
		 *
		 * Get the page item
		 *
		 * @param	string	$alias	Path to view Page::View format
		 * @return	Item
		 * @see		GetPage
		**/
		public static function GetPage ( $alias )
		{
			// Access instance of Menu
			$page = static::Factory( );

			// If there are no items to use
			if ( empty( $page->items ) )
			{
				return false;
			}


			// Get the list of views in lowercase
			$views = explode( "::", strToLower( $alias ) );

			// Walk through each views
			foreach ( $views as $view )
			{
				// Get the page of the view
				$page = $page->get( $view );

				// If there is no such page
				if ( $page === false )
				{
					return false;
				}
			}


			return $page;
		}


		/**
		 *
		 * Get the list of childs
		 *
		 * @param	void
		 * @return	array
		 * @see		Childs
		**/
		public static function Childs ( )
		{
			// Access the list of items from current page
			$items = self::Current( )->items( );

			// If there are no chilren to show
			if ( sizeof( $items ) <= 1 )
			{
				return array( );
			}


			return $items;
		}



		/**
		 *
		 * Assign database source
		 *
		 * @param	scalar	$alias	Set database context
		 * @return	void
		 * @throws	Exception
		 * @see		source
		**/
		public function source ( $alias )
		{
			// Access database connection
			$db = &DB\DB::Get( $alias );

			// If the database is not connected
			if ( !$db->connected( ) )
			{
				throw new \Exception("Database named '{$alias}' is not connected");
			}


			// Assign database alias
			$this->source = $alias;
		}


		/**
		 *
		 * Register menu
		 *
		 * @param	void
		 * @return	bool
		 * @throws	Exception
		 * @see		register
		**/
		public function register ( )
		{
			// Access database connection
			$db = &DB\DB::Get( $this->source );

			// If the database is not connected
			if ( !$db->connected( ) )
			{
				return false;
			}

			// If we could not load menus
			if ( !$this->load_menus( ) )
			{
				return false;
			}

			// If we could not load submenus
			if ( !$this->load_submenus( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Register menu
		 *
		 * @param	string	$key		Item alias
		 * @param	Item	$item		Item instance
		 * @return	bool
		 * @see		add
		**/
		public function add ( $key, Item &$item )
		{
			// If the item is already present in holder
			if ( $this->has( $key ) )
			{
				return false;
			}


			// Assign Item to the holder
			$this->items[ $key ] = &$item;

			return true;
		}


		/**
		 *
		 * Does item exists?
		 *
		 * @param	string	$key	Item alias
		 * @return	bool
		 * @see		has
		**/
		public function has ( $key )
		{
			// Access the list of items
			$items = &$this->items;

			// If there are no items
			if ( empty( $items ) )
			{
				return false;
			}

			// If there is no such item in the list
			if ( !array_key_exists( $key, $items ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Does item exists?
		 *
		 * @param	string	$key	Item alias
		 * @return	Item
		 * @see		get
		**/
		public function get ( $key )
		{
			// If the re is no such item
			if ( !$this->has( $key ) )
			{
				return false;
			}


			// Access the item from holder
			$item = &$this->items[ $key ];

			return $item;
		}


		/**
		 *
		 * Delete the item
		 *
		 * @param	string	$key	Item alias
		 * @return	bool
		 * @see		delete
		**/
		public function delete ( $key )
		{
			// If the re is no such item
			if ( !$this->has( $key ) )
			{
				return false;
			}


			// Remove the Item from the holder
			unset( $this->items[ $key ] );

			return true;
		}


		/**
		 *
		 * Does item exists?
		 *
		 * @param	string	$key	Item alias
		 * @return	Item
		 * @see		get
		**/
		public function home ( )
		{
			// Access the item from holder
			return $this->items[ $this->default ];
		}


		/**
		 *
		 * List of the items
		 *
		 * @param	void
		 * @return	array
		 * @see		items
		**/
		public function items ( )
		{
			return $this->items;
		}


		/**
		 *
		 * Count of the items
		 *
		 * @param	void
		 * @return	int
		 * @see		count
		**/
		public function count ( )
		{
			return sizeof( $this->items );
		}


		/**
		 *
		 * Clear the items
		 *
		 * @param	void
		 * @return	void
		 * @see		clear
		**/
		public function clear ( )
		{
			$this->items = array( );
		}


		/**
		 *
		 * Display the page
		 *
		 * @param	string	$page	Page alias
		 * @return	bool
		 * @see		display
		**/
		public function display ( $page = '404' )
		{
			// Access the Page Item object
			$item = $this->get( $page );

			// If the item failed to load
			if ( $item === false )
			{
				return false;
			}


			// Display the page
			return $item->display( );
		}



		/**
		 *
		 * Load main menu
		 *
		 * @param	void
		 * @return	bool
		 * @see		load_menus
		**/
		private function load_menus ( )
		{
			// Access database connection
			$db = &DB\DB::Get( $this->source );

			// Build SQL query to select menu items from the database
			$select = DB\SQL::Set("SELECT * FROM `page_menu` ORDER BY `order` ASC;");

			// If the query has failed
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there are no items
			if ( !$db->numrows( ) )
			{
				return true;
			}


			// Access mainmenu items list
			$items = $db->results( );
			$items->assoc('alias');

			// Iterate through each menu item
			foreach ( $items as $key => &$item )
			{
				// If the item already exists
				if ( $this->has( $key ) )
				{
					continue;
				}


				// Restore Item from the MySQL
				$item = &Item::Factory( $item );

				// If the Item failed to restore
				if ( $item === false )
				{
					continue;
				}


				// Insert new item into holder
				$this->add( $key, $item );
			}


			return true;
		}


		/**
		 *
		 * Load sub menu
		 *
		 * @param	void
		 * @return	bool
		 * @see		load_submenus
		**/
		private function load_submenus ( )
		{
			// Access database connection
			$db = &DB\DB::Get( $this->source );

			// Build SQL query to select menu items from the database
			$select = DB\SQL::Set("SELECT * FROM `page_submenu` ORDER BY `order` ASC;");

			// If the query has failed
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there are no items
			if ( !$db->numrows( ) )
			{
				return true;
			}


			// Access mainmenu items list
			$items = $db->results( );
			//$items->assoc('alias');

			// Iterate through each menu item
			foreach ( $items as &$item )
			{
				// Access the parent key
				$parent = $item->parent;
				$key = $item->alias;

				// If there is no such parent
				if ( !$this->has( $parent ) )
				{
					continue;
				}


				// Access parent item object
				$parent = $this->get( $parent );

				// If the item already exists
				if ( $parent->has( $key ) )
				{
					continue;
				}


				// Restore Item from the MySQL
				$item = &Item::Factory( $item );

				// If the Item failed to restore
				if ( $item === false )
				{
					continue;
				}


				// Insert new item into holder
				$parent->add( $key, $item );
			}


			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////

