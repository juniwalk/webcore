<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-03-31 00:55
	* @file:		Menu Item library
	* @copyright:	(c) 2012 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	MENU ITEM LIBRARY


	namespace WebCore\Web\Menu;


	use \WebCore\DB\Result;
	use \WebCore\Net;
	use \WebCore\User;
	use \WebCore\Web;


	final class Item
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Properties
		**/
		public $id			= null;
		public $order		= null;
		public $parent		= null;
		public $alias		= null;
		public $title		= null;
		public $icon		= null;
		public $url			= null;
		public $access		= 'user';
		public $cache		= false;
		public $active		= false;
		public $hidden		= false;
		public $disabled	= false;


		/**
		 * Holder for the items
		**/
		private $items	= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( ) { }



		/**
		 *
		 * Factory method
		 *
		 * @param	\WebCore\DB\Result\Row	$data		Item data
		 * @param	bool					$translate	Should the names be translated?
		 * @return	self
		 * @see		Factory
		**/
		public static function &Factory ( Result\Row &$data, $translate = true )
		{
			// New self instance
			$item = new self( );
			$item->items['self'] = &$item;

			// Iterate through each data property
			foreach ( $data as $key => $value )
			{
				// Assign new item value into property
				$item->assign( $key, $value, $translate );
			}


			// Create new URL object from current
			// URL and assign path to /index.php
			$url = Net\URL::Current( );
			//$url->path = "/index.php";
			$url->clear( );

			// Add name of the page to the query
			$url->set( 'page', $item->alias );

			// If there is an parent page
			if ( !empty( $item->parent ) )
			{
				$url->set( 'page', $item->parent );
				$url->set( 'view', $item->alias );
			}

			// If there is no Url override
			if ( empty( $item->url ) )
			{
				// Assign default Url
				$item->url = $url;
			}


			return $item;
		}


		/**
		 *
		 * Create Item from code
		 *
		 * @param	string	$alias		Page alias
		 * @param	string	$title		Page title
		 * @param	int		$role		Access role
		 * @param	string	$parent		Page parent
		 * @param	array	$params		Other params
		 * @return	bool
		 * @see		Create
		**/
		public static function Create ( $alias, $title, $role, $parent = null, array $params = array( ) )
		{
			// New self instance
			$item = new self( );
			$item->items['self'] = &$item;

			$values = array
			(
				'id'		=> null,
				'order'		=> 0,
				'parent'	=> $parent,
				'alias'		=> $alias,
				'title'		=> $title,
				'icon'		=> null,
				'url'		=> null,
				'access'	=> 'user',
				'cache'		=> null,
				'hidden'	=> null,
				'disabled'	=> null,
			);


			// Get the Access Role from UAC
			$access = User\UAC::Role( $role );

			// If the ROle was found
			if ( $access !== false )
			{
				// Assign alias to the Item
				$values['access'] = $access->alias;
			}

			// Iterate through each data property
			foreach ( $values as $key => $value )
			{
				// If there is value in the params holder
				if ( array_key_exists( $key, $params ) )
				{
					$value = $params[ $key ];
				}


				// Assign new item value into property
				$item->assign( $key, $value, true );
			}


			// Create new URL object from current
			// URL and assign path to /index.php
			$url = Net\URL::Current( );
			$url->path = "/index.php";
			$url->clear( );

			// Add name of the page to the query
			$url->set( 'page', $item->alias );

			// If there is an parent page
			if ( !empty( $item->parent ) )
			{
				$url->set( 'page', $item->parent );
				$url->set( 'view', $item->alias );
			}

			// If there is no Url override
			if ( empty( $item->url ) )
			{
				// Assign default Url
				$item->url = $url;
			}


			// Access Menu instance
			$page = &Menu::Factory( );

			// If there is parent defined
			if ( !is_null( $parent ) )
			{
				// Get the parent page first
				$page = $page->get( $parent );
			}

			// If there is no such page
			if ( $page === false )
			{
				return false;
			}


			// Insert the Item into Menu
			return $page->add( $alias, $item );
		}



		/**
		 *
		 * Register menu
		 *
		 * @param	string	$key		Item alias
		 * @param	Item	$item		Item instance
		 * @return	bool
		 * @see		add
		**/
		public function add ( $key, Item &$item )
		{
			// If the item is already present in holder
			if ( $this->has( $key ) )
			{
				return false;
			}


			// Assign Item to the holder
			$this->items[ $key ] = &$item;

			return true;
		}


		/**
		 *
		 * Does item exists?
		 *
		 * @param	string	$key		Item alias
		 * @return	bool
		 * @see		has
		**/
		public function has ( $key )
		{
			// Access the list of items
			$items = &$this->items;

			// If there are no items
			if ( empty( $items ) )
			{
				return false;
			}

			// If there is no such item in the list
			if ( !array_key_exists( $key, $items ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Does item exists?
		 *
		 * @param	string	$key	Item alias
		 * @return	Item
		 * @see		get
		**/
		public function get ( $key )
		{
			// If the re is no such item
			if ( !$this->has( $key ) )
			{
				return false;
			}


			// Access the item from holder
			$item = &$this->items[ $key ];

			return $item;
		}


		/**
		 *
		 * Delete the item
		 *
		 * @param	string	$key	Item alias
		 * @return	bool
		 * @see		delete
		**/
		public function delete ( $key )
		{
			// If the re is no such item
			if ( !$this->has( $key ) )
			{
				return false;
			}


			// Remove the Item from the holder
			unset( $this->items[ $key ] );

			return true;
		}


		/**
		 *
		 * List of the items
		 *
		 * @param	void
		 * @return	array
		 * @see		items
		**/
		public function items ( )
		{
			return $this->items;
		}


		/**
		 *
		 * Count of the items
		 *
		 * @param	void
		 * @return	int
		 * @see		count
		**/
		public function count ( )
		{
			return sizeof( $this->items );
		}


		/**
		 *
		 * Clear the items
		 *
		 * @param	void
		 * @return	void
		 * @see		clear
		**/
		public function clear ( )
		{
			$this->items = array( );
		}


		/**
		 *
		 * Display the page
		 *
		 * @param	void
		 * @return	bool
		 * @see		display
		**/
		public function display ( )
		{
			// Access menu object
			$menu = &Menu::Factory( );
			$user = &User\User::Factory( );

			// Idf the page is disabled
			if ( $this->is('disabled') )
			{
				return $menu->get('403')->display( );
			}

			// If the visitor does not have enough access to the page
			if ( !User\UAC::Authorize( $user, $this->access ) )
			{
				return $menu->get('403')->display( );
			}

			// If this item is currently active
			if ( $this->alias == $menu->page )
			{
				$this->active = true;
			}


			// Get the name of the template
			$tpl = str_replace( '.', '/', $this->alias ).".tpl";

			// If there is parent defined
			if ( !empty( $this->parent ) )
			{
				// Access the parent object from menu
				// and prepend its alias to template name
				$page = $menu->get( $this->parent );
				$tpl = "{$page->alias}/{$tpl}";
			}

			// Smarty does not exists so we can't continue
			if ( !class_exists( '\Smarty', false ) )
			{
				throw new \Exception("Template provider 'Smarty' not found");
			}


			// Get the instance of Smarty
			$smarty = &\Smarty::Factory( );

			// If the template does not exists
			if ( !$smarty->templateExists( $tpl ) )
			{
				return $menu->get('404')->display( );
			}


			// Assign Menu and Page objects
			$smarty->assign( 'menu', $menu );
			$smarty->assign( 'page', $this );

			// If there is Page object
			if ( isset( $page ) )
			{
				// Assign this object instead of child
				$smarty->assign( 'page', $page );
				$smarty->assign( 'view', $this );
			}

			// If the cache is disabled for current page
			if ( !$this->cache )
			{
				// Clear the cached content and
				// disable caching for this runtime
				$smarty->clearCache( $tpl );
				$smarty->caching = false;
			}

			// If we could not load model class
			if ( !$this->model( $tpl ) )
			{
				throw new \Excfeption("Unable to load template model class");
			}


			// Display the template to the user
			$smarty->display( $tpl );

			return true;
		}



		/**
		 *
		 * Assign variable to the page
		 *
		 * @param	string	$key	Variable name
		 * @param	mixed	$value	Contents
		 * @return	bool
		 * @see		variable
		**/
		public function variable ( $key, $value )
		{
			// If the key is not scalar
			if ( !is_scalar( $key ) )
			{
				return false;
			}

			// Smarty does not exists so we can't continue
			if ( !class_exists( '\Smarty', false ) )
			{
				return false;
			}


			// Get the instance of Smarty
			$smarty = &\Smarty::Factory( );
			$smarty->assign( $key, $value );

			return true;
		}


		/**
		 *
		 * Validate item status
		 *
		 * @param	void
		 * @return	bool
		 * @see		validate
		**/
		public function validate ( )
		{
			// If the item is hidden
			if ( $this->is('hidden') )
			{
				return false;
			}

			// If the item is disabled
			if ( $this->is('disabled') )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Check the item status
		 *
		 * @param	string	$key	Property name
		 * @return	bool
		 * @see		is
		**/
		public function is ( $key )
		{
			// Access Menu object
			$menu = &Menu::Factory( );
			$page = $menu->page;	// Current page
			$view = $menu->view;	// Current view

			// Switch the wanted key name
			switch ( strToLower( $key ) )
			{
				case 'cache':
				case 'hidden':
				case 'disabled':

					// Access the value of the key
					if ( (bool) $this->$key )
					{
						return true;
					}

				break;

				case 'active':

					// If this item is currently active
					if ( $this->alias == $page )
					{
						return true;
					}

				break;

				case 'subactive':

					// If the child item is active
					if ( $this->alias == $view )
					{
						return true;
					}

					// If the child is a main page and there is no child
					if ( $this->alias == $page AND empty( $view ) )
					{
						return true;
					}

				break;
			}


			return false;
		}



		/**
		 *
		 * Load page model file
		 *
		 * @param	string	$tpl	Temlate name
		 * @return	bool
		 * @see		model
		**/
		private function model ( $tpl )
		{
			// Get the instance of Smarty and
			// register model class to the tpl
			$smarty = &\Smarty::Factory( );

			// if the template is being cached
			if ( $smarty->isCached( $tpl ) )
			{
				return true;
			}


			// Get the template name without extension and
			// create name of the model class out of it
			$class = substr( $tpl, 0, strrpos( $tpl, '.' ) );
			$class = str_replace( "/", '\\', $class );
			$class = str_replace( ".", '\\', $class );
			$class = "WebApp\Models\\{$class}";

			// If no such class was found
			if ( !class_exists( $class ) )
			{
				// Fallback to the global model
				$class = "WebCore\Web\Model";
			}


			// Create instance of the class and assign
			// it to the template engine for use in view
			$smarty->assign( 'model', new $class( ) );

			return true;
		}


		/**
		 *
		 * Register menu
		 *
		 * @param	string	$key		Item property
		 * @param	mixed	$value		Property value
		 * @param	bool	$translate	Enable translations?
		 * @return	bool
		 * @see		assign
		**/
		private function assign ( $key, $value, $translate = true )
		{
			// Set the key to lower case
			$key = strToLower( $key );

			// If the property does not exists in object
			if ( !property_exists( $this, $key ) )
			{
				return false;
			}


			// Access language instance
			$lang = &Web\Lang::Factory( );

			switch ( $key )
			{
				case 'id':
				case 'order':

					// Set type to the integer
					$value = (int) $value;

				break;

				case 'cache':
				case 'hidden':
				case 'disabled':

					// Set type to the boolean
					$value = (bool) $value;

				break;

				case 'url':

					// TODO: Convert string URLs to objects

				break;

				default:

					// Set the type to the string
					$value = (string) $value;

					// If the value is empty
					if ( empty( $value ) )
					{
						$value = null; break;
					}

					// If the string has been translated
					if ( $lang->translated( $value ) AND $translate )
					{
						// Try to translate the string
						$value = $lang->X( $value );
					}

				break;
			}


			// Assign value in desired type
			$this->$key = $value;

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////

