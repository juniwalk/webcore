<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-12-5 15:23
	* @file:		Pager plugin
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	PAGER PLUGIN


	namespace WebCore\Web;


	use \WebCore\Net;
	use \WebCore\Web\Menu\Menu;
	use \WebCore\Web\Pager;
	use \WebCore\User\User;


	trait Pager
	{
		/**
		 * Properties
		**/
		protected $__page		= null;		// Current page
		protected $__pages		= null;		// Number of pages [ ceil( count / perpage ) ]
		protected $__perpage	= null;		// Number of items per page
		protected $__count		= null;		// Total count


		/**
		 * Possible values for perpage selector
		**/
		protected $__list		= "10,25,50,75,*";



		/**
		 *
		 * Pager cookie name
		 *
		 * @param	void
		 * @return	string
		 * @see		getPagerCookie
		**/
		protected function getPagerCookie ( )
		{
			// Make sure value is cached
			static $cookie = null;

			// If there is no name yet
			if ( !isset( $cookie ) )
			{
				// Access current menu item
				$cookie = Menu::Current( )->alias;
			}


			return $cookie;
		}


		/**
		 *
		 * <<init>> Pager
		 *
		 * @param	void
		 * @return	array
		 * @see		__pager
		**/
		protected function __pager ( )
		{
			// Get current user instance
			$user = User::Factory( );
			$cookie = $this->getPagerCookie( );
			$perpage = null;	// Empty holder

			// If the parent class does not extends Model
			if ( !$this instanceof Model )
			{
				throw new \RuntimeException("Pager trait can only be used in the \WebCore\Web\Model childs");
			}

			// If the parent class does not implement Pager Model
			if ( !$this instanceof Pager\Model )
			{
				throw new \RuntimeException("Pager trait need parent class to implement \WebCore\Web\Pager\Model interface");
			}

			// If there is page selected
			if ( !Request::Query( '__page', $this->__page ) )
			{
				// Go to page one
				$this->__page = 1;
			}

			// If there is cookei saved with value
			if ( $user->has( $cookie, "pager" ) )
			{
				// Load current value from the cookie
				$perpage = (int) $user->get( $cookie, "pager" );
			}

			// If there is no value, use the ALL value
			// FIX: 'All' option was not properly saved
			if ( $perpage == 0 )
			{
				// Make it "star"
				$perpage = '*';
			}


			// Get the perpage value from the request query
			Request::Query( '__perpage', $this->__perpage, $perpage );

			// If there is no value from the request
			if ( empty( $this->__perpage ) )
			{
				// Get default number of per page items
				$this->__perpage = current(explode(',',$this->__list));
			}


			// Access values needed to compute limit
			$perpage = (int) $this->__perpage;
			$page = (int) $this->__page;

			if ( $perpage != $user->get( $cookie, "pager" ) )
			{
				// Save the value for later
				$user->set( $cookie, $perpage, "pager", true );
			}

			// If there is no value for perpage
			// item limiter - means we want all
			if ( empty( $perpage ) )
			{
				// Get the total count of items
				$perpage = $this->count( );
			}

			// If there are more then zero items per page
			// FIX: Division by zero error in soem cases
			if ( empty( $perpage ) )
			{
				// Get default number of per page items
				$perpage = current(explode(',',$this->__list));
			}


			// Get total number of entries and compute number of pages
			$this->__count = $this->count( );
			$this->__pages = ceil( $this->__count / $perpage );

			// If the pages has incorrect value
			if ( $page <= 0 )
			{
				// Go to default page
				$this->__page = 1;
			}
		}


		/**
		 *
		 * Access base URL
		 *
		 * @param	void
		 * @return	URL
		 * @see		__url
		**/
		protected function __url ( )
		{
			// Access base URL address
			$base = $this->url( );
			$base->remove('notify');

			// Set default base URL values for pagination
			$base->set( '__perpage', $this->__perpage );
			$base->set( '__page', $this->__page );

			// Retrurn base URL
			return $base;
		}


		/**
		 *
		 * Get list of URL to pages
		 *
		 * @param	void
		 * @return	array
		 * @see		__pages
		**/
		protected function __pages ( )
		{
			// Pages holder and current page
			$holder = array( );			// Pages holder
			$pages = $this->__pages;	// Number of pages
			$page = $this->__page;		// Current page
			$base = $this->__url( );	// Base URL

			// Create [First] and [Previous] links and insert them into the pagination holder
			$holder['first'] = $this->__page( $base, "PAGER_FIRST",	1,			false, $page <= 1 );
			$holder['prev'] = $this->__page( $base, "PAGER_PREV",	$page -1,	false, $page <= 1 );

			// Create left side of buttons around current page
			for ( $i = ( $page - 5 ); $i < $page; $i++ )
			{
				// If the page index is
				// outside the page range
				if ( $i < 1 )
				{
					// Skip
					continue;
				}


				// Insert single page into the pagination holder
				$holder[$i] = $this->__page( $base, $i, $i );
			}


			// Insert current page as actvie link into the pagination holder
			$holder[$page] = $this->__page( $base, $page, $page, true );

			// Create right side of buttons around current page
			for ( $i = ( $page + 1 ); $i < ( $page + 1 + 5 ); $i++ )
			{
				// If the page index is
				// outside the page range
				if ( $i > $pages )
				{
					// Skip
					continue;
				}


				// Insert single page into the pagination holder
				$holder[$i] = $this->__page( $base, $i, $i );
			}


			// Create [Last] and [Next] links and insert them into the pagination holder
			$holder['next'] = $this->__page( $base, "PAGER_NEXT",	$page +1,	false, $page >= $pages );
			$holder['last'] = $this->__page( $base, "PAGER_LAST",	$pages,		false, $page >= $pages );

			// Return list
			return $holder;
		}


		/**
		 *
		 * Creates link for pager (used in smarty)
		 *
		 * @param	Url		$base		Base link
		 * @param	string	$title		Title of the button
		 * @param	int		$page		Url object
		 * @param	bool	$active		Is link active?
		 * @param	bool	$disabled	Is link disabled?
		 * @return	stdClass
		 * @see		__page
		**/
		protected function __page ( Net\Url &$base, $title, $page, $active = false, $disabled = false )
		{
			// Make sure that those variables
			// are in the boolean format type
			$active = (bool) $active;
			$disabled = (bool) $disabled;

			// Insert the page index into Url
			$url = clone( $base );
			$url->set( '__page', $page );

			// Return the page link object
			return (object) array
			(
				'page'		=> $page,		// Page index
				'title'		=> $title,		// Link title
				'url'		=> $url,		// Link URL
				'active'	=> $active,		// Link active?
				'disabled'	=> $disabled,	// Link disabled?
			);
		}


		/**
		 *
		 * Get the list of size per page
		 *
		 * @param	void
		 * @return	array
		 * @see		__perpage
		**/
		protected function __perpage ( )
		{
			// Create list with possible number of items per page
			$holder = explode( ',', $this->__list );
			$items = array( );

			// Get the base URL address
			// and set default page to first
			$base = $this->__url( );
			$base->set( '__page', 1 );

			// Walk through each entry in holder
			foreach ( $holder as $item )
			{
				// PArse entry name into value
				$value = intval( $item );
				$title = (string) $item;

				// Set the value for the base URL
				$url = clone( $base );
				$url->set( '__perpage', $item );

				// If the item is 'all'
				if ( $item == '*' )
				{
					// Insert language anchor
					$title = "PAGER_ALL";
					$value = $item;
				}

				// Build item object from fata
				$items[ ] = (object) array
				(
					'title'		=> $title,
					'value'		=> $value,
					'url'		=> $url,
					'active'	=> $this->__perpage == $item,
				);
			}


			// Return list
			return $items;
		}


		/**
		 *
		 * Get limit for SQL
		 *
		 * @param	void
		 * @return	array
		 * @see		__limit
		**/
		protected function __limit ( )
		{
			// Access values needed to compute limit
			$perpage = (int) $this->__perpage;
			$page = (int) $this->__page;

			// If there is no value for perpage
			// item limiter - means we want all
			if ( $perpage == 0 )
			{
				// Get the total count of items
				$perpage = $this->count( );
			}

			// If we have failed to get the count of rows
			// FIX: Division by zero error in soem cases
			if ( empty( $perpage ) )
			{
				// Get default number of per page items
				$perpage = current(explode(',',$this->__list));
			}


			return array
			(
				( $perpage * ( $page -1 ) ),	// Offset
				$perpage,						// Number of rows
			);
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

