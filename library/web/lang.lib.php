<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-04-22 22:01
	* @file:		Lang library
	* @copyright:	(c) 2012 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	LANG LIBRARY


	namespace WebCore\Web;


	use \WebApp;
	use \WebCore\Core\Exceptions;
	use \WebCore\Data;
	use \WebCore\User;


	final class Lang
	{
		/**
		 * Version holder
		**/
		const VERSION		= 2;


		/**
		 * Paths
		**/
		private $paths		= array( );


		/**
		 * Language
		**/
		private $locale		= null;
		private $default	= 'cs_cz';


		/**
		 * Values
		**/
		private $values		= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( )
		{
			// Create instance of the WebApp config
			$config = &WebApp\Config::Factory( );

			// If there is default language defined in config
			if ( property_exists( $config, 'PAGE_LANG' ) )
			{
				// Set new default language from config
				$this->default = $config->PAGE_LANG;
			}


			// Assign paths as sources for languages
			$this->paths[] = WEBCORE."/language";
			$this->paths[] = WEBAPP."/language";
		}


		/**
		 *
		 * Factory
		 *
		 * @param	string	$alias	Aias of the page
		 * @return	self
		 * @see		Factory
		**/
		public static function &Factory ( )
		{
			// Instance holder
			static $class = null;

			// If there is no instance
			if ( is_null( $class ) )
			{
				// Create new instance and
				// preload primary language
				$class = new self( );
			}


			return $class;
		}


		/**
		 *
		 * Add language source path
		 *
		 * @param	string	$path	Path to folder
		 * @return	bool
		 * @see		Path
		**/
		public static function Path ( $path )
		{
			// Access Lang instance
			$self = &self::Factory( );

			// If the path does not lead to directory
			if ( !is_dir( $path ) )
			{
				return false;
			}

			// If the path is already included
			if ( in_array( $path, $self->paths ) )
			{
				return false;
			}


			// Insert the path into holder
			$self->paths[] = $path;

			return true;
		}


		/**
		 *
		 * List of available languages
		 *
		 * @param	void
		 * @return	array
		 * @see		Available
		**/
		public static function Available ( )
		{
			// Cached data holder
			static $langs = array( );

			// If there are data cached
			if ( !empty( $langs ) )
			{
				return $langs;
			}


			// Load list of available locales in the application
			$langs = glob( WEBAPP."/language/*", GLOB_ONLYDIR );
			$langs = array_unique( $langs );

			// If no langs were found
			if ( empty( $langs ) )
			{
				return array( );
			}

			// Iterate through each language
			foreach ( $langs as &$lang )
			{
				// Get only locale name instead of full language path
				$lang = substr( $lang, strrpos( $lang, '/' )+1 );
			}


			return $langs;
		}


		/**
		 *
		 * Set locale
		 *
		 * @param	string	$key	Set new locale
		 * @return	bool
		 * @see		Set
		**/
		public static function Set ( $key )
		{
			// Access current time
			$time = time( );

			// If the lang should be cleared
			if ( empty( $key ) )
			{
				$time = 0;
			}


			// Try to set new cookie with the locale name
			return Request::Cookie( 'locale', $key, $time );
		}



		/**
		 *
		 * <<magic>> Getter
		 *
		 * @param	string	$key	User property
		 * @return	mixed
		 * @see		__get
		**/
		final public function __get ( $key )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such property of the User
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}


			return $this->$key;
		}


		/**
		 *
		 * Get the translation
		 *
		 * @param	string	$key	String key
		 * @return	string
		 * @see		X
		**/
		public function X ( $key )
		{
			// Access list of all params
			$params = func_get_args( );

			// If the key is empty
			if ( empty( $key ) )
			{
				return null;
			}

			// If the key is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}


			// Make the key uppercased
			$key = strToUpper( $key );

			// If the key does not have translation
			if ( !$this->translated( $key ) )
			{
				return $key;
			}


			// Access the translation by key
			$value = $this->values[ $key ];

			// If the value is package
			// yet of another values
			if ( is_array( $value ) )
			{
				return $value;
			}

			// If there are no params
			if ( $params <= 1 )
			{
				return $value;
			}



			// Rewrite the param
			$params[0] = $value;

			// Call the spintf function with given params
			return call_user_func_array( "sprintf", $params );
		}


		/**
		 *
		 * Get the translation
		 *
		 * @param	string	$key		String key
		 * @param	string	$default	Default value
		 * @return	string
		 * @see		Y
		**/
		public function Y ( $key, $default = null )
		{
			// Access list of all params
			$params = func_get_args( );

			// If the key is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}


			// Make the key uppercased
			$key = strToUpper( $key );

			// If there is no defautl value
			if ( is_null( $default ) )
			{
				// Use the key instead
				$default = $key;
			}


			// Remove default value
			// from function params
			unset( $params[1] );

			// If the key does not have translation
			if ( !$this->translated( $key ) )
			{
				return $default;
			}


			// Access the translation by key
			$value = $this->values[ $key ];

			// If there are no params
			if ( $params <= 2 )
			{
				return $value;
			}


			// Rewrite the param
			$params[0] = $value;

			// Remove the default key
			unset( $params[1] );

			// Call the spintf function with given params
			return call_user_func_array( "sprintf", $params );
		}


		/**
		 *
		 * Get the translation
		 *
		 * @param	string	$key	String key
		 * @return	string
		 * @see		Z
		**/
		public function Z ( $key )
		{
			// If the key is empty
			if ( empty( $key ) )
			{
				return null;
			}

			// If the key is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}


			// Make the key uppercased
			$key = strToUpper( $key );

			// If the key does not have translation
			if ( !$this->translated( $key ) )
			{
				return $key;
			}


			// Access the translation by key
			$value = $this->values[ $key ];

			// If the value is package
			// yet of another values
			if ( is_array( $value ) )
			{
				return $value;
			}


			return $value;
		}


		/**
		 *
		 * Is the key translated?
		 *
		 * @param	string	$key	String key
		 * @return	bool
		 * @see		translated
		**/
		public function translated ( $key )
		{
			// If the key is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}


			// Make the key uppercased
			$key = strToUpper( $key );

			// if there is no entry for given translation key
			if ( !array_key_exists( $key, $this->values ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Detect locale from browser
		 *
		 * @param	void
		 * @return	bool
		 * @see		detect
		**/
		public function detect ( )
		{
			// If there is no language folder
			if ( !is_dir( WEBAPP."/language" ) )
			{
				return true;
			}


			// Access the locale name in the query
			Request::Query( 'lang', $key, null );

			// If locale key was found
			if ( !empty( $key ) )
			{
				// Load language for it
				return $this->locale( $key );
			}


			// Access the locale name in the cookie
			$key = User\Browser::Cookie('locale');

			// If locale key was found
			if ( !empty( $key ) )
			{
				// Load language for it
				return $this->locale( $key );
			}


			// Create instance of the WebApp config
			$config = WebApp\Config::Factory( );

			// If auto-detection is disabled
			if ( !$config->PAGE_LANG_DETECT )
			{
				// Load default page locale
				return $this->locale( $this->default );
			}


			// Get the list of supported languages
			$langs = User\Browser::Languages( );

			// Iterate through each language
			foreach ( $langs as $lang )
			{
				// Try to load language strings
				$state = $this->locale( $lang );

				// If the language was loaded
				if ( $state === true )
				{
					return true;
				}
			}


			// Load default language
			return $this->locale( $this->default );
		}


		/**
		 *
		 * Load locale
		 *
		 * @param	string	$key	Locale key
		 * @return	bool
		 * @see		locale
		**/
		public function locale ( $key )
		{
			// Get list of available locales
			$locales = self::Available( );

			// If the key is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}


			// Load list of the translations
			$files = $this->files( $key );

			// if there are no translations
			if ( empty( $files ) )
			{
				return false;
			}

			// Iterate through list of files
			foreach ( $files as $file )
			{
				// Load each file
				$this->load( $file );
			}


			// get the path from last file
			$path = dirname( $file );

			// If there are still no values
			if ( empty( $this->values ) )
			{
				return false;
			}


			// Get only locale name instead of full language path
			$lang = substr( $path, strrpos( $path, '/' )+1 );

			// If the locale is not supported
			if ( !in_array( $lang, $locales ) )
			{
				return false;
			}


			// Assign current locale
			$this->locale = $lang;

			// Try to set internal PHP locale
			if ( !setlocale( LC_ALL, $lang ) )
			{
				// what to do?
			}


			return true;
		}


		/**
		 *
		 * List of translation files
		 *
		 * @param	string	$locale		Locale key
		 * @return	array
		 * @see		files
		**/
		public function files ( $locale )
		{
			// If the key is not a string
			if ( !is_string( $locale ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $locale, 1 );
			}


			// Make the locale lower cased
			$locale = strToLower( $locale );

			// If there are no sources
			if ( empty( $this->paths ) )
			{
				return array( );
			}


			// Temporary holder
			$paths = array( );

			// iterate through each path
			foreach ( $this->paths as $path )
			{
				// Look for the files matching selected locale on harddrive
				$files = glob("{$path}/{$locale}*/{$locale}*.*.ini");

				// If there are no such files
				if ( empty( $files ) )
				{
					continue;
				}


				// Merge the path to the files into paths
				$paths = array_merge( $paths, $files );
			}


			// We don't want to load same file twice
			$paths = array_unique( $paths );

			// If no files were found
			if ( empty( $paths ) )
			{
				return array( );
			}


			return $paths;
		}


		/**
		 *
		 * Load INI strings
		 *
		 * @param	string	$file	Path to the file
		 * @return	bool
		 * @see		load
		**/
		private function load ( $file )
		{
			// Load and parse values from file into array
			$assets = Data\INI::Read( $file, true );

			// If parsing the file failed
			if ( $assets === false )
			{
				return false;
			}

			// Iterate trough each dimension and add translation
			foreach ( $assets as $section	=> $values )	// Section
			foreach ( $values as $key		=> $value )		// Values
			{
				// Prepend section name into key uppercase
				$key = strToUpper("{$section}_{$key}");

				// If the value is already defined
				if ( $this->translated( $key ) )
				{
					continue;
				}


				// Save the value into the holder
				$this->values[ $key ] = $value;
			}


			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

