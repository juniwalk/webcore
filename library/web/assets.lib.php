<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-6-22 17:42
	* @file:		Assets library
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ASSETS LIBRARY


	namespace WebCore\Web;


	// Assetic
	use Assetic\AssetManager;
	use Assetic\AssetWriter;
	use Assetic\Cache\FilesystemCache;
	use Assetic\Factory\AssetFactory;
	use Assetic\Factory\Worker\CacheBustingWorker;

	// Asset Types
	use Assetic\Asset\AssetCache;
	use Assetic\Asset\FileAsset;
	use Assetic\Asset\GlobAsset;
	use Assetic\Asset\HttpAsset;
	use Assetic\Asset\StringAsset;

	// Assetic Filters
	use Assetic\FilterManager;
	use Assetic\Filter\GoogleClosure\CompilerApiFilter;
	use Assetic\Filter\GoogleClosure\CompilerJarFilter;
	use Assetic\Filter\LessphpFilter;
	use Assetic\Filter\Sass\SassFilter;
	use Assetic\Filter\Yui\CssCompressorFilter;
	use Assetic\Filter\Yui\JsCompressorFilter;


	class Assets extends AssetFactory
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Paths
		**/
		const JAVA				= "/usr/bin/java";
		const YUI_COMPRESSOR	= "/srv/htdocs/dp/webcore/plugins/yuicompressor-2.4.8.jar";
		const GC_COMPILER		= "/srv/htdocs/dp/webcore/plugins/compiler.jar";


		/**
		 * Types
		**/
		const FILE				= "file";		// FileAsset
		const HTTP				= "http";		// HttpAsset
		const GLOB				= "glob";		// GlobAsset
		const CONTENT			= "content";	// StringAsset
		const BUNDLE			= "bundle";		// Bundle of Assets


		/**
		 * Properties
		**/
		private $assets			= [ ];


#region Magic methods


		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( )
		{
			// Call AssetFactory constructor
			parent::__construct( WEBAPP, false );

			// Create new instances of needed libraries
			$this->setAssetManager( new AssetManager( ) );
			$this->setFilterManager( new FilterManager( ) );
			$this->addWorker( new CacheBustingWorker( ) );

			// If we were unable to create set of available filters
			if ( !$this->prepareFilters( ) )
			{
				throw new \RuntimeException("{$this}: Unable to assign default filters");
			}
		}


		/**
		 *
		 * <<magic>> To string converter
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			return get_class( $this );
		}


		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__destruct
		**/
		public function __destruct ( ) { }


#endregion

#region Instance


		/**
		 *
		 * Factory new instance
		 *
		 * @param	bool	$clear	Load clear instance
		 * @return	self
		 * @see		Factory
		**/
		public static function &Factory ( $clear = false )
		{
			// Instance holder
			static $self = null;

			// If there is no instance yet or
			// we need clear instance of this class
			if ( is_null( $self ) OR $clear )
			{
				// Create new instance
				$self = new static( );
			}


			return $self;
		}


#endregion

#region Methods


		/**
		 *
		 * Add new asset
		 *
		 * @param	string	$type	Type of the asset
		 * @param	string	$asset	Asset's source
		 * @return	bool
		 * @see		add
		**/
		public function add ( $type, $asset )
		{
			// Create alias name for the asset
			$name = substr( sha1( $asset ), 0, 7 );

			// If we already have such asset in the collection
			if ( array_key_exists( $name, $this->assets ) )
			{
				return false;
			}


			// Access AssetManager instance
			$am = $this->getAssetManager( );

			// Switch the type of the asset
			switch ( strToLower( $type ) )
			{
				// FileAsset
				case static::FILE:

					// Create Asset instance
					$asset = new FileAsset( $asset );

				break;

				// GlobAsset
				case static::GLOB:

					// Create Asset instance
					$asset = new GlobAsset( $asset );

				break;

				// HttpAsset
				case static::HTTP:

					// Create Asset instance
					$asset = new HttpAsset( $asset );

				break;

				// StringAsset
				case static::CONTENT:

					// Create Asset instance
					$asset = new StringAsset( $asset );

				break;

				// Bundle of Assets
				case static::BUNDLE:

					throw new \NotImplementedException("{$this}: Inserting bundles is not implemented yet");

				break;

				// Unknown asset type
				default: return false;
			}


			// Add asset to the collection
			$this->assets[ $name ] = "@{$name}";
			$am->set( $name, $asset );

			return true;
		}


		/**
		 *
		 * Set type of assets
		 *
		 * @param	string	$output		Output type
		 * @return	bool
		 * @see		setType
		**/
		public function setType ( $output )
		{
			// Define possible output types
			static $outputs = [ 'css', 'js' ];

			// If the output type is unknown
			if ( !in_array( $output, $outputs ) )
			{
				return false;
			}


			// Set the output type for this instance
			$this->setDefaultOutput("assets/*.{$output}");

			return true;
		}


		/**
		 *
		 * Compile assets
		 *
		 * @param	string	$path		Build path
		 * @param	array	$filters	List of filters
		 * @return	string
		 * @see		compile
		**/
		public function compile ( $path, array $filters = [ ] )
		{
			// Get the realpath for the output
			$output = realpath( $path );

			// Output path is invalid
			if ( $output === false )
			{
				return false;
			}

			// There are no Assets to write
			if ( empty( $this->assets ) )
			{
				return false;
			}


			// Create Asset collection for further compilation and
			// build target path using output folder and name of asset
			$assets = $this->createAsset( $this->assets, $filters );
			$target = "{$path}/".$assets->getTargetPath( );

			// If target file already exits
			if ( is_file( $target ) )
			{
				return $target;
			}

			try
			{
				// Prepare the cache for writing into file
				$cache = new FileSystemCache( $output );
	            $cache = new AssetCache( $assets, $cache );

				// Prepare the assets writer and then
				// try to write compiled asset into file
				$writer = new AssetWriter( $output );
				$writer->writeAsset( $cache );
			}

			// Catch the exception thrown
			catch ( \RuntimeException $e )
			{
				return false;
			}


			return $target;
		}


#endregion

#region Helpers


		/**
		 *
		 * Create set of filters
		 *
		 * @param	void
		 * @return	bool
		 * @see		prepareFilters
		**/
		private function prepareFilters ( )
		{
			// Access FilterManager from Factory
			$fm = $this->getFilterManager( );

			// We were unable to get FilterManager
			if ( !is_object( $fm ) )
			{
				return false;
			}


			// Create set of available filters
			$fm->set( 'yui_js', new JsCompressorFilter( static::YUI_COMPRESSOR ) );
			$fm->set( 'yui_css', new CssCompressorFilter( static::YUI_COMPRESSOR ) );
			$fm->set( 'less', new LessphpFilter( ) );
			$fm->set( 'closure_api', new CompilerApiFilter( ) );
			$fm->set( 'closure_jar', new CompilerJarFilter( static::GC_COMPILER ) );

			return true;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

