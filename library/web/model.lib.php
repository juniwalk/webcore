<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-30 15:59
	* @file:		Model library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	MODEL LIBRARY


	namespace WebCore\Web;


	use \WebCore;
	use \WebCore\Net\URL;


	class Model
	{
		/**
		 * Version holder
		**/
		const VERSION	= 1;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( )
		{
			// Access Page/View from request
			$page = $this->request('page');
			$view = $this->request('view');

			// If there are no request data available
			if ( is_null( $page ) AND is_null( $view ) )
			{
				return null;
			}


			// Access current URI
			$uri = $this->getCurrentURL( );

			// If the page or the view does not match current URI parameters
			if ( $uri->get('page') != $page OR $uri->get('view') != $view )
			{
				// Clear the request properties
				$_SESSION['request'] = array( );
			}
		}


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		final public function __toString ( )
		{
			return get_called_class( );
		}


		/**
		 *
		 * Get notification
		 *
		 * @param	bool	$gettype	Get only type
		 * @return	string
		 * @see		notify
		**/
		final public function notify ( $gettype = false, $notify = null, array $params = null )
		{
			// If there is no key override
			if ( empty( $notify ) )
			{
				// Access notification value from request
				Request::Query( 'notify', $notify );
			}

			// If there is no notification
			if ( empty( $notify ) )
			{
				return null;
			}

			// If there is type override for this notification
			if ( preg_match( '/\_(success|warning|danger|info|error|scs|war|err|tip)$/i', $notify, $type ) )
			{
				// Remove the type from the notification key
				$notify = substr( $notify, 0, strrpos( $notify, '_' ) );
			}

			// If the gettype is empty
			if ( is_null( $gettype ) )
			{
				return strToUpper( $notify );
			}

			// If we want only the type
			if ( $gettype == true )
			{
				// Return the type in the lowercase
				return @strToLower( $type[1] );
			}


			// Access Language instance for notification
			return Lang::Factory( )->X( $notify, $params );
		}


		/**
		 *
		 * Is there an error?
		 *
		 * @param	string	$pattern	Pattern to match
		 * @return	bool
		 * @see		IsError
		**/
		final public function IsError ( $pattern )
		{
			// If there is no notification to match to pattern
			if ( !Request::Query( 'notify', $notify ) )
			{
				return false;
			}

			// If the pattern is not a string
			if ( !is_string( $pattern ) )
			{
				return false;
			}

			// If the notification does not match the pattern
			if ( !preg_match( $pattern, $notify ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Access to data holder
		 *
		 * @param	void
		 * @return	array
		 * @see		data
		**/
		public function data ( )
		{
			return null;
		}


		/**
		 *
		 * Alias of the model
		 *
		 * @param	void
		 * @return	string
		 * @see		alias
		**/
		public function alias ( )
		{
			return str_replace( "\\", "_", $this );
		}


		/**
		 *
		 * Access data from request
		 *
		 * @param	string	$key	Request key
		 * @return	mixed
		 * @see		query
		**/
		public function query ( $key )
		{
			// If there is no such key in request
			if ( !Request::Query( $key, $value ) )
			{
				return null;
			}


			return $value;
		}


		/**
		 *
		 * Access data from previous request
		 *
		 * @param	string	$key		Request key
		 * @param	mixed	$default	Default value
		 * @return	string
		 * @see		request
		**/
		public function request ( $key, $default = null )
		{
			// If there is no request stored in the session
			if ( !array_key_exists( 'request', $_SESSION ) )
			{
				return $default;
			}


			// Copy the request to the variable
			$request = &$_SESSION['request'];

			// If there is no such key in the request holder
			if ( !array_key_exists( $key, $request ) )
			{
				return $default;
			}


			return $request[ $key ];
		}


		/**
		 *
		 * AJAX Processor
		 *
		 * @param	void
		 * @return	void
		 * @see		ajax
		**/
		protected function assign ( $key, $value )
		{
			\Smarty::Factory( )->assign( $key, $value );
		}


		/**
		 *
		 * AJAX Processor
		 *
		 * @param	void
		 * @return	void
		 * @see		ajax
		**/
		protected function ajax ( )
		{
			return null;
		}


		/**
		 *
		 * Get current URL
		 *
		 * @param	void
		 * @return	URL
		 * @see		getCurrentURL
		**/
		public function getCurrentURL ( $bare = false )
		{
			// Get current URL address
			$url = URL::Current( );

			// If we want only bare url
			if ( $bare == false )
			{
				return $url;
			}


			// Remove unneccessary values
			$url->path = null;
			$url->query = [ ];
			$url->fragment = null;

			return $url;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

