<?php return array (
  'sans-serif' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'times' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'times-roman' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'courier' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'helvetica' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'zapfdingbats' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold_italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
  ),
  'symbol' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Symbol',
    'bold' => DOMPDF_FONT_DIR . 'Symbol',
    'italic' => DOMPDF_FONT_DIR . 'Symbol',
    'bold_italic' => DOMPDF_FONT_DIR . 'Symbol',
  ),
  'serif' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'monospace' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'fixed' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'dejavu sans' =>
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSans-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSans-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSans-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSans',
  ),
  'dejavu sans light' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSans-ExtraLight',
  ),
  'dejavu sans condensed' =>
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed',
  ),
  'dejavu sans mono' =>
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSansMono',
  ),
  'dejavu serif' =>
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSerif-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-BoldItalic',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-Italic',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSerif',
  ),
  'dejavu serif condensed' =>
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-BoldItalic',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Italic',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed',
  ),
  'tahoma' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'tahoma_0',
    'bold' => DOMPDF_FONT_DIR . 'tahomabd_0',
    'italic' => DOMPDF_FONT_DIR . 'tahomabd_0',
    'bold_italic' => DOMPDF_FONT_DIR . 'tahomabd_0',
  ),
  'segoe ui' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'segoeui',
    'bold' => DOMPDF_FONT_DIR . 'segoeuib',
    'italic' => DOMPDF_FONT_DIR . 'segoeuii',
    'bold_italic' => DOMPDF_FONT_DIR . 'segoeuiz',
  ),
  'courier new' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'cour',
    'bold' => DOMPDF_FONT_DIR . 'courbd',
    'italic' => DOMPDF_FONT_DIR . 'couri',
    'bold_italic' => DOMPDF_FONT_DIR . 'courbi',
  ),
  'consolas' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'consola',
    'bold' => DOMPDF_FONT_DIR . 'consolab',
    'italic' => DOMPDF_FONT_DIR . 'consolai',
    'bold_italic' => DOMPDF_FONT_DIR . 'consolaz',
  ),
) ?>