<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-03-21 14:30
	* @file:		XRate library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	XRATE LIBRARY


	namespace WebCore\Utils;


	use \WebCore\IO;


	abstract class XRate
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Resource
		**/
		const SOURCE			= "http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt";
		const CACHE				= "/srv/htdocs/dp/webcore/cache/xrates.json";



#region Methods


		/**
		 *
		 * Convert value to different currency
		 *
		 * @param	float	$value		Value to be converted
		 * @param	string	$input		Input currency
		 * @param	string	$output		Output currency
		 * @return	float
		 * @see		Convert
		**/
		final public static function Convert ( $value, $input, $output )
		{
			// Get the rate for this currency conversion
			$rate = static::rate( $input, $output );

			// If the rate is empty
			if ( empty( $rate ) )
			{
				return false;

			}


			// Return converted value
			return round( $value * $rate, 2 );
		}


		/**
		 *
		 * Get the list of exchange rates
		 *
		 * @param	void
		 * @return	array
		 * @see		Rates
		**/
		final public static function Rates ( )
		{
			// Access the rates list
			static $rates = array( );

			// If there are no rates
			if ( empty( $rates ) )
			{
				// Load rates right now
				$rates = static::load( );
			}


			// Return the rates
			return $rates;
		}


#endregion

#region Helpers


		/**
		 *
		 * Get the conversion rate
		 *
		 * @param	string	$input		Input currency
		 * @param	string	$output		Output currency
		 * @return	float
		 * @see		rate
		**/
		final protected static function rate ( $input, $output )
		{
			// Access the list of rates
			$rates = static::Rates( );

			// If there are no rates
			if ( empty( $rates ) )
			{
				return false;
			}


			// Make the input currency lowecased
			$input = strToLower( $input );

			// Check that we support this currency
			if ( !array_key_exists( $input, $rates ) )
			{
				return false;
			}


			// Make the output currency lowecased
			$output = strToLower( $output );

			// Check that we support this currency
			if ( !array_key_exists( $output, $rates ) )
			{
				return false;
			}


			// Return conversion rate as a float number
			return $rates[ $input ] / $rates[ $output ];
		}


		/**
		 *
		 * Load exchange rates
		 *
		 * @param	void
		 * @return	array
		 * @see		load
		**/
		final protected static function load ( )
		{
			// Open source XML file and cached JSON file
			$source = IO\File::Open( static::SOURCE );
			$cache = IO\File::Create( static::CACHE );

			// If we could not open the source file and there
			// are cached data from before so we could use them
			if ( $source === false AND $cache->size > 0 )
			{
				// Decode the JSON cache file into array
				return json_decode( $cache->contents( ), true );
			}

			// Still in trouble
			if ( $source === false )
			{
				return array( );
			}

			// If the source is older than the cache file and cache is not empty
			if ( $source->modified < $cache->modified AND $cache->size > 0 )
			{
				// Decode the JSON cache file into array
				return json_decode( $cache->contents( ), true );
			}


			// Access the fields of the CSV file
			$fields = $source->fields( '|' );

			// Prepare holder
			$rates = array( );
			$rates['czk'] = 1;

			// Iterate through the list of fields
			foreach ( $fields as $key => $field )
			{
				// Skip firts two lines
				if ( $key <= 1 )
				{
					continue;
				}


				// Format the values from the XML file
				$key = strToLower( $field[3] );
				$value = str_replace( ',', '.', $field[4] );
				$value = floatval( $value );

				// Store them into holder
				$rates[ $key ] = $value;
			}

			// If there are no values
			if ( sizeof( $rates ) <= 1 )
			{
				return array( );
			}


			// Write the array into cache
			$cache->truncate( );
			$cache->write( $rates );

			return $rates;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

