<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-7-23 8:55
	* @file:		RUIAN library
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	RUIAN LIBRARY


	namespace WebCore\Utils;


	use \WebCore\DB\DB;
	use \WebCore\DB\SQL;


	abstract class RUIAN
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Accuracy level
		**/
		const DISTRICT		= 1;
		const TOWN			= 3;
		const TOWNPART		= 5;
		const STREET		= 7;
		const HOUSE			= 9;
		const ADDRESS		= 11;
		const CITYPART		= 17;


#region Setters


		/**
		 *
		 * RUIAN :: Get from Address
		 *
		 * @param	string		$house		House number
		 * @param	string		$street		Street name
		 * @param	string		$city		City name
		 * @return	array<int>
		 * @see		setAddress
		**/
		public static function setAddress ( $house, $street, $city )
		{
			// Remove any numbers from the end of the string
			$city = preg_replace( "/\s+\d+$/", "", $city );

			// If there is no house number or street number
			if ( empty( $house ) OR empty( $street ) )
			{
				// Get just the code for City
				return static::setCity( $city );
			}

			// If there is no oriental number of the hosue
			if ( !substr_count( $house, '/' ) )
			{
				// Insert the zero into it
				$house = "{$house}/0";
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("
SELECT
	`adm_kod` AS `code`
FROM
	`ruian_adm`
WHERE
	`obec_nazev` = '{$city}'
AND
(
	`ulice_nazev` = '{$street}'
	OR
	`cobce_nazev` = '{$street}'
)
AND CONCAT(`cp`, '/', `co`, `co_znak`) = '{$house}';
"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}

			return array
			(
				'level'	=> static::ADDRESS,
				'code'	=> (int) $address->code,
			);
		}


		/**
		 *
		 * RUIAN :: Get from City
		 *
		 * @param	string		$city		Town name
		 * @return	array<int>
		 * @see		setCity
		**/
		public static function setCity ( $city )
		{
			// Build the request SQL and execute it
			$address = static::query( SQL::Set("SELECT `obec_kod` AS `code` FROM `ruian_obec` WHERE `obec_nazev` = '{$city}';"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			return array
			(
				'level'	=> static::TOWN,
				'code'	=> (int) $address->code,
			);
		}


#endregion


#region Getters


		/**
		 *
		 * RUIAN :: Get Address from code
		 *
		 * @param	int		$code		Address code
		 * @return	array<string>
		 * @see		getAddress
		**/
		public static function getAddress ( $code )
		{
			// If the RUIAN Code is not integer
			if ( !is_integer( $code ) )
			{
				return false;
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("
SELECT
	CONCAT(a.`cp`, '/', a.`co`, `co_znak`) AS number,
	a.ulice_nazev AS street,
	a.obec_nazev AS city,
	a.cobce_nazev AS citypart,
	b.ku_nazev AS cadastral,
	a.psc AS zip
FROM
	`ruian_adm` AS a
LEFT JOIN ruian_ku AS b ON ( a.obec_kod = b.obec_kod )
WHERE
	a.adm_kod = {$code};
"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			// Access the address and remove additional number data
			$address->number = trim( $address->number, '/0' );

			// If there is no street name
			if ( empty( $address->street ) )
			{
				// Use the city as a street name
				$address->street = $address->city;
			}


			return array
			(
				'address'	=> "{$address->street} {$address->number}, {$address->city} {$address->zip}",
				'number'	=> $address->number,
				'street'	=> $address->street,
				'city'		=> $address->city,
				'citypart'	=> $address->citypart,
				'cadastral'	=> null, // $address->cadastral,
				'zip'		=> (int) $address->zip
			);
		}


		/**
		 *
		 * RUIAN :: Get Address from city code
		 *
		 * @param	int		$code		City code
		 * @return	array<string>
		 * @see		getCity
		**/
		public static function getCity ( $code )
		{
			// If the RUIAN Code is not integer
			if ( !is_integer( $code ) )
			{
				return false;
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("
SELECT
	a.obec_nazev AS city
FROM
	`ruian_obec` AS a
WHERE
	a.obec_kod = {$code};
"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			return $address->city;
		}



		/**
		 *
		 * RUIAN :: Get StreetID from code
		 *
		 * @param	int		$code		Address code
		 * @return	int
		 * @see		getStreetID
		**/
		public static function getStreetID ( $code, $level )
		{
			// If the given level is not supported
			if ( !in_array( $level, [ 11 ] ) )
			{
				return false;
			}

			// If the RUIAN Code is not integer
			if ( !is_integer( $code ) )
			{
				return false;
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("SELECT `ulice_kod` FROM `ruian_v_adm` WHERE `adm_kod` = {$code};"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			// Return the ID of the city
			return (int) $address->ulice_kod;
		}


		/**
		 *
		 * RUIAN :: Get CityID from code
		 *
		 * @param	int		$code		Address code
		 * @return	int
		 * @see		getCityID
		**/
		public static function getCityID ( $code, $level )
		{
			// If the given level is not supported
			if ( !in_array( $level, [ 3, 11 ] ) )
			{
				return false;
			}

			// If the level is 3
			if ( $level == 3 )
			{
				return $code;
			}

			// If the RUIAN Code is not integer
			if ( !is_integer( $code ) )
			{
				return false;
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("SELECT `obec_kod` FROM `ruian_adm` WHERE `adm_kod` = {$code};"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			// Return the ID of the city
			return (int) $address->obec_kod;
		}


		/**
		 *
		 * RUIAN :: Get CityPartID from code
		 *
		 * @param	int		$code		Address code
		 * @return	int
		 * @see		getCityPartID
		**/
		public static function getCityPartID ( $code, $level )
		{
			// If the given level is not supported
			if ( !in_array( $level, [ 11 ] ) )
			{
				return false;
			}

			// If the RUIAN Code is not integer
			if ( !is_integer( $code ) )
			{
				return false;
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("SELECT `cobce_kod` FROM `ruian_adm` WHERE `adm_kod` = {$code};"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			// Return the ID of the city
			return (int) $address->cobce_kod;
		}


		/**
		 *
		 * RUIAN :: Get DistrictID from code
		 *
		 * @param	int		$code		Address code
		 * @return	int
		 * @see		getDistrictID
		**/
		public static function getDistrictID ( $code, $level, $select = '`b`.`okres_kod`' )
		{
			// If the given level is not supported
			if ( !in_array( $level, [ 3, 11 ] ) )
			{
				return false;
			}


			// Column for ruian level 11
			$column = "`a`.`adm_kod`";

			// If the level is 3
			if ( $level == 3 )
			{
				// Use different column source
				$column = "`a`.`obec_kod`";
			}

			// If the RUIAN Code is not integer
			if ( !is_integer( $code ) )
			{
				return false;
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("
SELECT
	{$select}
FROM
	`ruian_adm` AS `a`
LEFT JOIN `ruian_v_okresy_cr` AS `b` ON
(
	`a`.`obec_kod` = `b`.`obec_kod`
	AND
	`a`.`cobce_kod` = `b`.`cobce_kod`
)
LEFT JOIN `ruian_okres` AS `c` ON ( `b`.`okres_kod` = `c`.`okres_kod` )
WHERE
	{$column} = {$code};"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			$address = (array) $address;

			// Return the ID of the city
			return (int) reset( $address );
		}


		/**
		 *
		 * RUIAN :: Get DistrictNUTS from code
		 *
		 * @param	int		$code		Address code
		 * @return	int
		 * @see		getDistrictNUTS
		**/
		public static function getDistrictNUTS ( $code, $level )
		{
			// If the given level is not supported
			if ( !in_array( $level, [ 3, 11 ] ) )
			{
				return false;
			}


			// Column for ruian level 11
			$column = "`a`.`adm_kod`";

			// If the level is 3
			if ( $level == 3 )
			{
				// Use different column source
				$column = "`a`.`obec_kod`";
			}

			// If the RUIAN Code is not integer
			if ( !is_integer( $code ) )
			{
				return false;
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("
SELECT
	`c`.`nuts_lau`
FROM
	`ruian_adm` AS `a`
LEFT JOIN `ruian_v_okresy_cr` AS `b` ON
(
	`a`.`obec_kod` = `b`.`obec_kod`
	AND
	`a`.`cobce_kod` = `b`.`cobce_kod`
)
LEFT JOIN `ruian_okres` AS `c` ON ( `b`.`okres_kod` = `c`.`okres_kod` )
WHERE
	{$column} = {$code};"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			// Return the ID of the city
			return $address->nuts_lau;
		}



		/**
		 *
		 * RUIAN :: Get DistrictID from code
		 *
		 * @param	int		$code		Address code
		 * @return	int
		 * @see		getRegionID
		**/
		public static function getRegionID ( $code, $level )
		{
			// If the given level is not supported
			if ( !in_array( $level, [ 3, 11 ] ) )
			{
				return false;
			}


			// Column for ruian level 11
			$column = "`a`.`adm_kod`";

			// If the level is 3
			if ( $level == 3 )
			{
				// Use different column source
				$column = "`a`.`obec_kod`";
			}

			// If the RUIAN Code is not integer
			if ( !is_integer( $code ) )
			{
				return false;
			}


			// Build the request SQL and execute it
			$address = static::query( SQL::Set("
SELECT
	`c`.`vusc_kod`
FROM
	`ruian_adm` AS `a`
LEFT JOIN `ruian_v_okresy_cr` AS `b` ON
(
	`a`.`obec_kod` = `b`.`obec_kod`
	AND
	`a`.`cobce_kod` = `b`.`cobce_kod`
)
LEFT JOIN `ruian_okres` AS `c` ON ( `c`.`okres_kod` = `b`.`okres_kod` )
WHERE
	$column = {$code};"));

			// If the request has failed
			if ( $address === false )
			{
				return false;
			}


			// Return the ID of the city
			return (int) $address->vusc_kod;
		}




#endregion

#region Helpers


		/**
		 *
		 * Execute SQL query on RUIAN database
		 *
		 * @param	SQL		$query	SQL query to exexute
		 * @return	Row
		 * @see		query
		**/
		private static function query ( SQL $query )
		{
			// Access instance of database
			$db = DB::Get('webcore');

			// We are not connected
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Select RUIAN database as source
			$db->database('dp_dev_ruian');

			// If the query has failed
			if ( !$db->query( $query ) )
			{
				return false;
			}


			// Assign the list of results
			$results = $db->results(0);

			// If there are no results
			if ( !$db->numrows( ) )
			{
				return false;
			}


			return $results;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

