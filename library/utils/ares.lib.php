<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-04-07 15:34
	* @file:		ARES library
	* @copyright:	(c) 2014 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ARES LIBRARY


	namespace WebCore\Utils;


	use \WebCore\IO;


	abstract class ARES
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Resource
		**/
		const SOURCE		= "http://wwwinfo.mfcr.cz/cgi-bin/ares/ares_es.cgi";
		const KEY			= "dtt:s";



#region Methods


		/**
		 *
		 * Get data using IC
		 *
		 * @param	int		$value	IC Value
		 * @param	int		$entry	Entry number
		 * @return	array
		 * @see		IC
		**/
		public static function IC ( $value, $entry = null )
		{
			// Get the values using ICO property
			$users = static::get( 'ico', $value );

			// If we did not get any data
			if ( empty( $users ) )
			{
				return false;
			}

			// If there is mentioned index in the list
			if ( array_key_exists( $entry, $users ) )
			{
				// Return just wanted entry
				return $users[ $entry ];
			}


			// List of entries
			return $users;
		}


		/**
		 *
		 * Get data using Name
		 *
		 * @param	string	$value	Company name
		 * @param	int		$entry	Entry number
		 * @return	array
		 * @see		Name
		**/
		public static function Name ( $value, $entry = null )
		{
			// Get the values using NAME property
			$users = static::get( 'obch_jm', $value );

			// If we did not get any data
			if ( empty( $users ) )
			{
				return false;
			}

			// If there is mentioned index in the list
			if ( array_key_exists( $entry, $users ) )
			{
				// Return just wanted entry
				return $users[ $entry ];
			}


			// List of entries
			return $users;
		}


#endregion

#region Helpers


		/**
		 *
		 * Load data from ARES database
		 *
		 * @param	string	$key		Search by
		 * @param	mixed	$value		Query
		 * @return	array
		 * @see		get
		**/
		protected static function get ( $key, $value )
		{
			// Try to read the XML file from the source URL address
			$xml = IO\XML::Read( static::SOURCE."?{$key}={$value}&filtr=1");

			// If we failed to get the XML
			if ( empty( $xml ) )
			{
				return false;
			}


			// Access list of nodes with the key
			$nodes = $xml->nodes( static::KEY );
			$items = array( );

			// Iterate through each one
			foreach ( $nodes as $node )
			{
				// Parse ndoe data into item holder
				$items[] = static::parse( $node );
			}


			return $items;
		}


		/**
		 *
		 * Handle data from ARES
		 *
		 * @param	array	$node	Data Node
		 * @return	stdClass
		 * @see		parse
		**/
		protected static function parse ( array $node )
		{
			// Build the item object from data node
			$item = new \stdClass( );
			$item->name = $node['dtt:ojm'];		// Company name
			$item->type = $node['dtt:pf'];		// Type of company
			$item->street = null;				// Address
			$item->city = null;					// Address
			$item->ic = $node['dtt:ico'];		// IC
			$item->dic = null;					// Holder for DIC

			// Make the address usable in our system
			$address = $node['dtt:jmn'];
			$address = preg_split( "/([-,]+)/", $address );
			$address = array_map( "trim", $address );

			// Switch the size
			switch ( true )
			{
				// If there are two parts of the town
				case sizeof( $address ) > 4:

					// Lets not support this state
					// because it is too problematic
					$address = array( );

				break;

				// If there are two parts of the town
				case sizeof( $address ) > 3:

					// Remove the part of the town
					unset( $address[1] );
					unset( $address[2] );

				break;

				// If there is part of the town
				case sizeof( $address ) > 2:

					// Remove the part of the town
					unset( $address[1] );

				break;
/*
				// If there is part of the town
				case sizeof( $address ) == 0:

					// Get the city name from the street, because it is the same
					//$address[ ] = substr( $address[0], 0, strrpos( $address[0], ' ' ) );

					var_dump( "!!!!!!!!!!!!!!!!", $address, substr( $address[0], 0, strrpos( $address[0], ' ' ) ) );

				break;
*/
			}


			// Reset the index numbers of the address
			$address = array_reverse( $address, false );

			// If there is DIC defined in the data node
			if ( array_key_exists( 'dtt:p_dph', $node ) )
			{
				// Get the DIC without the starting part
				$item->dic = $node['dtt:p_dph'];
				$item->dic = substr( $item->dic, 4 );
				$item->dic = "CZ{$item->dic}";
			}

			// If there is no address
			if ( empty( $address ) )
			{
				return $item;
			}


			// Fill in the address
			$item->street = $address[0];
			$item->city = $address[1];
			$item->zip = $address[2];

			return $item;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

