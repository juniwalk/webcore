<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-3 12:22
	* @file:		DSN library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DSN LIBRARY


	namespace WebCore\Data;


	class DSN implements \ArrayAccess, \IteratorAggregate, \Countable
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Wrapper scheme
		**/
		public $scheme		= null;


		/**
		 * Login credentials
		**/
		public $host		= null;
		public $port		= null;
		public $user		= null;
		public $pass		= null;
		public $path		= null;
		public $fragment	= null;


		/**
		 * Connection properties
		**/
		public $query		= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	string	$dsn	DSN string
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( $dsn = null )
		{
			// Parse DSN string
			$this->parse( $dsn );
		}


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			// Build DSN string and return it
			return (string) $this->build( );
		}


		/**
		 *
		 * <<magic>> Getter
		 *
		 * @param	string	$key	Name of the property
		 * @return	mixed
		 * @see		__get
		**/
		public function __get ( $key )
		{
			// If the property does not exists
			if ( !$this->offsetExists( $key ) )
			{
				return null;
			}


			return $this->offsetGet( $key );
		}


		/**
		 *
		 * <<magic>> Setter
		 *
		 * @param	string	$key	Name of the property
		 * @param	mixed	$value	New value
		 * @return	void
		 * @see		__set
		**/
		public function __set ( $key, $value )
		{
			$this->offsetSet( $key, $value );
		}


		/**
		 *
		 * Extend DSN of new values
		 *
		 * @param	array  $values  List of changes
		 * @return	void
		 * @see		extend
		**/
		public function extend ( array $values )
		{
			// If there are no values
			if (empty($values)) {
				return;
			}

			// Iterate over the list of values
			foreach ($values as $key => $value) {
				// Set new value to instance
				$this->$key = $value;
			}
		}



		/**
		 *
		 * Parse DSN string
		 *
		 * @param	string	$dsn	DSN string
		 * @return	bool
		 * @see		parse
		**/
		protected function parse ( $dsn )
		{
			// If DSN is not a string
			if ( !is_string( $dsn ) )
			{
				return false;
			}


			// Parse DSN value into array
			$url = parse_url( $dsn );

			// If there are no values
			if ( empty( $url ) )
			{
				return false;
			}

			// Iterate through each value in holder
			foreach ( $url as $key => $value )
			{
				// Assign value to the object
				$this->$key = urldecode( $value );
			}

			// If there is a port from Url
			if ( !empty( $this->port ) )
			{
				// Cast the port number to the integer
				$this->port = (int) $this->port;
			}

			// If there is no query string in holder
			if ( empty( $this->query ) OR !is_string( $this->query ) )
			{
				return true;
			}


			// Parse query string into parameters array
			parse_str( $this->query, $this->query );

			return true;
		}


		/**
		 *
		 * Build DSN string
		 *
		 * @param	void
		 * @return	string
		 * @see		build
		**/
		protected function build ( )
		{
			// If there is no scheme set
			if ( empty( $this->scheme ) )
			{
				return null;
			}


			// Start with DSN scheme name
			$dsn = "{$this->scheme}://";

			// If there are login credentials
			if ( is_string( $this->user ) )
			{
				// Append url encoded username to the DSN
				$dsn .= urlencode( $this->user );
			}

			// If there are login credentials
			if ( !empty( $this->pass ) )
			{
				// Append url encoded password to the DSN
				$dsn .= ':'.urlencode( $this->pass );
			}

			// If there username set
			if ( !empty( $this->user ) )
			{
				// Append [at] to the DSN
				$dsn .= '@';
			}

			// if there is no host set
			if ( empty( $this->host ) )
			{
				return null;
			}


			// Follow with the server host name
			$dsn .= urlencode( $this->host );

			// If there is specific port number
			if ( is_numeric( $this->port ) )
			{
				// Append port number to the server
				$dsn .= ":{$this->port}";
			}

			// If the path is not empty
			if ( is_string( $this->path ) )
			{
				// Encode the path using urlencode
				// and fix some basic characters
				$path = urlencode( $this->path );
				$path = str_replace( ['+','%2F'], ['%20','/'], $path );

				// Save the path into the DSN
				$dsn .= "/".trim( $path, '\/' );
			}

			// If there are query values
			if ( !empty( $this->query ) )
			{
				// Build the http query from gathered array holder
				$dsn .= "?".http_build_query( $this->query );
			}

			// If there is URL fragment
			if ( !empty( $this->fragment ) )
			{
				// Append the fragment into the DSN
				$dsn .= "#".urlencode( $this->fragment );
			}


			// DSN string
			return $dsn;
		}



		/**
		 *
		 * <<interface>> Get the count of the fields
		 *
		 * @param	void
		 * @return	int
		 * @see		count
		**/
		final public function count ( )
		{
			return sizeof( $this->query );
		}


		/**
		 *
		 * <<interface>> Access array iterator
		 *
		 * @param	void
		 * @return	ArrayIterator
		 * @see		getIterator
		**/
		final public function getIterator ( )
		{
			return new \ArrayIterator( $this->query );
		}


		/**
		 *
		 * <<interface>> Set property value
		 *
		 * @param	string	$key	Property name
		 * @param	mixed	$value	Property value
		 * @return	void
		 * @see		offsetSet
		**/
		final public function offsetSet ( $key, $value )
		{
			$this->query[ $key ] = $value;
		}


		/**
		 *
		 * <<interface>> Get property value
		 *
		 * @param	string	$key	Property name
		 * @return	mixed
		 * @see		offsetGet
		**/
		final public function offsetGet ( $key )
		{
			return $this->query[ $key ];
		}


		/**
		 *
		 * <<interface>> Does property exists?
		 *
		 * @param	string	$key	Property name
		 * @return	bool
		 * @see		offsetExists
		**/
		final public function offsetExists ( $key )
		{
			return array_key_exists( $key, $this->query );
		}


		/**
		 *
		 * <<interface>> Unset property
		 *
		 * @param	string	$key	Property name
		 * @return	void
		 * @see		offsetUnset
		**/
		final public function offsetUnset ( $key )
		{
			unset( $this->query[ $key ] );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

