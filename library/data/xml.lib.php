<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-11-13 12:56
	* @file:		XML library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	XML LIBRARY


	namespace WebCore\Data;


	// Class moved to \WebCore\IO\XML, preserved for compatibility
	final class XML extends \WebCore\IO\XML { }


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

