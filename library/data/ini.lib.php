<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-14 12:56
	* @file:		INI library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	INI LIBRARY


	namespace WebCore\Data;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB;


	final class INI extends \WebCore\IO\INI { }


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

