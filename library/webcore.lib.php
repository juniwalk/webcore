<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-25 20:40
	* @file:		WebCore library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	WEBCORE LIBRARY


	namespace WebCore;


	use \WebApp;
	use \WebCore\Core;
	use \WebCore\Core\Exceptions;
	use \WebCore\Debug;
	use \WebCore\User;
	use \WebCore\Security;
	use \WebCore\Web;
	use \WebCore\Web\Menu;


	final class WebCore
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Database
		**/
		private static $db		= "mysqli://dp_webcore:Gu1D.1415-C1FF@localhost/dp_webcore?persistent=1";


		/**
		 * WebCore NS Aliases
		**/
		private static $aliases	= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( ) { }



		/**
		 *
		 * Set Content-Type header
		 *
		 * @param	string	$charset	Charset
		 * @param	string	$ctype		Content type
		 * @return	void
		 * @throws	InvalidArgument
		 * @see		Charset
		**/
		public static function Charset ( $charset, $ctype = "text/html" )
		{
			// If the charset is not a string
			if ( !is_string( $charset ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $charset, 1 );
			}

			// If the C-Type is not a string
			if ( !is_string( $ctype ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $ctype, 2 );
			}


			// Assign charset to Smarty class
			\Smarty::$_CHARSET = $charset;

			// Set the Content type header using provided values
			Header("Content-Type: {$ctype};charset={$charset}");
		}


		/**
		 *
		 * Create path alias for the namespace
		 *
		 * @param	string	$alias	Namespace alias
		 * @param	string	$path	Path on hard drive
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		Alias
		**/
		public static function Alias ( $alias, $path )
		{
			// If the alias is not a string
			if ( !is_string( $alias ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $alias, 1 );
			}

			// If the path is not a string
			if ( !is_string( $path ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $path, 2 );
			}


			// Register path alias in WebCore loader
			self::$aliases[ $alias ] = rtrim( $path, '\/' );
		}


		/**
		 *
		 * Alias of WebCore->load( )
		 *
		 * @param	string	$path		Path to the file (namespace)
		 * @param	float	$version	File version
		 * @return	bool
		 * @see		Using
		**/
		public static function Using ( $path, $version = 0 )
		{
			// Access list of the aliases
			$aliases = &self::$aliases;

			// If the alias is not a string
			if ( !is_string( $path ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $path, 1 );
			}

			// If the version is not a float
			if ( !is_numeric( $version ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'float', $version, 2 );
			}


			// Replace \ with / and lowercase all letters
			$path = strToLower("{$path}.lib.php");
			$path = str_replace( '\\', '/', $path );

			// Get the alias from the path to translate
			$alias = substr( $path, 0, strpos( $path, '/' ) );

			// If the namespace has entry in the aliases
			if ( array_key_exists( $alias, $aliases ) )
			{
				// Replace the alias with the assigned path to it
				$path = str_replace( $alias, $aliases[ $alias ], $path );
			}

			// If we are working with Assetic
			if ( in_array( $alias, [ 'assetic', 'fxmlrpc', 'tracy' ] ) )
			{
				// Modify the extension for standard php files
				$path = str_replace( ".lib.php", ".php", $path );
			}

			// If there is no such file
			if ( !file_exists( $path ) )
			{
				return false;
			}


			// Include library file
			include $path;

			return true;
		}


		/**
		 *
		 * Register WebCore services
		 *
		 * @param	void
		 * @return	bool
		 * @see		Register
		**/
		public static function Register ( )
		{
			// WebCore state holder
			static $__state = false;

			// If the WebCore has been registered
			if ( $__state === true )
			{
				return true;
			}

			// Try to register WebCore as default autoloading service
			if ( !spl_autoload_register( __CLASS__."::Using" ) )
			{
				return false;
			}


			// Access the WebCore Database authentication
			$auth = new Security\Auth( self::$db );

			// If the login to DB is invalid
			if ( !$auth->validate( ) )
			{
				return false;
			}


			// Register WebCore database link
			$db = DB\DB::Factory( $auth, 'webcore' );

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Access instance of the Site
			$site = Web\Site::Factory( );

			// Prepare configuration data
			if ( !$site->configurate( ) )
			{
				return false;
			}


			// Access instance of the Site's config
			$config = &WebApp\Config::Factory( );

			// Start output buffering
			ob_start( );
			ob_implicit_flush( false );

			// If compression is enabled
			if ( $config->PAGE_COMPRESS )
			{
				// Restart the output buffer with
				// code output compression enabled
				ob_end_clean( );
				ob_start("ob_gzhandler");
			}

			// If there is no session opened
			if ( session_id( ) === '' )
			{
				// Open up the session
				session_start( );
			}

			// If the debugging is enabled
			if ( $site->debug )
			{
				// Report all errors
				error_reporting(-1);
			}

			// if there is Smarty class loaded
			if ( !class_exists( "\Smarty", false ) )
			{
				return false;
			}



			// Get the instance of Smarty and
			// assign paths to the template views
			$smarty = &\Smarty::Factory( );
			$smarty->addPluginsDir( WEBCORE."/library/assetic/extension/smarty" );
			$smarty->addTemplateDir( WEBCORE."/view", 'webcore' );
			$smarty->force_compile = true;	// TODO: temporary

			// Assign paths for compiles and cached files
			$smarty->setCompileDir( WEBCORE."/compile" );
			$smarty->setCacheDir( WEBCORE."/cache" );

			// If debugging is enabled
			if ( Debug\Debug::Enabled( ) )
			{
				// Enable template debugging
				$smarty->debugging = true;
				$smarty->force_compile = true;
			}

			// If the site should be cached and debugging is disabled
			if ( $config->PAGE_CACHE AND !Debug\Debug::Enabled( ) )
			{
				// Enable template caching
				$smarty->caching = true;
			}


			// Access instance of the Language
			$lang = Web\Lang::Factory( );

			// if we could not detect locale
			if ( !$lang->detect( ) )
			{
				return false;
			}


			// Assign internal instances to the template
			$smarty->assign( 'config', $config );
			$smarty->assign( 'site', $site );
			$smarty->assign( 'lang', $lang );


			// Set default content charset from language
			self::Charset( $lang->X('LANG_CHARSET') );

			// Now Registered
			$__state = true;

			return true;
		}


		/**
		 *
		 * Initialize WebApp services
		 *
		 * @param	void
		 * @return	bool
		 * @see		Initialize
		**/
		public static function Initialize ( )
		{
			// WebCore state holder
			static $__state = false;

			// Create instance of the WebApp config
			$config = WebApp\Config::Factory( );

			// If the WebCore has been initialized
			if ( $__state === true )
			{
				return true;
			}


			// Get the instance of Smarty and
			// assign paths to the template views
			$smarty = &\Smarty::Factory( );
			$smarty->addTemplateDir( WEBAPP."/view", 'webapp' );

			// Assign paths for compileds and cached files
			$smarty->setCompileDir( WEBAPP."/compile" );
			$smarty->setCacheDir( WEBAPP."/cache" );


			// Access WebApp database login
			$login = $config->login( );

			// If the MySQL is not used
			if ( is_null( $login ) )
			{
				return true;
			}

			// If the DSN was not valid
			if ( !$login->validate( ) )
			{
				return false;
			}


			// Register WebApp database link
			$db = DB\DB::Factory( $login, 'webapp' );

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}

			// If we were unable to kill expired sessions
			if ( !User\Session::KillExpired( ) )
			{
				return false;
			}


			// Access Session instance
			$session = User\Session::Factory( );

			// If we could not touch the session
			if ( !$session->touch( ) )
			{
				return false;
			}


			// Create instance of the User
			$user = &User\User::Factory( );

			// Access Menu instance
			$menu = &Menu\Menu::Factory( );
			$menu->source('webapp');

			// If we could not register menu
			if ( !$menu->register( ) )
			{
				return false;
			}


			// Create error pages using private keys
			Menu\Item::Create( '403', "MENU_HTTP403", User\UAC::GUEST, null, array( 'cache' => 1, 'hidden' => 1 ) );
			Menu\Item::Create( '404', "MENU_HTTP404", User\UAC::GUEST, null, array( 'cache' => 1, 'hidden' => 1 ) );
			Menu\Item::Create( 'signin', "MENU_SIGNIN", User\UAC::GUEST, null, array( 'cache' => 1, 'hidden' => 1 ) );
			Menu\Item::Create( 'signup', "MENU_SIGNUP", User\UAC::GUEST, null, array( 'cache' => 1, 'hidden' => 1 ) );

			// Assign basic instances to template
			$smarty->assign( 'menu', $menu );
			$smarty->assign( 'user', $user );
			$smarty->assign( 'session', $session );


			// Now Initialized
			$__state = true;

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

