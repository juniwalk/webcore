<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-12-12 9:40
	* @file:		Zip library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ZIP LIBRARY


	namespace WebCore\IO;


	class Zip extends \ZipArchive
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( $file = null, $options = null )
		{
			// If the file does not exist
			if ( !is_file( $file ) )
			{
				return null;
			}


			// try to open the Zip Archive using given options
			return parent::open( $file, $options );
		}


		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	bool
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			// If there is no filename
			if ( empty( $this->filename ) )
			{
				return null;
			}


			// Call the closing method
			return parent::close( );
		}


#region Read & Write


		/**
		 *
		 * Add new file
		 *
		 * @param	File	$file		File handler
		 * @param	string	$filename	File name
		 * @return	bool
		 * @see		file
		**/
		public function file ( File $file, $filename = null )
		{
			// If the filename is not entered
			if ( is_null( $filename ) )
			{
				// Get the name of the file from the object
				$filename = basename( $file->path );
			}


			// Insert the file into the Zip archive instance
			return parent::addFromString( $filename, $file->contents( ) );
		}


		/**
		 *
		 * Create new dir
		 *
		 * @param	string	$dirname	Dir name
		 * @return	bool
		 * @see		dir
		**/
		public function dir ( $dirname )
		{
			return parent::addEmptyDir( $dirname );
		}


		/**
		 *
		 * Rename entry
		 *
		 * @param	mixed	$entry		Entry name or index
		 * @param	string	$filename	File name
		 * @return	bool
		 * @see		rename
		**/
		public function rename ( $entry, $filename )
		{
			// If the file name is not a string
			if ( !is_string( $filename ) )
			{
				return false;
			}

			// If the entry is an integer
			if ( is_integer( $entry ) )
			{
				// Revert changes by the entry index
				return parent::renameIndex( $entry, $filename );
			}

			// If the entry is a string
			if ( is_string( $entry ) )
			{
				// Revert changes by the entry name
				return parent::renameName( $entry, $filename );
			}


			return false;
		}


		/**
		 *
		 * Delete entry
		 *
		 * @param	mixed	$entry	Entry name or index
		 * @return	bool
		 * @see		delete
		**/
		public function delete ( $entry )
		{
			// If the entry is an integer
			if ( is_integer( $entry ) )
			{
				// Revert changes by the entry index
				return parent::deleteIndex( $entry );
			}

			// If the entry is a string
			if ( is_string( $entry ) )
			{
				// Revert changes by the entry name
				return parent::deleteName( $entry );
			}


			return false;
		}


		/**
		 *
		 * Extract Zip files
		 *
		 * @param	string	$path	Path to extract files to
		 * @param	array	$files	List of files to extract
		 * @return	bool
		 * @see		extract
		**/
		public function extract ( $path, array $files = null )
		{
			return parent::extractTo( $path, $files );
		}


#endregion

#region Operations


		/**
		 *
		 * Restore Zip to its initial state
		 *
		 * @param	void
		 * @return	bool
		 * @see		restore
		**/
		public function restore ( )
		{
			return parent::unchangeAll( );
		}


		/**
		 *
		 * Revert changes to a entry
		 *
		 * @param	mixed	$entry	Entry name or index
		 * @return	bool
		 * @see		revert
		**/
		public function revert ( $entry )
		{
			// If the entry is an integer
			if ( is_integer( $entry ) )
			{
				// Revert changes by the entry index
				return parent::unchangeIndex( $entry );
			}

			// If the entry is a string
			if ( is_string( $entry ) )
			{
				// Revert changes by the entry name
				return parent::unchangeName( $entry );
			}


			return false;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

