<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-14 12:56
	* @file:		INI library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	INI LIBRARY


	namespace WebCore\IO;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB;


	class INI
	{
		/**
		 * Version holder
		**/
		const VERSION	= 2.2;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		protected function __construct ( ) { }



		/**
		 *
		 * Parse INI file or string into array
		 *
		 * @param	string	$source		Path to the ini file or ini string
		 * @param	bool	$sections	Does the source include sections?
		 * @return	array
		 * @throws	InvalidArgument
		 * @see		Read
		**/
		public static function Read ( $source, $sections = true )
		{
			// If the source is not a string
			if ( !is_string( $source ) AND !is_null( $source ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $source, 1 );
			}

			// If the source is a file
			if ( is_file( $source ) )
			{
				return parse_ini_file( $source, $sections );
			}

			// If parsing file has failed and the
			// source is not empty string
			if ( !empty( $source ) )
			{
				return parse_ini_string( $source, $sections );
			}


			return array( );
		}


		/**
		 *
		 * Parse INI file or string into array
		 *
		 * @param	array	$values		Array to be parsed to ini
		 * @param	bool	$sections	Should we write sections?
		 * @param	string	$path		Should the data be written to file?
		 * @return	mixed
		 * @see		Write
		**/
		public static function Write ( array $values, $sections = true, $path = null )
		{
			// If there are no values
			if ( empty( $values ) )
			{
				return false;
			}

			// Make sure that array are at the bottom
			if ( !asort( $values, SORT_REGULAR ) )
			{
				return false;
			}


			// Parse values into the INI string
			$ini = static::Parse( $values, array( ), $sections );
			$ini = trim( $ini, PHP_EOL );

			// If there is no path to file
			if ( is_null( $path ) )
			{
				return $ini;
			}

			// If the path is not a string
			if ( !is_string( $path ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $path, 3 );
			}

			// If we ware unable to write data into file
			if ( !file_put_contents( $path, $ini, LOCK_EX ) )
			{
				throw new \Exception("Failed to write INI data into file");
			}


			// Return values
			return $ini;
		}


		/**
		 *
		 * Store key/value in the holder
		 *
		 * @param	array	$values		List of values to parse
		 * @param	array	$parent		Temporary section holder
		 * @param	bool	$section	Use sections?
		 * @return	void
		 * @see		Store
		**/
		protected static function Parse ( array $values, array $parent = array( ), $sections = true )
		{
			// Prepare holder
			$ini = null;

			// Iterate through each value in holder
			foreach ( $values as $key => $value )
			{
				// If the value is an array
				if ( !is_array( $value ) )
				{
					// Store value into the standard key/value pair and go to next
					$ini .= static::Store( $key, $value, false ); continue;
				}


				// Get the name of the section for this array tree
				$key = array_merge( (array) $parent, (array) $key );

				// if the sections are enabled
				if ( $sections == true )
				{
					// Add section to the output INI string
					$ini .= static::Store( $key, null, true );
				}


				// Go to the deeper level to parse the array
				$ini .= static::Parse( $value, $key, $sections );
			}


			return $ini;
		}


		/**
		 *
		 * Store key/value in the holder
		 *
		 * @param	string	$key		Key of the entry
		 * @param	string	$value		Value of the entry
		 * @param	bool	$section	Is this a section?
		 * @return	void
		 * @see		Store
		**/
		protected static function Store ( $key, $value, $setion = false )
		{
			// If the key is section
			if ( $setion == true )
			{
				$key = implode( ".", $key );
				$key = PHP_EOL."[{$key}]";
			}


			// Get the type of the value
			$type = gettype( $value );
			$type = strToLower( $type );

			// Switch the type
			switch ( $type )
			{
				// Standard string
				case 'string':

					// Add quotes to the value
					$value = addslashes( $value );
					$value = "\"{$value}\"";

				break;

				// Empty & Bool values
				case 'boolean':
				case 'null':

					// if the value is true
					if ( $value == true )
					{
						$value = 'On';
					}

					// If the value is false
					if ( $value == false )
					{
						$value = 'Off';
					}

				break;

				// Any other value
				default: break;
			}

			// if this is not a section
			if ( $setion == false )
			{
				// Add value to the key
				$key = "{$key} = {$value}";
			}


			// Return the key
			return $key.PHP_EOL;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

