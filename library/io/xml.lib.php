<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-11-13 12:56
	* @file:		XML library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	XML LIBRARY


	namespace WebCore\IO;


	class XML
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Resource
		**/
		protected $link			= null;
		protected $cdata		= true;
		protected $parsed		= false;
        protected $upper        = false;


		/**
		 * Holder
		**/
		protected $holder		= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		protected function __construct ( ) { }


		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	bool
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			// If we were writing an XML file
			if ( is_a( $this->link, "XMLWriter" ) )
			{
				return true;
			}


			// Close link to XML Reader
			return $this->link->close( );
		}


#region Instance


		/**
		 *
		 * Write XML data
		 *
		 * @param	array	$value		Value to be written
		 * @param	string	$indent		Indent character
		 * @param	bool	$cdata		Enable CDATA encapsulation?
		 * @param	bool	$upper		Use upercassed tag names?
		 * @return	self
		 * @see		Write
		**/
		public static function Write ( array $value, $indent = null, $cdata = true, $upper = false )
		{
			// Create XML Instance
			$xml = new self;
            $xml->upper = (bool) $upper;
			$xml->cdata = (bool) $cdata;
			$xml->link = new \XMLWriter( );
			$xml->link->openMemory( );
			$xml->link->setIndent( false );

			// If there are no values
			if ( empty( $value ) )
			{
				return false;
			}


			// Assign values to the holder
			$xml->holder = $value;

			// If there is node indenting
			if ( is_string( $indent ) )
			{
				// Set indent string and enable indenting
				$xml->link->setIndentString( $indent );
				$xml->link->setIndent( true );
			}


			// Read the XML into ARRAY using created XML Parser
			$xml->link->startDocument( '1.0', "UTF-8" );

			return $xml;
		}


		/**
		 *
		 * Read XML data
		 *
		 * @param	string	$value		Value to be read
		 * @param	int		$options	Options used when opening data
		 * @param	string	$encoding	Document encoding
		 * @return	self
		 * @see		Read
		**/
		public static function Read ( $value, $options = null, $encoding = null )
		{
			// Create XML Instance
			$xml = new self( );
			$xml->link = new \XMLReader( );

			// If the value is instance of File class
			if ( is_a( $value, '\WebCore\IO\File' ) )
			{
				// Get the path to the file
				$value = $value->path;
			}

			// If the value is not a string
			if ( !is_string( $value ) )
			{
				return false;
			}

			// Determine method
			switch ( true )
			{
				// Opening a fyzical file from the drive or server
				case preg_match( "#^(https?|ftp)://#i", $value ):
				case preg_match( "#^(zip)://#i", $value ):
				case is_file( $value ):

					// Use the OPEN method
					$method = "open";

				break;

				// Reading an XML string provided by the user
				case preg_match( "#(<[^>]+)([^>]*)#iU", $value ):

					// Use the XML method
					$method = "xml";

				break;

				// Default action is to fail
				default: return false;
			}

			// If there are no options available
			if ( !is_integer( $options ) )
			{
				$options = LIBXML_PARSEHUGE | LIBXML_COMPACT | LIBXML_NOBLANKS;
			}

			// If we have failed to open the XML value
			if ( !$xml->link->$method( $value, $encoding, $options ) )
			{
				return false;
			}


			// Return XML
			return $xml;
		}


		/**
		 *
		 * Write XML data in one line
		 *
		 * @param	array	$value		Value to be written
		 * @param	string	$path		Path to the file
		 * @param	string	$indent		Indent character
		 * @param	bool	$cdata		Enable CDATA encapsulation?
		 * @return	string
		 * @see		WriteQuick
		**/
		public static function WriteQuick ( array $value, $path = null, $indent = "\t", $cdata = true )
		{
			// Write the array values into the XML file / string
			$xml = static::Write( $value, $indent, $cdata );

			// If we could not read XML
			if ( $xml === false )
			{
				return false;
			}

			// If the parsing has failed
			if ( !$xml->parse( ) )
			{
				return false;
			}


			// Return results data
			return $xml->results( $path );
		}


		/**
		 *
		 * Read XML data in one line
		 *
		 * @param	string	$value		Value to be read
		 * @param	int		$options	Options used when opening data
		 * @param	string	$encoding	Document encoding
		 * @return	array
		 * @see		ReadQuick
		**/
		public static function ReadQuick ( $value, $options = null, $encoding = null )
		{
			// Read the XML file / string into the XML Reader
			$xml = static::Read( $value, $options, $encoding );

			// If we could not read XML
			if ( $xml === false )
			{
				return false;
			}

			// If the parsing has failed
			if ( !$xml->parse( ) )
			{
				return false;
			}


			// Return results data
			return $xml->results( );
		}


#endregion

#region Read & Write


		/**
		 *
		 * Read XML data
		 *
		 * @param	XMLReader	$xml	XML Reader instance
		 * @return	bool
		 * @see		__read
		**/
		protected function __read ( \XMLReader $xml )
		{
			// Prepare data holder
			$holder = null;

			// Iterate through each node
			while ( $xml->read( ) )
			switch ( $xml->nodeType )
			{
				// Node element
				case $xml::ELEMENT:

					// Get the name of the element
					$name = strToLower( $xml->name );

					// Store value in an array using node name and contents
					$holder[ $name ][ ] = $this->__read( $xml );

	            break;

				// Inner contents
				case $xml::TEXT:
				case $xml::CDATA:
				case $xml::WHITESPACE:
				case $xml::SIGNIFICANT_WHITESPACE:

					// Add value to the holder
					$holder .= $xml->value;

				break;

				// Break the While loop in the end
				case $xml::END_ELEMENT: break 2;
	        }


			// Check the values in Holder
			$this->check( $holder );

			return $holder;
		}


		/**
		 *
		 * Write XML data
		 *
		 * @param	XMLWriter	$xml		XML Writer instance
		 * @param	mixed		$holder		Data to be written
		 * @return	bool
		 * @see		__read
		**/
		protected function __write ( \XMLWriter $xml, $holder, $parent = null )
		{
			// If the holder is empty
			if ( empty( $holder ) )
			{
				return false;
			}

			// Iterate through each element in holder
			foreach ( $holder as $key => $value )
			{
				// If the index is not string
				if ( !is_string( $key ) )
				{
					// Use parent index
					$key = $parent;
				}


				// Wirte element into the XML Writer
				$this->node( $xml, $key, $value );
			}


			return true;
		}


		/**
		 *
		 * Get the nodes
		 *
		 * @param	string	$node	Name of the node
		 * @return	Generator
		 * @see		nodes
		**/
		public function &nodes ( $node )
		{
			// Access the XML link
			$xml = &$this->link;

			// If the XML was parsed
			if ( $this->parsed )
			{
				return;
			}

			// Iterate through each node
			while ( $xml->read( ) )
			{
				// If this is not a node we want
				if ( strToLower( $xml->name ) != $node )
				{
					continue;
				}


				// Access the value from the XML
				$value = $this->__read( $xml );

				// if there is no value
				if ( empty( $value ) )
				{
					continue;
				}


				// Yield the value
				yield $value;
			}
		}


#endregion

#region Helpers


		/**
		 *
		 * Parse loaded data
		 *
		 * @param	void
		 * @return	bool
		 * @see		parse
		**/
		public function parse ( )
		{
			// Access XML Reader/Writer
			$xml = &$this->link;

			// If the XML was parsed
			if ( $this->parsed )
			{
				return false;
			}


			// The XML is now declared as parsed
			$this->parsed = true;

			// Switch the type of operation
			switch ( true )
			{
				// We are reading the XML document
				case is_null( $this->holder ):

					// Read the XML into ARRAY using created XML Reader
					$this->holder = $this->__read( $xml, null );

					// Close the parser
					if ( !$xml->close( ) )
					{
						return false;
					}

				break;

				// We are writing the XML document
				case !empty( $this->holder ):

					// If we were unable to write data into XML Writer
					if ( !$this->__write( $xml, $this->holder ) )
					{
						return false;
					}


					// Close the document
					$xml->endDocument( );

					// Save data into the string and trim the end
					$this->holder = $xml->outputMemory( true );
					$this->holder = trim( $this->holder );

				break;

				// Default action is to fail
				default: return false;
			}


			// Clear the parser
			unset( $xml );

			// If there is no holder data
			if ( empty( $this->holder ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Get and/or Save XML data
		 *
		 * @param	string	$path	Path to the file
		 * @return	mixed
		 * @see		results
		**/
		public function results ( $path = null )
		{
			// Access the holder of data
			$holder = &$this->holder;

			// If there is no data
			if ( empty( $holder ) )
			{
				return false;
			}

			// If there is no file
			if ( is_null( $path ) )
			{
				return $holder;
			}

			// If the path is not a string
			if ( !is_string( $path ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $path, 1 );
			}

			// If the XML holder is not parsed
			if ( !is_string( $holder ) )
			{
				throw new \Exception("The XML data are not parsed into string");
			}

			// If we ware unable to write data into file
			if ( !file_put_contents( $path, $holder, LOCK_EX ) )
			{
				throw new \Exception("Failed to write XML data into file");
			}


			// Return values
			return $holder;
		}


		/**
		 *
		 * Output parsed data
		 *
		 * @param	void
		 * @return	bool
		 * @see		output
		**/
		public function output ( )
		{
			// Access results of the XML
			$holder = $this->results( null );

			// If the XML holder is not parsed
			if ( !is_string( $holder ) )
			{
				return false;
			}


			// Close the output buffer
			ob_end_clean( );

			// If there are no headers
			if ( !headers_sent( ) )
			{
				Header("Content-Type: application/xml;charset=UTF-8");
			}


			// Output the XML to the browser
			return file_put_contents( "php://output", $holder );
		}


		/**
		 *
		 * Perform a data check
		 *
		 * @param	mixed	$value	Data to check
		 * @return	bool
		 * @see		check
		**/
		protected function check ( &$value )
		{
			// If the value is not array
			if ( !is_array( $value ) )
			{
				// Trim the string value and break
				$value = trim( $value ); return;
			}

			// Iterate through each entry
			foreach ( $value as $key => &$entry )
			{
				// Access holder using current key
				$holder = &$value[ $key ];

				// If the holder is not an array
				if ( !is_array( $holder ) )
				{
					// Perform check on the entry
					$this->check( $entry ); continue;
				}

				// If there is no Zero-Index bug
				if ( !array_key_exists( 0, $holder ) )
				{
					// Perform check on the entry
					$this->check( $entry ); continue;
				}

				// If there is more than one element
				if ( sizeof( $holder ) > 1 )
				{
					// Perform check on the entry
					$this->check( $entry ); continue;
				}


				// Remove Zero-Index bug
				$holder = $holder[0];
			}


			return null;
		}


		/**
		 *
		 * Write single node
		 *
		 * @param	mixed	$value	Data to check
		 * @return	void
		 * @see		node
		**/
		protected function node ( \XMLWriter $xml, $key, $value )
		{
		    if ($this->upper == true) {
		        $key = strtoupper($key);
		    }

			// Should we write tags?
			$writetag = true;

			// If the value is array and there are indexed items in there
			if ( is_array( $value ) AND array_key_exists( 0, $value ) )
			{
				// Do not write tag
				$writetag = false;
			}

			// Should we write tags?
			if ( $writetag == true )
			{
				// Start new element using Node name
				$xml->startElement( $key );
			}

			// Determine value type
			switch ( true )
			{
				// If the value is numeric
				case is_numeric( $value ):

					// Save it just as text
					$xml->text( $value );

				break;

				// If the value is boolean
				case is_bool( $value ):

					// Save it just as text
					$xml->text( $value ? 'TRUE' : 'FALSE' );

				break;

				// If the value is string
				case is_string( $value ) AND $this->cdata == true:

					// Store it as CData text
					$xml->writeCData( $value );

				break;

				// If the value is string
				case is_string( $value ) AND $this->cdata == false:

					// Store it as text
					$xml->text( $value );

				break;

				// If the value is array or object
				case is_array( $value ):
				case is_object( $value ):

					// Write the nodes into the XML
					$this->__write( $xml, $value, $key );

				break;
			}

			// Should we write tags?
			if ( $writetag == true )
			{
				// Close the element
				$xml->endElement( );
			}
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

