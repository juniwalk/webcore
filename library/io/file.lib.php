<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-12-5 15:23
	* @file:		File library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	FILE LIBRARY


	namespace WebCore\IO;


	use \WebCore\Core\Exceptions;


	class File
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Holder
		**/
		protected $link			= null;
		protected $path			= null;
		protected $stream		= null;
		protected $wrapper		= null;


		/**
		 * Properties
		**/
		protected $name			= null;
		protected $type			= null;
		protected $mode			= null;
		protected $size			= null;
		protected $mimetype		= null;
		protected $blocked		= null;
		protected $seekable		= null;
		protected $created		= null;
		protected $modified		= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		protected function __construct ( ) { }


		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	bool
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			// Close the file
			return $this->close( );
		}


		/**
		 *
		 * <<magic>> Getter
		 *
		 * @param	string	$key	Property name
		 * @return	string
		 * @throws	RuntimeException
		 * @see		__get
		**/
		public function __get ( $key )
		{
			// If there is no such property
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}


			// Return the value
			return $this->$key;
		}


#region Instance


		/**
		 *
		 * Create new file
		 *
		 * @param	string	$path		Path to file
		 * @param	bool	$overwrite	Overwrite existing files?
		 * @return	WebCore\IO\File
		 * @see		Create
		**/
		public static function Create ( $path, $overwrite = false )
		{
			// Create File Instance
			$file = new static( );

			// If the path is empty
			if ( empty( $path ) )
			{
				return false;
			}

			// If overwrite is disabled and file exists
			if ( !$overwrite AND is_file( $path ) )
			{
				// Open the file handler
				return static::Open( $path );
			}


			// Open the source file for reading and writing
			$file->link = @fopen( $path, "w+b", false );

			// We could not open file for reading
			if ( $file->link === false )
			{
				return false;
			}
/*
			// Truncate the file to zero
			if ( !$file->truncate( ) )
			{
				return false;
			}
*/
			// Gather file information
			if ( !$file->stats( ) )
			{
				return false;
			}


			// Use UTF-8 encoding by default
			$file->encoding("UTF-8");

			return $file;
		}


		/**
		 *
		 * Open the file
		 *
		 * @param	string	$path	Path to file
		 * @param	string	$mode	Mode for file
		 * @return	WebCore\IO\File
		 * @see		Open
		**/
		public static function Open ( $path, $mode = "a+b" )
		{
			// if the path is instance of the URL class
			if ( is_a( $path, '\WebCore\Net\URL' ) )
			{
				// Convert URL into string
				$path = (string) $path;
			}

			// If the path is not string
			if ( !is_string( $path ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $path, 1 );
			}

			// If the path is empty
			if ( empty( $path ) )
			{
				return false;
			}

			// If the file is not writable
			if ( !is_writable( $path ) )
			{
				$mode = "rb";
			}


			// Create File Instance and open the file
			$file = new static( );
			$file->link = fopen( $path, $mode, false );

			// We could not open file pointer
			if ( $file->link === false )
			{
				return false;
			}

			// Gather file information
			if ( !$file->stats( ) )
			{
				return false;
			}


			// Use UTF-8 encoding by default
			$file->encoding("UTF-8");

			return $file;
		}


#endregion

#region Read & Write


		/**
		 *
		 * Read file contents
		 *
		 * @param	int		$length		Number of bytes to read
		 * @return	string
		 * @see		read
		**/
		public function read ( $length )
		{
			// if the length is empty
			if ( is_null( $length ) )
			{
				// Return whole file
				return $this->contents( );
			}


			// Try to read the content from the file stream
			return fread( $this->link, $length );
		}


		/**
		 *
		 * Write to the file
		 *
		 * @param	string	$content	Contents to be written
		 * @param	int		$length		Number of bytes to write
		 * @return	int
		 * @throws	RuntimeException
		 * @see		write
		**/
		public function write ( $content, $length = null )
		{
			// If the file is not local
			if ( !$this->is_local( ) )
			{
				throw new \RuntimeException("Unable to write into remote files");
			}

			// If the value is not scalar and it is not empty
			if ( !is_scalar( $content ) AND !empty( $content ) )
			{
				// Encode the content into JSON string
				$content = json_encode( $content );
			}

			// if the length is empty
			if ( is_null( $length ) )
			{
				// Get the length of the content
				$length = strlen( $content );
			}


			// Try to write the content to the file stream
			$response = fwrite( $this->link, $content, $length );

			// If we have failed to write
			if ( $response === false )
			{
				return false;
			}

			// Gather file information
			if ( !$this->stats( ) )
			{
				return false;
			}


			return $response;
		}


		/**
		 *
		 * Read file contents as
		 *
		 * @param	string	$type	Output type
		 * @return	mixed
		 * @see		readAs
		**/
		public function readAs ( $type )
		{
			// Get the content of the file
			$content = $this->contents( );

			// If there is no content
			if ( empty( $content ) )
			{
				//return null;
			}

			// Switch the output type
			switch ( strToLower( $type ) )
			{
				// Read JSON contents
				case 'json':

					// Decode content as a JSON string
					return json_decode( $content, true );

				break;

				// Read INI contents
				case 'ini':

					// Parse content as a INI string
					return INI::Read( $content, true );

				break;

				// Read XML contents
				case 'xml':

					// Parse contents as a XML string
					return XML::Read( $this );

				break;

				// Read XML contents
				case 'xml-quick':

					// Parse contents as a XML string in
					// a little faster way than standard
					return XML::ReadQuick( $this );

				break;

				// Read CSV contents
				case 'csv':

					// Try to read the CSV fields
					return $this->fields( );

				break;

				// Read plain text
				case 'plain':

					return $content;

				break;
			}


			// If the output type is unknown, thow runtime exception
			throw new \RuntimeException("{$this}: Unknown output type '{$type}'");
		}


		/**
		 *
		 * Write to the file as
		 *
		 * @param	string	$type		Input type
		 * @param	mixed	$content	Contents to be written
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		writeAs
		**/
		public function writeAs ( $type, $content )
		{
			// If the file is not local
			if ( !$this->is_local( ) )
			{
				return false;
			}

			// Switch the output type
			switch ( strToLower( $type ) )
			{
				// Read JSON contents
				case 'json':

					// Decode contents as a JSON string
					$content = json_encode( $content );

				break;

				// Read INI contents
				case 'ini':

					// If the content is no array
					if ( !is_array( $content ) )
					{
						return false;
					}


					// Write the list into the INI string
					$content = INI::Write( $content, true );

				break;

				// Read XML contents
				case 'xml':

					// If the content is no array
					if ( !is_array( $content ) )
					{
						return false;
					}


					// Write the list into the XML string
					$content = XML::WriteQuick( $content );

				break;

				// Read CSV contents
				case 'csv':

					// If the content is no array
					if ( !is_array( $content ) )
					{
						return false;
					}

					// Walk through each fieldset in list
					foreach ( $content as $fields )
					{
						// If the set is not a list
						if ( !is_array( $fields ) )
						{
							return false;
						}


						// Write the set into the file pointer
						$status = fputcsv( $this->link, $fields, ',', '"' );

						// If the writing has failed
						if ( $status === false )
						{
							return false;
						}
					}


					return true;

				break;

				// Write plain text
				case 'plain': break;

				// Unknown type
				default:

					// If the output type is unknown, thow runtime exception
					throw new \RuntimeException("{$this}: Unknown output type '{$type}'");

				break;
			}

			// We were unable to write content
			if ( $this->write( $content ) === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Truncate file to specified size
		 *
		 * @param	int		$size	Size of the file
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		truncate
		**/
		public function truncate ( $size = 0 )
		{
			// If the file is not local
			if ( !$this->is_local( ) )
			{
				throw new \RuntimeException("Unable to truncate remote files");
			}


			// truncate the file to desired size
			return ftruncate( $this->link, $size );
		}


		/**
		 *
		 * Lock file
		 *
		 * @param	void
		 * @return	bool
		 * @see		lock
		**/
		public function flush ( )
		{
			return fflush( $this->link );
		}


		/**
		 *
		 * End of file?
		 *
		 * @param	void
		 * @return	bool
		 * @see		eof
		**/
		public function eof ( )
		{
			return feof( $this->link );
		}


		/**
		 *
		 * Get the pointer position
		 *
		 * @param	void
		 * @return	int
		 * @see		pointer
		**/
		public function pointer ( )
		{
			return ftell( $this->link );
		}


		/**
		 *
		 * Move pointer to position
		 *
		 * @param	int		$offset		Offset for the seek
		 * @param	int		$mode		Seek mode
		 * @return	bool
		 * @see		seek
		**/
		public function seek ( $offset, $mode = SEEK_SET )
		{
			// If this stream is unable to seek
			if ( $this->seekable == false )
			{
				return false;
			}


			// Try to seek to the desired offset in the stream
			$response = fseek( $this->link, $offset, $mode );

			// If something has failed
			if ( $response === -1 )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Move pointer to the begining
		 *
		 * @param	void
		 * @return	bool
		 * @see		rewind
		**/
		public function rewind ( )
		{
			// If this stream is unable to seek
			if ( $this->seekable == false )
			{
				return false;
			}


			// Rewind the pointer to the begining
			return rewind( $this->link );
		}


		/**
		 *
		 * Get lines from the file
		 *
		 * @param	void
		 * @return	string
		 * @see		lines
		**/
		public function lines ( )
		{
			// While there are any lines to read
			while ( $line = fgets( $this->link ) )
			{
				// Yield trimmed line
				yield rtrim( $line );
			}
		}


		/**
		 *
		 * Get CSV fields from the file
		 *
		 * @param	string	$delimeter	Fields delimeter
		 * @param	string	$enclosure	Data enclosure
		 * @param	string	$escape		Escaping character
		 * @return	array
		 * @see		fields
		**/
		public function fields ( $delimeter = ';', $enclosure = '"', $escape = '\\' )
		{
			// While there are any lines with fields to read
			while ( $field = fgetcsv( $this->link, 0, $delimeter, $enclosure, $escape ) )
			{
				// Yield trimmed fields in the holder
				yield array_map( "trim", $field );
			}
		}


		/**
		 *
		 * Get raw file contents
		 *
		 * @param	void
		 * @return	string
		 * @see		contents
		**/
		public function contents ( )
		{
			// Rewind to the begining
			if ( !$this->rewind( ) )
			{
				//return false;
			}


			// Return the contents of the whole file
			$contents = stream_get_contents( $this->link );

			// Rewind to the begining
			if ( !$this->rewind( ) )
			{
				//return false;
			}


			return $contents;
		}


		/**
		 *
		 * Lock file
		 *
		 * @param	void
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		lock
		**/
		public function lock ( )
		{
			// If the file does not support locks
			if ( !$this->is_lockable( ) )
			{
				throw new \RuntimeException("Locking is not supported on this file");
			}


			// Perform an non-blocking exclusive lock of a file
			return flock( $this->link, LOCK_EX | LOCK_NB );
		}


		/**
		 *
		 * Unlock file
		 *
		 * @param	void
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		unlock
		**/
		public function unlock ( )
		{
			// If the file does not support locks
			if ( !$this->is_lockable( ) )
			{
				throw new \RuntimeException("Locking is not supported on this file");
			}


			// Perform an non-blocking unlock of a file
			return flock( $this->link, LOCK_UN | LOCK_NB );
		}


#endregion

#region Operations


		/**
		 *
		 * Copy file
		 *
		 * @param	WebCore\IO\File		$file	Source file
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		copy
		**/
		public function copy ( self $file )
		{
			// If destination file is remote
			if ( !$this->is_local( ) )
			{
				throw new \RuntimeException("Unable to copy file into remote server");
			}

			// Rewind to the begining
			if ( !$this->rewind( ) )
			{
				//return false;
			}


			// Try to copy current file into given stream and catch the response
			$response = stream_copy_to_stream( $file->link, $this->link );

			// if the file was copied
			if ( $response === false )
			{
				return false;
			}

			// Recompute file stats
			if ( !$this->stats( ) )
			{
				// No action needed
			}


			return true;
		}


		/**
		 *
		 * Move file
		 *
		 * @param	self	$file	Destination file
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		move
		**/
		public function move ( self $file )
		{
			// If destination file is remote
			if ( !$file->is_local( ) )
			{
				throw new \RuntimeException("Unable to move remote or virtual files");
			}

			// If we could not copy the file
			if ( !$this->copy( $file ) )
			{
				return false;
			}

			// If we could not delete the file
			if ( !$this->delete( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Delete file
		 *
		 * @param	void
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		delete
		**/
		public function delete ( )
		{
			// If the file is not local
			if ( !$this->is_local( ) )
			{
				throw new \RuntimeException("Unable to delete remote files");
			}

			// if there is no such file anymore
			if ( !is_file( $this->path ) )
			{
				return true;
			}

			// If we were unable to close the file
			if ( !$this->close( ) )
			{
				return false;
			}


			// Close the file handler
			return unlink( $this->path );
		}


		/**
		 *
		 * Touch the file
		 *
		 * @param	int		$time	Modification time
		 * @return	bool
		 * @see		touch
		**/
		public function touch ( $time = null )
		{
			// If the time is not integer
			if ( !is_integer( $time ) )
			{
				// Use default time
				$time = time( );
			}

			// Touch the file with given timestamp
			if ( !touch( $this->path, $time ) );
			{
				return false;
			}


			// Set new modified time
			$this->modified = $time;

			return true;
		}


		/**
		 *
		 * Change file permisions
		 *
		 * @param	int		$value	File mode
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		mode
		**/
		public function mode ( $value )
		{
			// If this file is remote
			if ( !is_file( $this->path ) )
			{
				throw new \RuntimeException("Unable to change the mode of remote files");
			}


			// Change the mode of the file
			return chmod( $this->path, $value );
		}


		/**
		 *
		 * Rename file
		 *
		 * @param	string	$name	New file name
		 * @return	bool
		 * @throws	RuntimeException
		 * @see		rename
		**/
		public function rename ( $name )
		{
			// Get just the name if there is path too
			$name = basename( $name );
			$path = dirname( $this->path )."/{$name}";

			// If this file is remote
			if ( !is_file( $this->path ) )
			{
				throw new \RuntimeException("Unable to rename remote files");
			}

			// If we were unable to close the file
			if ( !$this->close( ) )
			{
				return false;
			}

			// Try tp rename the file to new name
			if ( !rename( $this->path, $path ) )
			{
				return false;
			}


			// Assign new path
			$this->path = $path;

			// Reopen the file
			if ( !$this->reopen( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Output file contents to the browser
		 *
		 * @param	void
		 * @return	bool
		 * @see		output
		**/
		public function output ( )
		{
			// Make the date of last modification time from the file
			$modified = date( "D, d M Y H:i:s \G\M\T", $this->modified );

			// Rewind to the begining
			if ( !$this->rewind( ) )
			{
				//return false;
			}


			// Send appropriate headers for file output
			Header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
			Header("Pragma: no-cache");
			Header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			Header("Content-type: {$this->mimetype}");
			Header("Content-Length: {$this->size}");
			Header("Last-Modified: {$modified}");

			// Call functions to prepare download
			ob_end_clean( );		// Exit and clear the buffer
			session_write_close( );	// End PHP session handler

			// Output the contents to the browser
			$response = fpassthru( $this->link );

			// If we have failed to output contents
			if ( $response === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Download file
		 *
		 * @param	string	$name	Name of downloaded file
		 * @return	bool
		 * @see		download
		**/
		public function download ( $name = null )
		{
			// If there is no filename given or it is not string
			if ( empty( $name ) OR !is_string( $name ) )
			{
				// Use path to the file
				$name = $this->path;
			}


			// Get just the name of the file
			$name = basename( $name );

			// If the headers were sent
			if ( headers_sent( ) )
			{
				return false;
			}


			// Ignore when user presses abor in the browser
			// and disable the time limit for this script
			ignore_user_abort( true );
			set_time_limit( 0 );

			// If the request comes from the Internet Explorer
			if ( strstr( $_SERVER['HTTP_USER_AGENT'], "MSIE" ) )
			{
				// Make sure the filename won't get screwed over because of multiple dots
				$name = preg_replace('/\./', '%2e', $name, substr_count( $name, '.' ) - 1 );
			}


			// Send appropriate headers for download
			Header("Content-Disposition: attachment; filename={$name}");
			Header("Content-Transfer-Encoding: binary");
			Header("Content-Description: File Download");

			// Call functions to prepare download
			ignore_user_abort( );	// Ignore abort requests
			set_time_limit(0);		// Allow more than 30 sec

			// Try to output the file
			if ( !$this->output( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Save file as
		 *
		 * @param	string	$path		Path to a new file
		 * @param	bool	$overwrite	Overwrite existing files
		 * @param	mixed	$file		[out] Saved file instance
		 * @return	bool
		 * @see		save
		**/
		public function save ( $path, $overwrite = false, &$file = null )
		{
			// Try to create new file on given path
			$file = static::Create( $path, $overwrite );

			// If we could not create new file
			if ( $file === false )
			{
				return false;
			}

			// If we failed to write data
			if ( !$file->copy( $this ) )
			{
				return false;
			}


			return true;
		}


#endregion

#region File checksum


		/**
		 *
		 * Get the MD5 footprint
		 *
		 * @param	void
		 * @return	string
		 * @see		md5
		**/
		public function md5 ( )
		{
			// Return the footprint of the file
			return hash( "md5", $this->contents( ) );
		}


		/**
		 *
		 * Get the SHA1 footprint
		 *
		 * @param	void
		 * @return	string
		 * @see		sha1
		**/
		public function sha1 ( )
		{
			// Return the footprint of the file
			return hash( "sha1", $this->contents( ) );
		}


		/**
		 *
		 * Get the CRC32 footprint
		 *
		 * @param	void
		 * @return	string
		 * @see		crc32
		**/
		public function crc32 ( )
		{
			// Return the footprint of the file
			return hash_file( "crc32b", $this->path );
		}


		/**
		 *
		 * Get the BASE64 footprint
		 *
		 * @param	void
		 * @return	string
		 * @see		base64
		**/
		public function base64 ( )
		{
			// Return the file in base64 format
			return base64_encode( $this->contents( ) );
		}


#endregion

#region Helpers


		/**
		 *
		 * Check the file mime-type
		 *
		 * @param	string	$type	Mime-Type pattern
		 * @return	bool
		 * @see		check
		**/
		public function check ( $type )
		{
			// If there is no type
			if ( empty( $type ) )
			{
				return true;
			}

			// If the file type does not match given type pattern
			if ( !preg_match( "#{$type}#is", $this->mimetype ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Get the name of the file
		 *
		 * @param	void
		 * @return	string
		 * @see		base
		**/
		public function base ( )
		{
			return basename( $this->path );
		}


		/**
		 *
		 * Check for locks in stream
		 *
		 * @param	void
		 * @return	bool
		 * @see		is_lockable
		**/
		public function is_lockable ( )
		{
			// Check wether the stream supports locks
			return stream_supports_lock( $this->link );
		}


		/**
		 *
		 * Check for local stream
		 *
		 * @param	void
		 * @return	bool
		 * @see		is_local
		**/
		public function is_local ( )
		{
			// Check wether the stream is opened to local file
			return stream_is_local( $this->link );
		}


		/**
		 *
		 * Set stream character set
		 *
		 * @param	string	$charset	Character set for the stream
		 * @return	bool
		 * @see		encoding
		**/
		public function encoding ( $charset )
		{
			// If there is no such function available
			if ( !function_exists("stream_encoding") )
			{
				return false;
			}


			// Set the encoding of the stream to given charset
			return stream_encoding( $this->link, $charset );
		}


		/**
		 *
		 * Gather file statistics
		 *
		 * @param	void
		 * @return	bool
		 * @see		stats
		**/
		public function stats ( )
		{
			// Get the statistics from the stream
			$stats = fstat( $this->link );

			// Assign statistics from the fstat
			$this->size = $stats['size'];		// File size
			$this->created = $stats['ctime'];	// Creation time
			$this->modified = $stats['mtime'];	// Modification time


			// Load metadata from the stream
			$metadata = stream_get_meta_data( $this->link );

			// Assign statistics from the stream meta data
			$this->wrapper = strToUpper( $metadata['wrapper_type'] );
			$this->stream = strToUpper( $metadata['stream_type'] );
			$this->seekable = $metadata['seekable'];
			$this->mode = $metadata['mode'];
			$this->path = $metadata['uri'];

			// If there is information about stream blocking
			if ( array_key_exists( 'blocked', $metadata ) )
			{
				// Gather that information for the file
				$this->blocked = $metadata['blocked'];
			}


			// Get the name of the file and extension
			$path = pathinfo( $this->path );
			$this->name = $path['filename'];

			// if there is an extension in the path information
			if ( array_key_exists( 'extension', $path ) )
			{
				// get the extension to the type hodler
				$this->type = $path['extension'];
			}

			// If the path leads to a file
			if ( is_file( $this->path ) )
			{
				// Get the content type from the file path
				$this->mimetype = @mime_content_type( $this->path ); // ftp gives warnings
			}

			// Do not continue for local streams
			if ( stream_is_local( $this->link ) )
			{
				return true;
			}

			// Iterate through request headers from the wrapper
            if ( isset( $metadata['wrapper_data'] ) )
			foreach ( $metadata['wrapper_data'] as $header )
			{
				// Explode the header into pieces
				$header = explode( ": ", $header );

				// Switch the header
				switch ( $header[0] )
				{
					// Mime-Type of the file
					case "Content-Type":

						// Get the size of the file
						$this->mimetype = $header[1];
						$this->mimetype = explode( "; ", $this->mimetype );
						$this->mimetype = current( $this->mimetype );

					break;

					// Size of the file
					case "Content-Length":

						// Get the size of the file
						$this->size = intval( $header[1] );

					break;

					// Modification time
					case "Last-Modified":

						// Get the size of the file
						$this->modified = strToTIme( $header[1] );

					break;
				}
			}

            // If we have failed to load mtime
            if ( empty( $this->modified ) )
            {
                // Fallback to tbe filemtime function
                $this->modified = filemtime( $this->path );
            }

            // If we have failed to load mtime
            if ( empty( $this->size ) )
            {
                // Fallback to tbe filemtime function
                $this->size = filesize( $this->path );
            }

			return true;
		}


		/**
		 *
		 * Reopen the file handler
		 *
		 * @param	void
		 * @return	bool
		 * @see		reopen
		**/
		public function reopen ( )
		{
			// Open the file for reading and writing if available
			$this->link = fopen( $this->path, $this->mode, false );

			// We could not open file pointer
			if ( $this->link === false )
			{
				return false;
			}

			// Gather file information
			if ( !$this->stats( ) )
			{
				return false;
			}


			// Use UTF-8 encoding by default
			$this->encoding("UTF-8");

			return true;
		}


		/**
		 *
		 * Close the file
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		public function close ( )
		{
			// If there is no file opened
			if ( !is_resource( $this->link ) )
			{
				return true;
			}

			// Close the file handler
			if ( !fclose( $this->link ) )
			{
				return false;
			}


			// Clear the holder
			$this->link = null;

			return true;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

