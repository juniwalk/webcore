<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2014-03-12 21:36
	* @file:		Dir library
	* @copyright:	(c) 2014s Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DIR LIBRARY


	namespace WebCore\IO;


	class Dir
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Holder
		**/
		protected $link			= null;
		protected $path			= null;
		protected $stream		= null;
		protected $wrapper		= null;


		/**
		 * Properties
		**/
		protected $name			= null;
		protected $mode			= null;
		protected $size			= null;
		protected $blocked		= null;
		protected $seekable		= null;
		protected $created		= null;
		protected $modified		= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		protected function __construct ( ) { }


		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	bool
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			// Close the file
			return $this->close( );
		}


		/**
		 *
		 * <<magic>> Getter
		 *
		 * @param	string	$key	Property name
		 * @return	string
		 * @throws	RuntimeException
		 * @see		__get
		**/
		public function __get ( $key )
		{
			// If there is no such property
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}


			// Return the value
			return $this->$key;
		}


#region Instance


		/**
		 *
		 * Create new folder
		 *
		 * @param	string	$dirname	Path to folder
		 * @param	int		$mode		Attributes
		 * @param	bool	$recursive	Use recursive creation?
		 * @return	bool
		 * @see		Create
		**/
		public static function Create ( $dirname, $mode = 0755, $recursive = false )
		{
			// If there already is such folder
			if ( is_dir( $dirname ) )
			{
				return false;
			}

			// Try to create specified folder
			if ( !mkdir( $dirname, $mode, $recursive ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Remove folder
		 *
		 * @param	string	$dirname	Path to folder
		 * @return	bool
		 * @see		Remove
		**/
		public static function Remove ( $dirname )
		{
			// If there is no such folder
			if ( !is_dir( $dir->path ) )
			{
				return false;
			}

			// Try to remove specified folder
			if ( !rmdir( $dirname ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Open the folder
		 *
		 * @param	string	$dirname	Path to folder
		 * @return	WebCore\IO\Dir
		 * @see		Open
		**/
		public static function Open ( $dirname )
		{
			// Create Dir Instance
			$dir = new self( );
			$dir->path = rtrim( $dirname, '/' );

			// If there is no such folder
			if ( !is_dir( $dir->path ) )
			{
				return false;
			}


			// Open the folder handler
			$dir->link = opendir( $dir->path );

			// We could not open dir pointer
			if ( $dir->link === false )
			{
				return false;
			}

			// Gather file information
			if ( !$dir->stats( ) )
			{
				return false;
			}


			return $dir;
		}


#endregion

#region Read & Write


		/**
		 *
		 * Get contents of the dir
		 *
		 * @param	void
		 * @return	array
		 * @see		contents
		**/
		public function contents ( )
		{
			// While there are any lines to read
			while ( $item = readdir( $this->link ) )
			{
				// Buil the path to the item
				$path = "{$this->path}/{$item}";

				// Ignore the dots from content
				if ( $item == '.' OR $item == '..' )
				{
					continue;
				}

				// If the item is a folder
				if ( is_dir( $path ) )
				{
					// Open the folder instance
					$item = Dir::Open( $path );
				}

				// If the item is a file
				if ( is_file( $path ) )
				{
					// Open the file instance
					$item = File::Open( $path );
				}


				// Yield the item
				yield $item;
			}
		}


#endregion

#region Helpers


		/**
		 *
		 * Gather file statistics
		 *
		 * @param	void
		 * @return	bool
		 * @see		stats
		**/
		protected function stats ( )
		{
			// Load metadata from the stream
			$metadata = stream_get_meta_data( $this->link );

			// Assign statistics from the stream meta data
			$this->wrapper = strToUpper( $metadata['wrapper_type'] );
			$this->stream = strToUpper( $metadata['stream_type'] );
			$this->seekable = $metadata['seekable'];
			$this->mode = $metadata['mode'];

			// If there is information about stream blocking
			if ( array_key_exists( 'blocked', $metadata ) )
			{
				// Gather that information for the file
				$this->blocked = $metadata['blocked'];
			}


			// Get the name of the file and extension
			$path = pathinfo( $this->path );
			$this->name = $path['filename'];
			$this->created = filectime( $this->path );
			$this->modified = filemtime( $this->path );

			// Try to get the size of the folder
			if ( !$this->size( $this->path ) )
			{
				$this->size = 0;
			}


			return true;
		}


		/**
		 *
		 * Compute folder size
		 *
		 * @param	string	$path	Path to folder
		 * @return	bool
		 * @see		size
		**/
		protected function size ( $path )
		{
			// Scan the dir for files
			$files = scandir( $path );

			// If we faield to scan the dir
			if ( $files === false )
			{
				return false;
			}

			// Iterate through the contents
			foreach ( $files as $name )
			{
				// Get the path to file
				$file = "{$path}/{$name}";

				// If the file name are the dots
				if ( $name == '.' OR $name == '..' )
				{
					continue;
				}

				// If there is subdir
				if ( is_dir( $file ) )
				{
					// Get the size of files in the subdir
					$this->size( $file ); continue;
				}


				// Get the size of the file in dir
                $this->size += filesize( $file );
            }


			return true;
		}


		/**
		 *
		 * Close the file
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		public function close ( )
		{
			// If there is no file opened
			if ( !is_resource( $this->link ) )
			{
				return true;
			}

			// Close the file handler
			if ( !closedir( $this->link ) )
			{
				return false;
			}


			// Clear the holder
			$this->link = null;

			return true;
		}


#endregion

	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

