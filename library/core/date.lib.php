<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-26 16:47
	* @file:		Date library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DATE LIBRARY


	namespace WebCore\Core;


	final class Date extends \DateTime
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Formats
		**/
		const DATETIME			= "j.n.Y G:i:s";
		const TIMESTAMP			= "Y-m-d H:i:s";


		/**
		 * Empty timestamp
		**/
		const TIMESTAMP_EMPTY	= "00-00-00 00:00:00";


		/**
		 * DateTime
		**/
		public $date			= null;


		/**
		 * TimeZone
		**/
		public $timezone		= null;
		public $timezone_type	= null;



		/**
		 *
		 * <<marig>> To String
		 *
		 * @param	mixed			$time		Current time
		 * @param	\DateTimeZone	$timezone	Timezone object
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( $time = "NOW", \DateTimeZone $timezone = null )
		{
			// If the time is an timestamp
			if ( is_numeric( $time ) )
			{
				// Convert it into the string representation
				$time = date( self::TIMESTAMP, $time );
			}


			// Call parent constructor from here
			parent::__construct( $time, $timezone );
		}


		/**
		 *
		 * Current timestamp
		 *
		 * @param	void
		 * @return	self
		 * @see		NOW
		**/
		public static function NOW ( )
		{
			return new self( 'now', null );
		}



		/**
		 *
		 * <<marig>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			// Return the date formated to the timestamp
			return $this->format( self::TIMESTAMP );
		}


		/**
		 *
		 * Add date
		 *
		 * @param	mixed	$int	Interval spec
		 * @return	void
		 * @see		add
		**/
		public function add ( $int )
		{
			// If the interval is string
			if ( is_string( $int ) )
			{
				// Create instance of the interval
				$int = new \DateInterval( $int );
			}


			// Call parent add method
			return parent::add( $int );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

