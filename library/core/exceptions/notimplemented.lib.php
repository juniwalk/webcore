<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		NotImplemented exception
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	NOTIMPLEMENTED EXCEPTION


	namespace WebCore\Core\Exceptions;


	final class NotImplemented extends \Exception
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	string	$method	Name of the method
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( $method )
		{
			parent::__construct("{$method} is not implemented yet");
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

