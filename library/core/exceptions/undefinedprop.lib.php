<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-25 20:40
	* @file:		WebCore library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	WEBCORE LIBRARY


	namespace WebCore\Core\Exceptions;


	final class UndefinedProp extends \Exception
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	string	$key	Property name
		 * @param	string	$class	Class name
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( $key, $class )
		{
			parent::__construct("Undefined property: {$class}::\${$key}");
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

