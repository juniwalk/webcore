<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-25 20:40
	* @file:		WebCore library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	WEBCORE LIBRARY


	namespace WebCore\Core\Exceptions;


	final class InvalidArgument extends \InvalidArgumentException
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	string	$method	Name of the method
		 * @param	string	$type	Expected type
		 * @param	mixed	$given	Reference to given value
		 * @param	int		$num	Number of argument
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( $method, $type, $given, $num )
		{
			parent::__construct("Argument {$num} passed to {$method}() must be an {$type}, ".gettype($given)." given");
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

