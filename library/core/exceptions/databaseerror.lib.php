<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		DatabaseError exception
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DATABASEERROR EXCEPTION


	namespace WebCore\Core\Exceptions;


	final class DatabaseError extends \Exception
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1.2;
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

