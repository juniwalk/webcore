<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-26 14:27
	* @file:		Crypto library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	CRYPTO LIBRARY


	namespace WebCore\Core;


	use \WebApp;
	use \WebCore\Core\Exceptions;


	class Crypto
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		final private function __construct ( ) { }


		/**
		 *
		 * Create salt
		 *
		 * @param	int		$length		Length of the hash
		 * @return	string
		 * @see		Salt
		**/
		public static function Salt ( $length )
		{
			// Create hash from the salt
			$salt = uniqid( rand( ), true );
			$salt = hash( "sha512", $salt );

			// If we need only part
			if ( $length > 0 )
			{
				$salt = substr( $salt, 0, $length );
			}


			return $salt;
		}


		/**
		 *
		 * Create user password
		 *
		 * @param	string	$password	UnEncrypted user password
		 * @param	string	$salt		User salt
		 * @param	string	$user		User name
		 * @return	string
		 * @see		Password
		**/
		public static function Password ( $password, $salt, $user = null )
		{
			// Access instance of the Site's config
			$config = &WebApp\Config::Factory( );

			// If the security level in normal
			if ( $config->APP_SECURITY < 2 )
			{
				$user = strToLower( $user );
			}

			// If the security level in weak
			if ( $config->APP_SECURITY < 1 )
			{
				$user = null;
			}


			// Create password string from the
			// user password and its user name
			$password = "{$password}:{$user}";

			// Return the password in encrypted format
			return sha1( $password . md5( $salt ) );
		}


		/**
		 *
		 * Create GUID string
		 *
		 * @param	string	$salt	Salt string
		 * @return	string
		 * @throws	Exception|InvalidArgument
		 * @see		GUID
		**/
		public static function GUID ( $salt )
		{
			// Guid holder
			$guid = array( );

			// If the salt is not a string
			if ( !is_string( $salt ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $salt, 1 );
			}

			// If the salt is too short
			if ( strlen( $salt ) < 32 )
			{
				throw new \Esception("Length of the salt for GUID muset be atleast 32 characters");
			}


			// Hash value for the Visitor::Host
			$guid[0] = substr( $salt, 0, 8 );
			$guid[1] = substr( $salt, 8, 4 );
			$guid[2] = substr( $salt, 12, 4 );
			$guid[3] = substr( $salt, 16, 4 );
			$guid[4] = substr( $salt, 20, 12 );

			// Implode the guid and make it uppercase
			$guid = implode( "-", $guid );

			// Return created GUID in uppercase
			return strToUpper("{{$guid}}");
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

