<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-26 16:47
	* @file:		Date library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DATE LIBRARY


	namespace WebCore\Core;


	use \WebCore\Debug as XDEBUG;
	use \WebCore\Net;
	use \WebCore\User;
	use \WebCore\Web;


	abstract class Task
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Property firewall
		**/
		const FIREWALL		= 'execute,source,notify,redirect';


		/**
		 * Redirect URL
		**/
		protected $url		= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		final public function __construct ( )
		{
			// Access firewalled properties definition
			$firewall = explode( ",", self::FIREWALL );

			// Iterate through each asset in the request
			foreach ( $_REQUEST as $key => $__value )
			{
				// Access the value from the request query
				if ( !Web\Request::Query( $key, $value ) )
				{
					continue;
				}

				// If the property is firewalled
				if ( in_array( $key, $firewall ) )
				{
					continue;
				}


				// Assign value to task
				$this->assign( $key, $value );
			}


			// Create default URL instance
			$this->url = Net\URL::Current( );

			// Access redirect and referer URLs
			$referer = Net\URL::Referer( );
			$redirect = Net\URL::Redirect( );

			// If there is referer URL
			if ( is_object( $referer ) )
			{
				// Assign referer instance
				$this->url = &$referer;
			}

			// If there is redirect URL
			if ( is_object( $redirect ) )
			{
				// Assign Redirect instance
				$this->url = &$redirect;
			}

			// Iterate through each firewall key
			foreach ( $firewall as $key )
			{
				// Remove the key from the URL
				$this->url->remove( $key );
			}

			// If there is an entry property in task
			if ( property_exists( $this, 'entry' ) )
			{
				unset( $this->entry[0] );
			}
		}


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		final public function __toString ( )
		{
			return get_called_class( );
		}



		/**
		 *
		 * Access user instance
		 *
		 * @param	void
		 * @return	User
		 * @see		user
		**/
		final public function &user ( )
		{
			// Return User instance
			return User\User::Factory( );
		}


		/**
		 *
		 * Task preparation
		 *
		 * @param	XDEBUG\Log	$log	Logger instance
		 * @return	bool
		 * @see		prepare
		**/
		public function prepare ( XDEBUG\Log &$log )
		{
			// Access user instance
			$user = $this->user( );

			// If the user is not signed
			if ( !$user->signed( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Task execution
		 *
		 * @param	XDEBUG\Log	$log	Logger instance
		 * @return	bool
		 * @see		execute
		**/
		abstract public function execute ( XDEBUG\Log &$log = null );


		/**
		 *
		 * Assign new redirect URL
		 *
		 * @param	Net\URL	&$url	URL instance
		 * @return	Net\URL
		 * @see		url
		**/
		final public function url ( Net\URL &$url = null )
		{
			// If there new URL instance
			if ( !is_null( $url ) )
			{
				// Assign URL instance
				$this->url = &$url;
			}


			return $this->url;
		}



		/**
		 *
		 * Assign new value
		 *
		 * @param	string	$key	Property
		 * @param	string	$value	Value
		 * @return	bool
		 * @see		assign
		**/
		protected function assign ( $key, $value )
		{
			$this->$key = $value;
		}


		/**
		 *
		 * Has user access?
		 *
		 * @param	string	$access	Access level
		 * @return	bool
		 * @see		CanAccess
		**/
		public function CanAccess ( $access = 'user' )
		{
			// Access user instance
			$user = $this->user( );

			// If the user is not signed
			if ( !$user->signed( ) )
			{
				return false;
			}

			// If the user does not have access
			if ( !$user->can( $access ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Assign notification
		 *
		 * @param	string	$key	Notification
		 * @return	bool
		 * @see		notify
		**/
		final protected function notify ( $key )
		{
			// If there is no URL instance
			if ( is_null( $this->url ) )
			{
				return false;
			}


			// Assign notification to the URL
			$this->url->set( 'notify', strToUpper( $key ) );

			return false;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

