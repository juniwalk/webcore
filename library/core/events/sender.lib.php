<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-23 00:11
	* @file:		Sender library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	SENDER LIBRARY


	namespace WebCore\Core\Events;


	abstract class Sender
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Callbacks holder
		**/
		private $callbacks		= array( );



		/**
		 *
		 * Attach new event listener
		 *
		 * @param	string		$event		Name of the event
		 * @param	string		$method		Method name
		 * @param	object		$class		Object reference
		 * @return	void
		 * @see		attach
		**/
		protected function attach ( $event, $method, &$class = null )
		{
			// Callback is just method name
			$callback = $method;

			// If there is an object defined
			if ( is_object( $class ) )
			{
				// Create callback from the class/method
				$callback = array( &$class, $method );
			}

			// If the callback is not callable
			if ( !is_callable( $callback ) )
			{
				throw new \Exception("");
			}

			// If there is no such event registered
			if ( !$this->registered( $event ) )
			{
				// Register new event handler
				$this->register( $event );
			}


			// Assign listener to the holder in this dispatcher object
			$this->callbacks[ $event ][] = &$callback;
		}


		/**
		 *
		 * Dispatch event
		 *
		 * @param	string	$action		Name of the event
		 * @param	array	$params		Params for the event
		 * @param	Event	$event		Event class
		 * @return	bool
		 * @see		dispatch
		**/
		protected function dispatch ( $action, array &$params = array( ), Event &$event = null )
		{
			// If there is no event
			if ( is_null( $event ) )
			{
				// Create generic event for handling
				$event = new Event( $action, $params );
			}


			// Look for the callbacks for this event
			$callbacks = $this->callbacks( $action );

			// If there are no callbacks
			if ( empty( $callbacks ) )
			{
				return true;
			}

			// Iterate through each callback subscribed
			foreach ( $callbacks as $callback )
			{
				// If the propagation has been stopped
				if ( !$event->propagate( ) )
				{
					break;
				}

				// If the callback execution failed
				if ( !$callback( $this, $event ) )
				{
					return false;
				}
			}


			return true;
		}


		/**
		 *
		 * Get the list of callbacks
		 *
		 * @param	string	$event		Name of the event
		 * @return	array
		 * @see		callbacks
		**/
		protected function callbacks ( $event )
		{
			// If there is no such event registered
			if ( !$this->registered( $event ) )
			{
				return array( );
			}


			// Return list of callbacks for the event
			return $this->callbacks[ $event ];
		}



		/**
		 *
		 * Register new event
		 *
		 * @param	string	$event		Name of the event
		 * @return	void
		 * @see		register
		**/
		protected function register ( $event )
		{
			// If the event is already registered
			if ( $this->registered( $event ) )
			{
				// throw new
				return null;
			}


			// Get the first two letters from the event name
			$check = substr( $event, 0, 2 );
			$check = strToLower( $check );

			// If the event does not start with On word
			if ( $check !== 'on' )
			{
				// throw new
				return null;
			}


			// Store new registered event
			$this->callbacks[ $event ] = array( );
		}


		/**
		 *
		 * Is the event registered?
		 *
		 * @param	string	$event		Name of the event
		 * @return	bool
		 * @see		registered
		**/
		protected function registered ( $event )
		{
			return array_key_exists( $event, $this->callbacks );
		}


		/**
		 *
		 * Unregister event handler
		 *
		 * @param	string	$event		Name of the event
		 * @return	bool
		 * @see		unregister
		**/
		protected function unregister ( $event )
		{
			// If the event is already registered
			if ( $this->registered( $event ) )
			{
				return false;
			}


			// Remove the registered event
			unset( $this->callbacks[ $event ] );

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

