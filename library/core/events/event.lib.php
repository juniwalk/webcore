<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-23 00:25
	* @file:		Event library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	EVENT LIBRARY


	namespace WebCore\Core\Events;


	class Event
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Event properties
		**/
		public $name			= null;
		public $params			= null;


		/**
		 * Continue propagation?
		**/
		protected $propagate	= true;



		/**
		 *
		 * Constructor
		 *
		 * @param	string	$name		Name of the event
		 * @return	array	$params		Parameters from the event
		 * @see		__construct
		**/
		public function __construct ( $name, array &$params = array( ) )
		{
			$this->name = $name;
			$this->params = &$params;
		}


		/**
		 *
		 * Stop any other event propagation
		 *
		 * @param	void
		 * @return	void
		 * @see		stopPropagation
		**/
		public function stopPropagation ( )
		{
			$this->propagate = false;
		}


		/**
		 *
		 * Get the propagate property
		 *
		 * @param	void
		 * @return	bool
		 * @see		propagate
		**/
		public function propagate ( )
		{
			return $this->propagate;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

