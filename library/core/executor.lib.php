<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-26 22:34
	* @file:		Executor library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	EXECUTOR LIBRARY


	namespace WebCore\Core;


	use \WebCore\Core\Exceptions;
	use \WebCore\Web;
	use \WebCore\Debug;


	final class Executor
	{
		/**
		 * Version holder
		**/
		const VERSION	= 1;


		/**
		 * Version holder
		**/
		const CALLID	= '%s\Tasks\%s';



		/**
		 *
		 * <<marig>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( ) { }



		/**
		 *
		 * Check for pending tasks
		 *
		 * @param	void
		 * @return	string
		 * @see		Pending
		**/
		public static function Pending ( )
		{
			// Access request query and get name of the task
			Web\Request::Query( 'execute', $task, null );
			Web\Request::Query( 'source', $source, "WebApp" );

			// If the task is not a string
			if ( !is_string( $task ) )
			{
				return false;
			}


			// Create path to the task using source and name
			$class = sprintf( self::CALLID, $source, $task );

			// If there is no such class
			if ( !class_exists( $class ) )
			{
				return false;
			}


			return $class;
		}


		/**
		 *
		 * Execute pending task
		 *
		 * @param	void
		 * @return	void
		 * @see		Execute
		**/
		public static function Execute ( )
		{
			// Access pending task name
			// and create instance of it
			$task = self::pending( );
			$task = new $task( );

			// Check Task's validity
			if ( !$task instanceof Task )
			{
				throw new \Exception("Failed to execute '{$task}', task not executable");
			}


			// Prepare logging instance and
			// assign name of the task file
			$log = Debug\Log::Factory("{$task}");
			$log->task = (string) $task;

			// If task failed to prepare
			if ( !$task->prepare( $log ) )
			{
				// Redirect to the Task
				self::redirect( $task, $log, false );
			}

			// If task failed to execure
			if ( !$task->execute( $log ) )
			{
				// Redirect to the Task
				self::redirect( $task, $log, false );
			}


			// Redirect to the Task
			self::redirect( $task, $log, true );
		}


		/**
		 *
		 * Redirect to the URL
		 *
		 * @param	Task		$task	Task instance
		 * @param	Debug\Log	$log	Logger instance
		 * @param	bool		$status	Task status
		 * @return	void
		 * @see		redirect
		**/
		private static function redirect ( Task &$task, Debug\Log &$log = null, $status )
		{
			// Access Redirect URL
			$url = $task->url( );

			// Clear the request holder
			$_SESSION['request'] = array( );

			// If the log is enabled
			if ( !is_null( $log ) )
			{
				// Assign status of the task
				$log->status = $status;

				// If the task has failed to proceed
				if ( $log->status == false )
				{
					// Copy the GET and POST superglobals into the request
					$_SESSION['request'] = array_merge( $_GET, $_POST );
				}

				// If there is URL to access
				if ( !is_null( $url ) )
				{
					// Save the notification into the log
					$log->notify = $url->get('notify');
				}

				// Try to write the log
				if ( !$log->write( ) )
				{
					Debug\Debug::Dump("Unable to write the log");
				}


				// Print the log instance
				Debug\Debug::Dump( $log );
			}


			// Print the Task instance
			Debug\Debug::Dump( $task );

			// If there is no URL to redirect to
			if ( is_null( $url ) )
			{
				exit;
			}


			// Redirect to given URL instance
			Web\Request::Redirect( $url );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

