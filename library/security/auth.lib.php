<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		Auth library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	AUTH LIBRARY


	namespace WebCore\Security;


	use \WebCore\DB;
	use \WebCore\Core\Exceptions;
	use \WebCore\Data;


	final class Auth extends Data\DSN
	{
		/**
		 * Version holder
		**/
		const VERSION	= 1;



		/**
		 *
		 * Change DSN data
		 *
		 * @param	string	$data	DSN Data
		 * @return	string
		 * @see		data
		**/
		public function data ( $dsn )
		{
			$this->parse( $dsn );
		}


		/**
		 *
		 * Fill data from array
		 *
		 * @param	array	$data	List of values
		 * @return	bool
		 * @see		fromArray
		**/
		public function fromArray ( array $data )
		{
			// If the holder is empty
			if ( empty( $data ) )
			{
				return false;
			}

			// Walk through each value in the holder
			foreach ( $data as $key => $value )
			{
				// Assign the property
				$this->$key = $value;
			}


			return true;
		}


		/**
		 *
		 * Validate credentials
		 *
		 * @param	void
		 * @return	bool
		 * @see		validate
		**/
		public function validate ( )
		{
			// Access database scheme
			$scheme = $this->scheme;

			// If the scheme is not a string
			if ( !is_string( $scheme ) )
			{
				return false;
			}

			// Switch the scheme
			switch ( $scheme )
			{
				case "ssh";
				case "ssl";
				case "tls";

					// Check main login credentials for database
					if ( !is_string( $this->host ) ) return false;
					if ( !is_numeric( $this->port ) ) return false;
					if ( !is_scalar( $this->user ) ) return false;
					if ( !is_scalar( $this->pass ) ) return false;

				break;

				case DB\DB::MSSQL:
				case DB\DB::MYSQL:
				case DB\DB::MYSQLI:

					// Check main login credentials for database
					if ( !is_string( $this->host ) ) return false;
					if ( !is_scalar( $this->user ) ) return false;
					if ( !is_scalar( $this->pass ) ) return false;

				break;

				case DB\DB::SQLITE:
				case DB\DB::SQLITE3:

					// Check host name for SQLite database
					if ( !is_string( $this->host ) ) return false;

					// If we are not using memory type
					if ( $this->host != ":memory:" )
					{
						// Check the path to the database file
						if ( !is_string( $this->path ) ) return false;
					}


					// Trim the host of any trailing slashes
					$this->host = trim( $this->host, '\/' );

					// If there is no relative mark in the host
					if ( strpos( $this->host, '.' ) === false )
					{
						// Add slash to the host name
						$this->host = "/{$this->host}";
					}

				break;

				// Unknown scheme provided
				default: return false;
			}


			// Remove unwanted characters from the end
			$this->path = trim( $this->path, '\/' );

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

