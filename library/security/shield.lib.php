<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2012-11-9 14:47
	* @file:		Shield library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	SHIELD LIBRARY


	namespace WebCore\Security;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB;
	use \WebCore\User;
	use \WebCore\Web;


	final class Shield
	{
		/**
		 * Version holder
		**/
		const VERSION				= 2;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( ) { }


		/**
		 *
		 * Autohirze visitor's access
		 *
		 * @param	User\Visitor	$visitor	Visitor object
		 * @return	bool
		 * @see		Authorize
		**/
		public static function Authorize ( User\Visitor $visitor )
		{
			// Access the instance of the Site
			$site = &Web\Site::Factory( );

			// If the user is banned
			if ( $visitor->ban )
			{
				return false;
			}

			// If the site is disabled
			if ( $site->disabled )
			{
				return false;
			}

			// If the site shield is disabled
			if ( !$site->shield )
			{
				return true;
			}

			// If the visitor does not have enough access to the site
			if ( !User\UAC::Authorize( $visitor, $site->access ) )
			{
				return false;
			}

			// If there is no entry in the shield for access
			if ( !self::Allowed( $visitor, $site->alias ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Lock down the site access
		 *
		 * @param	void
		 * @return	void
		 * @see		Lockdown
		**/
		public static function Lockdown ( )
		{
			// Access Smarty instance
			$smarty = &\Smarty::Factory( );


			// TODO: Display Access Denied error site using Smarty!

			// $menu->get('403')->display( ); exit;

			echo "Access denied! {$_SERVER['REMOTE_ADDR']}"; exit;
		}


		/**
		 *
		 * Check visitor's access privileges
		 *
		 * @param	User\Visitor	$visitor	Visitor object
		 * @param	string			$site		Site alias
		 * @return	bool
		 * @see		Allowed
		**/
		public static function Allowed ( User\Visitor $visitor, $site )
		{
			// Access the WebCore database
			$db = DB\DB::Get('webcore');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}

			// If the site is not a string
			if ( !is_string( $site ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $site, 2 );
			}


			// Build SQL query to select the Shield entry from WebCore database which will validate access to current site for the visitor
			$select = DB\SQL::Set("SELECT * FROM `site_shield` WHERE `user` = '{$visitor->alias}' AND `site` = '{{$site}}' AND `disabled` = 0;");

			// If we were unable to select config
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there are no entries
			if ( !$db->numrows( ) )
			{
				return false;
			}


			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

