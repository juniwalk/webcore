<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-30 11:12
	* @file:		Check library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	CHECK LIBRARY


	namespace WebCore\Security;


	use \WebApp;
	use \WebCore\Core\Exceptions;
	use \WebCore\DB;


	class Check
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Check patterns
		**/
		const GUID			= '/\{([a-z0-9]{8})\-([a-z0-9]{4})\-([a-z0-9]{4})\-([a-z0-9]{4})\-([a-z0-9]{12})\}/i';



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		final private function __construct ( ) { }



		/**
		 *
		 * Check integrity of the string
		 *
		 * @param	mixed	$value	Value to check
		 * @return	bool
		 * @see		Integrity
		**/
		public static function Integrity ( &$value )
		{
			// If the value is an array
			if ( is_array( $value ) )
			{
				// Make recoursive integrity check of each entry
				return array_map( __METHOD__, $value );
			}

			// If the value is not scalar
			if ( !is_scalar( $value ) )
			{
				return false;
			}


			// Remove white space from the value
			$value = trim( $value, "\t\r\n " );

			// If the value is empty
			if ( empty( $value ) )
			{
				return true;
			}


			// Escape the value for security
			$value = DB\DB::Escape( $value );

			return true;
		}


		/**
		 *
		 * Validate given Username string
		 *
		 * @param	mixed	$value		Value to check
		 * @param	string	$pattern	Validation pattern
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		Username
		**/
		public static function Username ( $value, $pattern = null )
		{
			// Access web application config
			$config = &WebApp\Config::Factory( );

			// If there is no pattern for value
			if ( !is_string( $pattern ) )
			{
				// Access value from the config
				$pattern = $config->VALIDATOR_USERNAME;
			}

			// if the pattern doesn't match the value
			if ( !preg_match( $pattern, $value ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Validate given Password string
		 *
		 * @param	string	$value		Value to check
		 * @param	string	$pattern	Validation pattern
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		Password
		**/
		public static function Password ( $value, $pattern = null )
		{
			// Access web application config
			$config = &WebApp\Config::Factory( );

			// If there is no pattern for value
			if ( !is_string( $pattern ) )
			{
				// Access value from the config
				$pattern = $config->VALIDATOR_PASSWORD;
			}

			// if the pattern doesn't match the value
			if ( !preg_match( $pattern, $value ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Validate given Email string
		 *
		 * @param	string	$value	Value to check
		 * @param	bool	$bypass	Bypass DNS check
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		Email
		**/
		public static function Email ( $value, $bypass = false )
		{
			// If the value is not string
			if ( !is_string( $value ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $value, 1 );
			}


			// Get the domain delimiter
			$atIndex = strrpos( $value, "@" );

			// If the delimiter is not present
			if ( $atIndex === false )
			{
				return false;
			}


			// Get the chunks of the email for validation
			$domain = substr( $value, $atIndex +1 );
			$domainLen = strlen( $domain );

			$local = substr( $value, 0, $atIndex );
			$localLen = strlen( $local );

			// Check the length of local part
			if ( $localLen < 1 OR $localLen > 64 )
			{
				return false;
			}

			// Check the length of domain part
			if ( $domainLen < 1 OR $domainLen > 255 )
			{
				return false;
			}

			// Check that the local part does not start with dot
			if ( $local[0] == '.' OR $local[ $localLen -1 ] == '.' )
			{
				return false;
			}

			// Check that email don't have two consecutive dots
			if ( preg_match( '/\\.\\./', $local.$domain ) )
			{
				return false;
			}

			// Validate the domain part
			if ( !preg_match( '/^[A-Za-z0-9\\-\\.]+$/', $domain ) )
			{
				return false;
			}

			if ( !preg_match( '/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace( "\\\\", "", $local ) ) )
			{
				// Look for not valid character in local part unless local part is quoted
				if ( !preg_match( '/^"(\\\\"|[^"])+"$/', str_replace( "\\\\", "", $local ) ) )
				{
					return false;
				}
			}

			// If we should not bypass the MX check
			if ( $bypass == false )
			{
				// If the MX entry does not match
				if ( !checkdnsrr( $domain, "MX" ) )
				{
					return false;
				}

				// If the A entry does not match
				if ( !checkdnsrr( $domain, "A" ) )
				{
					return false;
				}
			}


			return true;
		}


		/**
		 *
		 * Check if the email should not be allowed
		 *
		 * @param	string	$value		Value to check
		 * @param	string	$domains	List of domains to filter
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		EmailAllowed
		**/
		public static function EmailAllowed ( $value, $domains = null )
		{
			// If the value is not string
			if ( !is_string( $value ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $value, 1 );
			}


			// Access web application config
			$config = &WebApp\Config::Factory( );

			// If there are no domains to filter
			if ( !is_string( $domains ) )
			{
				// Access value from the config
				$domains = $config->VALIDATOR_EMAIL_DOMAINS;
			}


			// Get the list of prohibited email domains
			$domains = explode( ',', $domains );

			// if there are no domains
			if ( empty( $domains ) )
			{
				return true;
			}


			// Get the username and the domain from the email address
			list ( $username, $domain ) = explode( "@", $email );

			// If the email is in domains array
			if ( in_array( $domain, $domains ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Validate given GUID string
		 *
		 * @param	string	$value	Value to check
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		GUID
		**/
		public static function GUID ( $value )
		{
			// If the value is not string
			if ( !is_string( $value ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $value, 1 );
			}

			// Compare value to defined GUID pattern
			if ( !preg_match( self::GUID, $value ) )
			{
				return false;
			}


			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

