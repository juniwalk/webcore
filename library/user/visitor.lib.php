<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-28 20:00
	* @file:		Visitor library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	VISITOR LIBRARY


	namespace WebCore\User;


	use \WebCore\DB;


	class Visitor
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Properties
		**/
		public $id			= 0;
		public $alias		= 'guest';
		public $firstname	= null;
		public $lastname	= null;
		public $email		= null;
		public $host		= null;
		public $note		= null;
		public $access		= 'guest';
		public $ban			= 0;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		protected function __construct ( ) { }


		/**
		 *
		 * Factory
		 *
		 * @param	void
		 * @return	Config
		 * @see		Factory
		**/
		public static function &Factory ( )
		{
			// Instance holder
			static $class = null;

			// If there is no instance
			if ( is_null( $class ) )
			{
				// Create new instance by name
				$class = new self( );
				$class->load( );
			}


			return $class;
		}


		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		load
		**/
		private function load ( )
		{
			// Assign the remote host IP address
			$this->host = $_SERVER['REMOTE_ADDR'];

			// Access the WebCore database
			$db = DB\DB::Get('webcore');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return null;
			}


			// Prepare SQL query to select visitor data
			$select = DB\SQL::Set("SELECT * FROM `user`, `user_hosts` WHERE `user`.`alias` = `user_hosts`.`alias` AND `user_hosts`.`host` = '{$this->host}';");

			// If the select failed to proceed
			if ( !$db->query( $select ) )
			{
				return null;
			}

			// If there is no entry
			if ( !$db->numrows( ) )
			{
				return null;
			}


			// Access Visitor's data
			$visitor = $db->results(0);

			// Iterate through each property
			foreach ( $visitor as $key => $value )
			{
				switch ( $key )
				{
					case 'id':

						$value = (int) $value;

					break;

					case 'ban':

						$value = (bool) $value;

					break;
				}


				// Assign property to Visitor
				$this->$key = $value;
			}


			return null;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

