<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-27 23:44
	* @file:		User library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	USER LIBRARY


	namespace WebCore\User;


	use \WebCore\Core;
	use \WebCore\Core\Exceptions;
	use \WebCore\Data;
	use \WebCore\DB;


	class User
	{
		/**
		 * Version holder
		**/
		const VERSION			= 2;


		/**
		 * Property firewall
		**/
		const FIREWALL			= 'id,alias,password,salt,signup';


		/**
		 * Properties
		**/
		protected $id			= null;
		protected $alias		= null;
		protected $firstname	= null;
		protected $lastname		= null;
		protected $email		= null;
		protected $password		= null;
		protected $salt			= null;
		protected $signup		= null;
		protected $signin		= null;
		protected $access		= null;
		protected $subscribe	= null;
		protected $ban			= null;
		protected $params		= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		final protected function __construct ( ) { }


		/**
		 *
		 * Factory
		 *
		 * @param	string	$alias	User alias
		 * @return	self
		 * @see		Factory
		**/
		public static function &Factory ( $alias = null )
		{
			// Create instances holder
			static $users = array( );

			// Get the name of the called class
			$class = get_called_class( );

			// If there is no alias
			if ( empty( $alias ) )
			{
				// Get the alias from current session
				$alias = Session::Factory( )->user;
			}

			// If there is no instance holder in the list
			if ( !array_key_exists( $alias, $users ) )
			{
				// Create instance holder
				$users[ $alias ] = null;
			}


			// Access the user instance
			$user = &$users[ $alias ];

			// If there is no instance
			if ( is_null( $user ) )
			{
				// Create new instance
				$user = new $class( );
				$user->load( $alias );
			}


			return $user;
		}


		/**
		 *
		 * Factory using condition
		 *
		 * @param	array	$where	Condition parameters
		 * @return	array<self>
		 * @see		FactoryWhere
		**/
		public static function FactoryWhere ( array $where )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}

			// If there is no condition
			if ( empty( $where ) )
			{
				return false;
			}


			// Build SQL to select aliases of the users we want to load
			$select = DB\SQL::Set( "SELECT `alias` FROM `user` WHERE [#query] ORDER BY `alias` ASC;", false );
			$select->data( $where );

			// If the select failed to proceed
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there are no users
			if ( !$db->numrows( ) )
			{
				return array( );
			}


			// Access the raw list of Users
			$users = $db->results( )->raw( );

			// Iterate trough each user
			foreach ( $users as &$user )
			{
				// Create instance of the user object
				$user = self::Factory( $user->alias );
			}


			return $users;
		}


		/**
		 *
		 * Does the user exists?
		 *
		 * @param	string	$alias	User alias
		 * @param	int		$id		[out] User id
		 * @return	bool
		 * @see		Exists
		**/
		public static function Exists ( $alias, &$id = null )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}

			// If the user name is not string
			if ( !is_string( $alias ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $alias, 1 );
			}


			// Prepare SQL query to select User Id
			$select = DB\SQL::Set("SELECT `id` FROM `user` WHERE `alias` = '{{$alias}}';");

			// If the query failed to execute
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// if nothing was found
			if ( !$db->numrows( ) )
			{
				return false;
			}


			// Return the Id of the user
			$id = $db->results(0)->id;
			$id = intval( $id );

			return true;
		}


		/**
		 *
		 * Register new user
		 *
		 * @param	string	$alias		User alias
		 * @param	mixed	$password	Password
		 * @param	string	$email		Email
		 * @param	string	$firstname	First name
		 * @param	string	$lastname	Last name
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		Register
		**/
		public static function Register ( $alias, $password, $email, $firstname = null, $lastname = null, $subscribe = false )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// Generate new salt for the user
			$salt = Core\Crypto::Salt( 8 );

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Create encrypted password usign password, salt and alias
			$hash = Core\Crypto::Password( $password, $salt, $alias );

			// If such user already exists
			if ( self::Exists( $alias ) )
			{
				return false;
			}

            $values = array
			(
				'id'		=> null,
				'alias'		=> $alias,
				'firstname'	=> $firstname,
				'lastname'	=> $lastname,
				'email'		=> $email,
				'password'	=> $hash,
				'salt'		=> $salt,
				'signup'	=> Core\Date::NOW( ),
				'signin'	=> Core\Date::TIMESTAMP_EMPTY,
				'access'	=> UAC::Role(4)->alias,
				'ban'		=> false,
			);

			// If the subscribing is disabled right now
			if (defined('USER_ENABLE_SUBSCRIBERS')) {
				$values['subscribe'] = (bool) $subscribe;
			}

			// Prepare SQL query to insert User data
			$insert = DB\SQL::Set("INSERT INTO `user` ([#keys]) VALUES ([#values]);");
			$insert->data($values);

			// If the query failed to execute
			if ( !$db->query( $insert ) )
			{
				return false;
			}

			// if nothing was affected
			if ( !$db->affected( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Get list of subscribers
		 *
		 * @param	void
		 * @return	array<self>
		 * @see		getSubscribers
		**/
		public static function getSubscribers ( )
		{
			// If the subscribing is disabled right now
			if ( !defined('USER_ENABLE_SUBSCRIBERS') )
			{
				return [ ];
			}


			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Build SQL to select aliases of the users we want to load
			$select = DB\SQL::Set("SELECT `alias` FROM `user` WHERE `subscribe` = 1 ORDER BY `alias` ASC;");

			// If the select failed to proceed
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there are no users
			if ( !$db->numrows( ) )
			{
				return array( );
			}


			// Access the raw list of Users
			$users = $db->results( )->raw( );

			// Iterate trough each user
			foreach ( $users as &$user )
			{
				// Create instance of the user object
				$user = self::Factory( $user->alias );
			}


			return $users;
		}



		/**
		 *
		 * <<magic>> Getter
		 *
		 * @param	string	$key	User property
		 * @return	mixed
		 * @see		__get
		**/
		final public function __get ( $key )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such property of the User
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}


			return $this->$key;
		}


		/**
		 *
		 * <<magic>> Setter
		 *
		 * @param	string	$key	User property
		 * @param	mixed	$value	New value
		 * @return	void
		 * @see		__set
		**/
		final public function __set ( $key, $value )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such property of the User
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}


			// Get the list of firewalled properties
			$keys = explode( ',', $this::FIREWALL );

			// If the property is blocked
			if ( in_array( $key, $keys ) )
			{
				return null;
			}


			// Assign new value through method
			$this->assign( $key, $value );
		}


#region Params


		/**
		 *
		 * Get wanted parameter
		 *
		 * @param	string	$key	Param key
		 * @param	string	$ns		Param namespace
		 * @return	mixed
		 * @see		get
		**/
		public function get ( $key, $ns = null )
		{
			// If property name is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If namespace name is not a string
			if ( !is_null( $ns ) AND !is_string( $ns ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $ns, 2 );
			}

			// Do we have it?
			if ( !$this->has( $key, $ns ) )
			{
				return null;
			}

			// Check value existence
			switch ( is_null( $ns ) )
			{
				// DO not use namespace
				case true:

					return $this->params[ $key ];

				break;

				// Use namespace
				case false:

					return $this->params[ $ns ][ $key ];

				break;
			}


			return null;
		}


		/**
		 *
		 * Set new parameter
		 *
		 * @param	string	$key		Param key
		 * @param	mixed	$value		Param value
		 * @param	string	$ns			Param namespace
		 * @param	bool	$update		Update the user
		 * @return	bool
		 * @see		set
		**/
		public function set ( $key, $value, $ns = null, $update = false )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If namespace name is not a string
			if ( !is_null( $ns ) AND !is_string( $ns ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $ns, 2 );
			}

			// If the value already exists and is same
			if ( $this->get( $key, $ns ) == $value )
			{
				return true;
			}

			// Check value existence
			switch ( is_null( $ns ) )
			{
				// DO not use namespace
				case true:

					// Assign parametr value to holder
					$this->params[ $key ] = $value;

				break;

				// Use namespace
				case false:

					// Assign parametr value to holder
					$this->params[ $ns ][ $key ] = $value;

				break;
			}

			// Shall we update the user?
			if ( $update == true )
			{
				// Return the state of action
				return $this->update( );
			}


			return true;
		}


		/**
		 *
		 * Do we have wanted parameter?
		 *
		 * @param	string	$key	Param key
		 * @param	string	$ns		Param namespace
		 * @return	bool
		 * @see		has
		**/
		public function has ( $key, $ns = null )
		{
			// If property name is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If namespace name is not a string
			if ( !is_null( $ns ) AND !is_string( $ns ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $ns, 2 );
			}

			// Check value existence
			switch ( is_null( $ns ) )
			{
				// DO not use namespace
				case true:

					// Check wether we have this param in holder or not
					return array_key_exists( $key, $this->params );

				break;

				// Use namespace
				case false:

					// If there is no such namespace defined
					if ( !array_key_exists( $ns, $this->params ) )
					{
						return false;
					}


					// Check wether we have this param in holder or not
					return array_key_exists( $key, $this->params[ $ns ] );

				break;
			}


			return false;
		}


#endregion


		/**
		 *
		 * Create copy of the object
		 *
		 * @param	void
		 * @return	array
		 * @see		copy
		**/
		final public function copy ( )
		{
			// Empty holder
			$copy = array( );

			// Iterate trough each property
			foreach ( $this as $key => $value )
			{
				$copy[ $key ] = $value;
			}


			return $copy;
		}


		/**
		 *
		 * Is the user signed in?
		 *
		 * @param	void
		 * @return	bool
		 * @see		signed
		**/
		public function signed ( $strict = true )
		{
			// Switch the strict mode
			switch ( (bool) $strict )
			{
				// Enabled
				case true:

					// Check if there is User or higher access
					return UAC::Authorize( $this, 'user' );

				break;

				// Disabled
				case false:

					// Nope, guest is not signed in user
					return $this->alias === 'guest';

				break;
			}


			// Just in case ...
			return false;
		}


		/**
		 *
		 * Get user name
		 *
		 * @param	void
		 * @return	string
		 * @see		getFullName
		**/
		public function getFullName ( ) { switch ( true )
		{
			// If there is no firstname or lastname of this user
			case empty( $this->firstname ) AND empty( $this->lastname ):

				// Return its username
				return $this->alias;

			break;

			default:

				// Return the full name of the user
				return "{$this->firstname} {$this->lastname}";

			break;
		}}


		/**
		 *
		 * Check if user has specified access category
		 *
		 * @param	string	$access		Access level
		 * @return	bool
		 * @see		is
		**/
		public function is ( $access )
		{
			// Check if there is User or higher access
			return UAC::Authorize( $this, $access );
		}


		/**
		 *
		 * Alias of User::is( )
		 *
		 * @param	string	$access		Access level
		 * @return	bool
		 * @see		can
		**/
		public function can ( $access )
		{
			return $this->is( $access );
		}


		/**
		 *
		 * Get available roles
		 *
		 * @param	void
		 * @return	array
		 * @see		roles
		**/
		public function roles ( )
		{
			return UAC::Roles( );
		}


		/**
		 *
		 * Is user online?
		 *
		 * @param	void
		 * @return	bool
		 * @see		online
		**/
		public function online ( )
		{
			// Runtime cache holder
			static $cache = [ ];
			static $loaded = false;

			// If there is cached status in the holder
			if ( array_key_exists( $this->alias, $cache ) )
			{
				return $cache[ $this->alias ];
			}

			// If the user is banned
			if ( $this->ban )
			{
				return $cache[ $this->alias ] = false;
			}

			// If the cache is not empty
			if ( $loaded === true )
			{
				return $cache[ $this->alias ] = false;
			}


			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return $cache[ $this->alias ] = false;
			}


			// Create expiration date object
			$expire = new \WebCore\Core\Date( Session::LIFETIME );


			$query = DB\SQL::Set("SELECT `user` FROM `user_session` WHERE `active` > '{$expire}';");

			// If the query failed to execute
			if ( !$db->query( $query ) )
			{
				return $cache[ $this->alias ] = false;
			}


			// We have used database
			$loaded = true;

			// Copy list of all those online users
			foreach ( $db->results( ) as $state )
			{
				$cache[ $state->user ] = true;
			}

			// If there are no entries
			if ( !array_key_exists( $this->alias, $cache ) )
			{
				return $cache[ $this->alias ] = false;
			}


			return $cache[ $this->alias ];
		}


		/**
		 *
		 * Change the user password.
		 *
		 * @param	string	$password	New user password
		 * @return	bool
		 * @see		signed
		**/
		public function password ( $password )
		{
			// Generate new salt for the user
			$salt = Core\Crypto::Salt( 8 );

			// If the password is not a string
			if ( !is_string( $password ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $password, 1 );
			}


			// Create password hash from given password and username
			$hash = Core\Crypto::Password( $password, $salt, $this->alias );

			// Assign new password and salt
			$this->password = $hash;
			$this->salt = $salt;

			// If we were unable to save changes
			if ( !$this->update( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Set ban state
		 *
		 * @param	bool	$state	Ban state
		 * @return	bool
		 * @see		ban
		**/
		public function ban ( $state )
		{
			// Assign new password and salt
			$this->ban = (int) (bool) $state;

			// If we were unable to save changes
			if ( !$this->update( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Save changes done to the user
		 *
		 * @param	void
		 * @return	bool
		 * @see		update
		**/
		public function update ( )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Access User values
			$user = $this->copy( );

			// Remove values we do not want to update
			unset( $user['id'] );
			unset( $user['alias'] );
			unset( $user['signup'] );

			// If the subscribing is disabled right now
			if ( !defined('USER_ENABLE_SUBSCRIBERS') )
			{
				unset( $user['subscribe'] );
			}

			// If there are data in parameters
			if ( !empty( $user['params'] ) )
			{
				// Write those data into INI compilant string
				$user['params'] = Data\INI::Write( $this->params, true );
			}

			// If there are no data in parameters
			if ( empty( $user['params'] ) )
			{
				$user['params'] = null;
			}


			// Prepare SQL query save the changes to the user
			$update = DB\SQL::Set( "UPDATE `user` SET [#query] WHERE `alias` = '{$this->alias}';", false );
			$update->data( $user );

			// If the query failed to execute
			if ( !$db->query( $update ) )
			{
				return false;
			}

			// if nothing was affected
			if ( !$db->affected( ) )
			{
				return false;
			}


			return true;
		}



		/**
		 *
		 * Load user data
		 *
		 * @param	string	$alias		User alias
		 * @param	bool	$return		Return instead of assigning
		 * @return	void
		 * @see		load
		**/
		protected function load ( $alias, $return = false )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}

			// If there is no alias
			if ( empty( $alias ) )
			{
				return false;
			}

			// If such user does not exists
			if ( !self::Exists( $alias, $id ) )
			{
				return false;
			}


			// Prepare SQL query to select visitor data
			$select = DB\SQL::Set("SELECT * FROM `user` WHERE `id` = {$id};");

			// If the select failed to proceed
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there is no entry
			if ( !$db->numrows( ) )
			{
				return false;
			}


			// Access User data
			$user = $db->results(0);

			// If we should return the data
			if ( $return == true )
			{
				return $user;
			}

			// iterate trough each property in user
			foreach ( $user as $key => $value )
			{
				// Assign value to object
				$this->assign( $key, $value );
			}


			return true;
		}


		/**
		 *
		 * Clear the session
		 *
		 * @param	void
		 * @return	bool
		 * @see		clear
		**/
		protected function clear ( )
		{
			// Iterate trough each property
			foreach ( $this as &$value )
			{
				$value = null;
			}


			return true;
		}


		/**
		 *
		 * New value assigment
		 *
		 * @param	string	$key	User property
		 * @param	mixed	$value	New value
		 * @return	bool
		 * @see		assign
		**/
		protected function assign ( $key, $value )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such property of the User
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}

			// Switch the property name
			switch ( strToLower( $key ) )
			{
				case 'id':

					// Convert to integer
					$value = (int) $value;

				break;

				case 'ban':
				case 'subscribe':

					// Convert to boolean
					$value = (bool) $value;

				break;

				case 'params':

					// If there is no value
					if ( empty( $value) )
					{
						// Set empty array instead
						$value = array( ); break;
					}


					// Read INI string from the params field
					$value = Data\INI::Read( $value, true );

				break;
			}


			// Assign new property value
			$this->$key = $value;

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

