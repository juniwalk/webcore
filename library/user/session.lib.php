<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-28 13:54
	* @file:		Session library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	SESSION LIBRARY


	namespace WebCore\User;


	use \WebCore\Core;
	use \WebCore\Core\Exceptions;
	use \WebCore\DB;
	use \WebCore\Debug;
	use \WebCore\User;
	use \WebCore\Web;


	class Session
	{
		/**
		 * Version holder
		**/
		const VERSION			= 2;


		/**
		 * Session duration
		**/
		const LIFETIME			= '-1 hour';


		/**
		 * Property firewall
		**/
		const FIREWALL			= 'id,guid';


		/**
		 * Properties
		**/
		protected $id			= null;
		protected $user			= null;
		protected $guid			= null;
		protected $host			= null;
		protected $site			= null;
		protected $platform		= null;
		protected $active		= null;
		protected $remember		= null;
		protected $useragent	= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		final protected function __construct ( )
		{
			// Assign Browser GUID to the Session
			$this->guid = User\Browser::GUID( );
		}



		/**
		 *
		 * Factory
		 *
		 * @param	void
		 * @return	self
		 * @see		Factory
		**/
		public static function &Factory ( )
		{
			// Instance holder
			static $class = null;

			// If there is no instance
			if ( is_null( $class ) )
			{
				// Create new instance
				$class = new self( );
				$class->create( );
			}


			return $class;
		}


		/**
		 *
		 * Kill all expired sessions
		 *
		 * @param	void
		 * @return	bool
		 * @see		KillExpired
		**/
		public static function KillExpired ( )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// Create expiration date object
			$expire = new Core\Date( self::LIFETIME );

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Prepare SQL query to select all expired sessions
			$select = DB\SQL::Set("SELECT `user`, `guid` FROM `user_session` WHERE `active` <= '{$expire}' AND `remember` = 0;");

			// If the select failed to proceed
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there is no entry
			if ( !$db->numrows( ) )
			{
				return true;
			}


			// Access list of expired guids
			$sessions = $db->results( );
			$sessions->assoc('guid');

			// Iterate trough each session
			foreach ( $sessions as $guid => $session )
			{
				// Create new instance for session kill and
				// assign all basic information about task
				$log = Debug\Log::Factory("kill_{$guid}");
				$log->entry = $session->user;
				$log->task = __METHOD__;
				$log->user = 'webcore';

				// Kill the session and catch status
				$log->status = self::Kill( $guid );

				// Try to write the log
				if ( !$log->write( ) )
				{
					continue;
				}
			}


			return true;
		}


		/**
		 *
		 * Kill session
		 *
		 * @param	string	$guid	Browser GUID
		 * @return	bool
		 * @see		Kill
		**/
		public static function Kill ( $guid )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}

			// If the GUID is not string
			if ( !is_string( $guid ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $guid, 1 );
			}


			// Prepare SQL query to kill specified user session
			$delete = DB\SQL::Set("DELETE FROM `user_session` WHERE `guid` = '{{$guid}}';");

			// If the query failed to execute
			if ( !$db->query( $delete ) )
			{
				return false;
			}

			// if nothing was affected
			if ( !$db->affected( ) )
			{
				return false;
			}


			return true;
		}



		/**
		 *
		 * <<magic>> Getter
		 *
		 * @param	string	$key	User property
		 * @return	mixed
		 * @see		__get
		**/
		final public function __get ( $key )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such property of the User
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}


			return $this->$key;
		}


		/**
		 *
		 * <<magic>> Setter
		 *
		 * @param	string	$key	User property
		 * @param	mixed	$value	New value
		 * @return	void
		 * @see		__set
		**/
		final public function __set ( $key, $value )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such property of the User
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}


			// Get the list of firewalled properties
			$keys = explode( ',', $this::FIREWALL );

			// If the property is blocked
			if ( in_array( $key, $keys ) )
			{
				return null;
			}


			// Assign new value through method
			$this->assign( $key, $value );
		}


		/**
		 *
		 * Create copy of the object
		 *
		 * @param	void
		 * @return	array
		 * @see		copy
		**/
		final public function copy ( )
		{
			// Empty holder
			$copy = array( );

			// Iterate trough each property
			foreach ( $this as $key => $value )
			{
				$copy[ $key ] = $value;
			}


			return $copy;
		}



		/**
		 *
		 * Touch the session
		 *
		 * @param	void
		 * @return	bool
		 * @see		touch
		**/
		public function touch ( )
		{
			// Update internal data
			$this->host = Web\Request::Host( );
			$this->site = Web\Site::Factory( )->alias;
			$this->active = (string) Core\Date::NOW( );

			// If touching the session has failed
			if ( !$this->update( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Terminates session
		 *
		 * @param	void
		 * @return	bool
		 * @see		terminate
		**/
		public function terminate ( )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Prepare SQL query to retminate current user session
			$delete = DB\SQL::Set("DELETE FROM `user_session` WHERE `guid` = '{{$this->guid}}';");

			// If the query failed to execute
			if ( !$db->query( $delete ) )
			{
				return false;
			}

			// if nothing was affected
			if ( !$db->affected( ) )
			{
				return false;
			}


			// Clear the data
			$this->clear( );

			return true;
		}


		/**
		 *
		 * Save changes done to the user
		 *
		 * @param	void
		 * @return	bool
		 * @see		update
		**/
		public function update ( )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// Get the list of firewalled properties
			$firewall = explode( ',', $this::FIREWALL );

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Create copy of the Session
			$session = $this->copy( );

			// Iterate through each key in firewall
			foreach ( $firewall as $key )
			{
				unset( $session[ $key ] );
			}


			// Prepare SQL query to save the changes to the session
			$update = DB\SQL::Set("UPDATE `user_session` SET [#query] WHERE `guid` = '{{$this->guid}}';");
			$update->data( $session );

			// If the query failed to execute
			if ( !$db->query( $update ) )
			{
				return false;
			}

			// if nothing was affected
			if ( !$db->affected( ) )
			{
				return false;
			}


			return true;
		}



		/**
		 *
		 * Create or update session
		 *
		 * @param	void
		 * @return	void
		 * @see		create
		**/
		protected function create ( )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}

			// Try to refresh the session
			if ( $this->refresh( ) )
			{
				return true;
			}


			// Prepare SQL query to insert User data
			$insert = DB\SQL::Set("INSERT INTO `user_session` ([#keys]) VALUES ([#values]);");

			// Prepare data of new user account
			$insert->data( array
			(
				'id'		=> null,
				'user'		=> UAC::Role(5)->alias,
				'guid'		=> $this->guid,
				'host'		=> Web\Request::Host( ),
				'site'		=> Web\Site::Factory( )->alias,
				'platform'	=> User\Browser::Platform( ),
				'active'	=> Core\Date::NOW( ),
				'remember'	=> 0,
				'useragent'	=> User\Browser::Agent( ),
			));

			// If the query failed to execute
			if ( !$db->query( $insert ) )
			{
				return false;
			}

			// if nothing was affected
			if ( !$db->affected( ) )
			{
				return false;
			}

			// Try to refresh the session
			if ( !$this->refresh( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Refresh session3
		 *
		 * @param	void
		 * @return	void
		 * @see		refresh
		**/
		protected function refresh ( )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Prepare SQL query to select session data
			$select = DB\SQL::Set("SELECT * FROM `user_session` WHERE `guid` = '{{$this->guid}}';");

			// If the select failed to proceed
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there is no entry
			if ( !$db->numrows( ) )
			{
				return false;
			}


			// Access Session data
			$session = $db->results(0);

			// iterate trough each property in session
			foreach ( $session as $key => $value )
			{
				// Assign value to object
				$this->assign( $key, $value );
			}


			return true;
		}


		/**
		 *
		 * Clear the session
		 *
		 * @param	void
		 * @return	bool
		 * @see		clear
		**/
		protected function clear ( )
		{
			// Iterate trough each property
			foreach ( $this as &$value )
			{
				$value = null;
			}


			return true;
		}


		/**
		 *
		 * New value assigment
		 *
		 * @param	string	$key	User property
		 * @param	mixed	$value	New value
		 * @return	bool
		 * @see		assign
		**/
		protected function assign ( $key, $value )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such property of the User
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}

			switch ( $key )
			{
				case 'id':

					$value = (int) $value;

				break;

				case 'remember':

					$value = (bool) $value;

				break;
			}


			// Assign new property value
			$this->$key = $value;

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

