<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-24 00:30
	* @file:		Browser library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	BROWSER LIBRARY


	namespace WebCore\User;


	use \WebCore\Core;
	use \WebCore\Web;


	final class Browser
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( ) { }


		/**
		 *
		 * Get the cookie
		 *
		 * @param	string	$key	Name of the cookie
		 * @return	mixed
		 * @throws	InvalidArgument
		 * @see		Cookie
		**/
		public static function Cookie ( $key )
		{
			// If the key is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such cookie set right now
			if ( !array_key_exists( $key, $_COOKIE ) )
			{
				return null;
			}


			// Return the value of the cookie
			return $_COOKIE[ $key ];
		}


		/**
		 *
		 * Create Microsoft-Compatible GUID
		 *
		 * @param	void
		 * @return	string
		 * @see		GUID
		**/
		public static function GUID ( )
		{
			// Access browser guid from the cookie
			// and generate random salt of 32 chars
			$guid = Browser::Cookie('guid');
			$salt = Core\Crypto::Salt(32);

			// If there already is guid
			if ( is_string( $guid ) )
			{
				return $guid;
			}


			// Create GUID from randomly generated salt
			$guid = Core\Crypto::GUID( $salt );

			// If we were unable to set new cookie with the GUID
			if ( !Web\Request::Cookie( 'guid', $guid, time( ) ) )
			{
				throw new \Exception("Unable to set new cookie with browser guid");
			}


			return $guid;
		}


		/**
		 *
		 * Get the user agent value
		 *
		 * @param	void
		 * @return	string
		 * @see		Agent
		**/
		public static function Agent ( )
		{
			// Return the user agent value
			return $_SERVER['HTTP_USER_AGENT'];
		}


		/**
		 *
		 * Get list of supported languages
		 *
		 * @param	void
		 * @return	array
		 * @see		Languages
		**/
		public static function Languages ( )
		{
			// Language cache holder
			static $langs = array( );

			// If there is a list of languages
			if ( !empty( $langs ) )
			{
				return $langs;
			}


			// Access the string of supported languages
			$langs = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
			$langs = explode( ',', $langs );

			// Iterate through each language
			foreach ( $langs as $k => $lang )
			{
				// Look for the semicolon
				$colon = strpos( $lang, ';' );

				// If there is no semicolon
				if ( $colon === false )
				{
					continue;
				}


				// Remove part after the semicolon from language
				$langs[$k] = substr( $lang, 0, $colon );
			}


			return $langs;
		}


		/**
		 *
		 * Check encoding support
		 *
		 * @param	string	$encoding	Type of encoding
		 * @return	bool
		 * @see		Supports
		**/
		public static function Supports ( $encoding )
		{
			// Get header from browser of supported encoding
			$supported = $_SERVER['HTTP_ACCEPT_ENCODING'];

			// Check if gZIP is supported by the browser
			if ( strpos( $supported, $encoding ) === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Browser platform
		 *
		 * @param	void
		 * @return	string
		 * @see		Platform
		**/
		public static function Platform ( )
		{
			return 'windows';
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

