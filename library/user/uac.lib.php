<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-23 22:54
	* @file:		UAC library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	UAC LIBRARY


	namespace WebCore\User;


	use \WebCore\DB;


	final class UAC
	{
		/**
		 * Version holder
		**/
		const VERSION	= 1;


		/**
		 * Role IDs
		**/
		const ROOT		= 1;
		const ADMIN		= 2;
		const MANAGER	= 3;
		const USER		= 4;
		const GUEST		= 5;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( ) { }


		/**
		 *
		 * Authorize user access by role
		 *
		 * @param	object	$user	User
		 * @param	string	$role	Role type
		 * @return	bool
		 * @see		Authorize
		**/
		public static function Authorize ( &$user, $role )
		{
			// Access User Access Roles
			$roles = self::Roles( );

			// If there is no access property to check on
			if ( !property_exists( $user, 'access' ) )
			{
				return false;
			}

			// If there is no such role in the User Access
			if ( !$roles->offsetExists( $role ) )
			{
				return false;
			}

			// If the user does not have enough access for given role
			if ( $roles[ $user->access ]->id > $roles[ $role ]->id )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Get role object
		 *
		 * @param	mixed	$key	Id of the role
		 * @param	string	$source	Source of the role
		 * @return	object
		 * @see		Role
		**/
		public static function Role ( $key, $source = 'id' )
		{
			// Define list of allowed sources
			$sources = array( 'id', 'alias' );

			// If the source is not allowed
			if ( !in_array( $source, $sources ) )
			{
				return false;
			}


			// Access list of roles
			$roles = self::Roles( );
			$roles->assoc( $source );

			// If there is no such role defined
			if ( !$roles->offsetExists( $key ) )
			{
				return false;
			}


			// Return the clone of the role
			return clone $roles[ $key ];
		}


		/**
		 *
		 * Get user access roles
		 *
		 * @param	void
		 * @return	array
		 * @see		Roles
		**/
		public static function Roles ( )
		{
			// Static roles holder
			static $roles = array( );

			// If there are roles loaded
			if ( !empty( $roles ) )
			{
				return clone $roles;
			}


			// Access the WebCore database
			$db = DB\DB::Get('webcore');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Build SQL query to select list of available User Access Roles from database
			$select = DB\SQL::Set("SELECT * FROM `user_roles` ORDER BY `id` ASC;");

			// If we were unable to select config
			if ( !$db->query( $select ) )
			{
				return false;
			}

			// If there are no entries
			if ( !$db->numrows( ) )
			{
				return false;
			}


			// Save the results to the holder
			$roles = $db->results( );
			$roles->assoc('alias');

			return clone $roles;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

