<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		Event library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	EVENT LIBRARY


	namespace WebCore\DB;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB\Result;


	class Event implements \ArrayAccess, \IteratorAggregate, \Countable
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Query
		**/
		protected $error		= null;
		protected $query		= null;
		protected $time			= null;


		/**
		 * Information
		**/
		protected $state		= null;
		protected $numrows		= null;
		protected $affected		= null;


		/**
		 * Results
		**/
		protected $result		= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	string	$sql	Database query
		 * @return	void
		 * @throws	InvalidArgument
		 * @see		__construct
		**/
		public function __construct ( $sql )
		{
			// Save the event begin microtime
			$this->time -= microtime( true );

			// If the SQL is not a string
			if ( !is_string( $sql ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $sql, 1 );
			}


			// Assign SQL to the holder
			$this->query = $sql;
		}


		/**
		 *
		 * Assign SQL error
		 *
		 * @param	string	$message	Error message
		 * @return	void
		 * @see		error
		**/
		public function error ( $message )
		{
			// If the message is not scalar
			if ( !is_scalar( $message ) )
			{
				return null;
			}


			// Assign new SQL error
			$this->error = $message;
		}


		/**
		 *
		 * Assign SQL result
		 *
		 * @param	\WebCore\DB\Result\Result	$rows	Result rows
		 * @return	void
		 * @see		result
		**/
		public function result ( Result\Result &$rows )
		{
			// Assign list of the rows to the event
			$this->result = &$rows;
		}


		/**
		 *
		 * Finish event
		 *
		 * @param	bool	$state		Query state
		 * @param	int		$numrows	Rows selected
		 * @param	int		$affected	Rows affected
		 * @return	void
		 * @see		done
		**/
		public function done ( $state, $numrows, $affected )
		{
			// Assign number of selected and affected rows
			$this->numrows = $numrows;
			$this->affected = $affected;
			$this->state = $state;

			// Save the event end microtime
			$this->time += microtime( true );
		}



		/**
		 *
		 * <<interface>> Get the count of the fields
		 *
		 * @param	void
		 * @return	int
		 * @see		count
		**/
		public function count ( )
		{
			return sizeof( (array) $this );
		}


		/**
		 *
		 * <<interface>> Access array iterator
		 *
		 * @param	void
		 * @return	ArrayIterator
		 * @see		getIterator
		**/
		public function getIterator ( )
		{
			return new \ArrayIterator( $this );
		}


		/**
		 *
		 * <<interface>> Set property value
		 *
		 * @param	string	$key	Property name
		 * @param	mixed	$value	Property value
		 * @return	void
		 * @see		offsetSet
		**/
		public function offsetSet ( $key, $value )
		{
			$this->$key = $value;
		}


		/**
		 *
		 * <<interface>> Get property value
		 *
		 * @param	string	$key	Property name
		 * @return	mixed
		 * @see		offsetGet
		**/
		public function offsetGet ( $key )
		{
			return $this->$key;
		}


		/**
		 *
		 * <<interface>> Does property exists?
		 *
		 * @param	string	$key	Property name
		 * @return	bool
		 * @see		offsetExists
		**/
		public function offsetExists ( $key )
		{
			return isset( $this->$key );
		}


		/**
		 *
		 * <<interface>> Unset property
		 *
		 * @param	string	$key	Property name
		 * @return	void
		 * @see		offsetUnset
		**/
		public function offsetUnset ( $key )
		{
			unset( $this->$key );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

