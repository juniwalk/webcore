<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-29 15:11
	* @file:		SQL library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	SQL LIBRARY


	namespace WebCore\DB;


	use \WebCore\Core\Exceptions;
	use \WebCore\IO;
	use \WebCore\Security;


	final class SQL
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1.5;


		/**
		 * Search patterns
		**/
		const COLUMNS		= "/([[](\w+)[]])/i";	// [column]
		const VALUES		= "/('{(.*?)}')/i";		// '{value}'
		const SHEETS		= "/([[](#\w+)[]])/i";	// [#sheet]


		/**
		 * SQL string
		**/
		protected $query	= null;
		protected $escape	= null;


		/**
		 * Replacement
		**/
		protected $quotes	= null;
		protected $values	= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		protected function __construct ( ) { }


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			// Compile SQL to the MySQL type using empty Authentication object
			return $this->compile( new Security\Auth("mysql://u:p@s/d") );
		}


		/**
		 *
		 * Quote data for the query
		 *
		 * @param	mixed	$value	Value to be queoted
		 * @param	bool	$column	Is it column name?
		 * @return	bool
		 * @see		Quote
		**/
		public static function Quote ( &$value, $column = false )
		{
			// If there is no value for column
			if ( empty( $value ) AND $column )
			{
				return true;
			}

			// Sign for *everything*
			if ( $value === "*" )
			{
				return true;
			}

			// If the value is resource
			if ( is_resource( $value ) )
			{
				return false;
			}

			// If the value is an array
			if ( is_array( $value ) )
			{
				return false;
			}

			// If the value is Query and we don't want column name
			if ( is_a( $value, __CLASS__ ) )
			{
				// Insert brackets around the query
				$value = "( {$value} )"; return true;
			}

			// If the value is an instance of the date class
			if ( is_a( $value, "\WebCore\Core\Date" ) )
			{
				// Convert it to the string (timestamp)
				$value = "'{$value}'"; return true;
			}

			// If the value is an object
			if ( is_object( $value ) )
			{
				return false;
			}


			// Create lower copy of value
			$lower = mb_strToLower( $value );

			// If the value is null from HTML or even null from PHP
			if ( $lower == "null" OR is_null( $value ) )
			{
				// Assign MySQL NULL value
				$value = "NULL"; return true;
			}

			// If the value is empty
			if ( $value === "" )
			{
				// Quote empty string and return true
				$value = "''"; return true;
			}


			// Escape the value using MySQL
			$value = DB::Escape( $value );

			// If the value is column
			if ( $column === true )
			{
				return true;
			}

			// If the value is boolean
			if ( is_bool( $value ) )
			{
				// Switch boolean value into integer
				$value = (int) $value; return true;
			}

			// If the value is numeric and there is dot presented
			if ( is_numeric( $value ) AND strpos( $lower, "." ) )
			{
				// Typecast the value to the double
				$value = (float) $value; return true;
			}

			// If the value is numeric and there is E presented
			if ( is_numeric( $value ) AND strpos( $lower, "e" ) )
			{
				// Typecast the value to the double
				$value = "'{$value}'"; return true;
				$value = (float) $value; return true;
			}

			// If the value is numeric
			if ( is_numeric( $value ) )
			{
				// Typecast value to the integer
				$value = (int) $value; return true;
			}


			// Standard string escape
			$value = "'{$value}'";

			return true;
		}


		/**
		 *
		 * Prepare query to be escaped
		 *
		 * @param	string	$query	SQL to quote
		 * @return	self
		 * @throws	InvalidArgument
		 * @see		Set
		**/
		public static function Set ( $query, $escape = true )
		{
			// Check the query type
			if ( !is_string( $query ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $query, 1 );
			}

			// If the query is empty
			if ( empty( $query ) )
			{
				return false;
			}


			// Create new instance
			$self = new self( );
			$self->query = $query;
			$self->escape = $escape;

			return $self;
		}


		/**
		 *
		 * Prepare query to be escaped from File
		 *
		 * @param	string	$path	SQL File
		 * @return	self
		 * @throws	InvalidArgument
		 * @see		File
		**/
		public static function File ( $path, $escape = true )
		{
			// Check the query type
			if ( !is_string( $path ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $path, 1 );
			}


			// Open the SQL file
			$file = IO\File::Open( $path );

			// If we could not open file
			if ( $file === false )
			{
				return false;
			}

			// If the file type is not SQL
			if ( $file->type != 'sql' )
			{
				return false;
			}

			// If the contents are not plain text
			if ( $file->mimetype != "text/plain" )
			{
				return false;
			}


			// Call the Set method with file contents
			return self::Set( $file->contents( ), $escape );
		}


		/**
		 *
		 * Assign replacement data
		 *
		 * @param	array	$values	Replacement values
		 * @return	self
		 * @see		data
		**/
		public function &data ( array $values = null )
		{
			// If there are no values
			if ( empty( $values ) )
			{
				$values = null;
			}


			// Assign new values
			$this->values = $values;

			return $this;
		}


		/**
		 *
		 * Compile SQL into string
		 *
		 * @param	Auth	$auth	Database login
		 * @return	string
		 * @throws	DatabaseError
		 * @see		compile
		**/
		public function compile ( Security\Auth &$auth )
		{
			// If the query is empty
			if ( empty( $this->query ) )
			{
				return null;
			}

			// If the login is invalid
			if ( !$auth->validate( ) )
			{
				throw new Exceptions\DatabaseError("You have provided an unsupported database driver '{$auth->scheme}'");
			}


			// Access the database specific quotes using driver name
			$this->quotes = constant("\WebCore\DB\Driver\\{$auth->scheme}::QUOTES");

			// If escaping values failed
			if ( !$this->values( ) )
			{
				throw new Exceptions\DatabaseError("Could not escape values in SQL");
			}

			// If replacing sheets failed
			if ( !$this->sheets( ) )
			{
				throw new Exceptions\DatabaseError("Could not replace sheets in SQL");
			}

			// If escaping columns failed
			if ( $this->escape AND !$this->columns( ) )
			{
				throw new Exceptions\DatabaseError("Could not escape columns in SQL");
			}


			// Return query string
			return $this->query;
		}



		/**
		 *
		 * Replace columns
		 *
		 * @param	void
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		columns
		**/
		protected function columns ( )
		{
			// Direct access to the query
			$query = &$this->query;


			// Perform regular match on the whole SQL to search for columns to be escaped
			$response = preg_match_all( self::COLUMNS, $query, $values, PREG_SET_ORDER );

			// if the search has failed
			if ( $response === false )
			{
				throw new Exceptions\DatabaseError("Failed to search for columns to escape");
			}

			// If there are no values
			if ( empty( $values ) )
			{
				return true;
			}


			// Prepare replacement
			$replace = array( );

			// Iterate through each value
			foreach ( $values as $__value )
			{
				// Gather the key / value pair
				$key = $__value[1];
				$value = $__value[2];

				// If the quoting would failed to proceed
				if ( !self::Quote( $value, true ) )
				{
					throw new Exceptions\DatabaseError("<strong>{$value}</strong> is not valid column name");
				}


				// Save the replacement quoted in database quotes
				$replace[ $key ] = sprintf( $this->quotes, $value );
			}

			// If there are no values
			if ( empty( $replace ) )
			{
				return false;
			}


			// Replace escaped columns in the SQL string
			$query = str_replace( array_keys( $replace ), array_values( $replace ), $query );

			return true;
		}


		/**
		 *
		 * Replace values
		 *
		 * @param	void
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		values
		**/
		protected function values ( )
		{
			// Direct access to the query
			$query = &$this->query;


			// Perform regular match on the whole SQL to search for values to be escaped
			$response = preg_match_all( self::VALUES, $query, $values, PREG_SET_ORDER );

			// if the search has failed
			if ( $response === false )
			{
				throw new Exceptions\DatabaseError("Failed to search for values to escape");
			}

			// If there are no values
			if ( empty( $values ) )
			{
				return true;
			}


			// Prepare replacement
			$replace = array( );

			// Iterate through each value
			foreach ( $values as $__value )
			{
				// Gather the key / value pair
				$key = $__value[1];
				$value = $__value[2];

				// If the quoting would failed to proceed
				if ( !self::Quote( $value, false ) )
				{
					throw new Exceptions\DatabaseError("<strong>{$value}</strong> is not valid column name");
				}


				// Save the replacement
				$replace[ $key ] = $value;
			}

			// If there are no values
			if ( empty( $replace ) )
			{
				return false;
			}


			// Replace escaped columns in the SQL string
			$query = str_replace( array_keys( $replace ), array_values( $replace ), $query );

			return true;
		}


		/**
		 *
		 * Replace sheets
		 *
		 * @param	void
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		sheets
		**/
		protected function sheets ( )
		{
			// If there are no sheet values
			if ( empty( $this->values ) )
			{
				return true;
			}


			// Direct access to the query
			$query = &$this->query;


			// Perform regular match on the whole SQL to search for sheets to be replaced
			$response = preg_match_all( self::SHEETS, $query, $values, PREG_SET_ORDER );

			// if the search has failed
			if ( $response === false )
			{
				throw new Exceptions\DatabaseError("Failed to search for sheets to replace");
			}

			// If there are no values
			if ( empty( $values ) )
			{
				return true;
			}


			// Prepare replacement
			$replace = array( );

			// Iterate through each value
			foreach ( $values as $__value )
			{
				// Gather the key / value pair
				$key = $__value[1];
				$value = $__value[2];

				// Save the sheet replacement build for specified key
				$replace[ $key ] = $this->build( $value );
			}

			// If there are no values
			if ( empty( $replace ) )
			{
				return false;
			}


			// Replace SQL sheets with build escaped data values
			$query = str_replace( array_keys( $replace ), array_values( $replace ), $query );

			return true;
		}


		/**
		 *
		 * Build replacement for sheet template
		 *
		 * @param	string	$template	Sheet template
		 * @return	string
		 * @throws	DatabaseError
		 * @see		build
		**/
		protected function build ( $template )
		{
			// If the sheet template is no string
			if ( !is_string( $template ) )
			{
				return false;
			}

			// If there are no values to build
			if ( empty( $this->values ) )
			{
				return false;
			}


			// Prepare holder
			$holder = array( );

			// Iterate through each key / value pair in values
			foreach ( $this->values as $key => $value )
			switch ( $template )	// Perform template switch
			{
				case '#list':

					// Escape and quote the value like column
					if ( !self::Quote( $value, true ) )
					{
						throw new Exceptions\DatabaseError("'{$value}' cannot be used as column name");
					}


					// Save the column into holder
					$holder[] = $value;

				break;

				case '#keys':

					// Escape and quote the value like column
					if ( !self::Quote( $key, true ) )
					{
						throw new Exceptions\DatabaseError("'{$key}' cannot be used as column name");
					}


					// Save the column into holder
					$holder[] = sprintf( $this->quotes, $key );

				break;

				case '#values':

					// Escape and quote the value like data
					if ( !self::Quote( $value, false ) )
					{
						throw new Exceptions\DatabaseError("Unable to escape '". gettype( $value ) ."' for use in database");
					}


					// Save the value into holder
					$holder[] = $value;

				break;

				case '#query':

					// Escape and quote the value like column
					if ( !self::Quote( $key, true ) )
					{
						throw new Exceptions\DatabaseError("'{$key}' cannot be used as column name");
					}

					// Escape and quote the value like data
					if ( !self::Quote( $value, false ) )
					{
						throw new Exceptions\DatabaseError("Unable to escape '". gettype( $value ) ."' for use in database");
					}


					// Add escaping quotes to the key name
					$key = sprintf( $this->quotes, $key );

					// Save the key/value pair into holder
					$holder[] = "{$key} = {$value}";

				break;

				default: return false;
			}

			// if the holder is empty
			if ( empty( $holder ) )
			{
				return false;
			}


			// Imlode the holder into the string
			return implode( ", ", $holder );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

