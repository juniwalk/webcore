<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-7-25 20:40
	* @file:		DB library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DB LIBRARY


	namespace WebCore\DB;


	use \WebCore\Core\Exceptions;
	use \WebCore\Security;


	final class DB
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1.3;


		/**
		 * Available drivers
		**/
		const MSSQL			= "mssql";
		const MYSQL			= "mysql";
		const MYSQLI		= "mysqli";
		const ODBC			= "odbc";
		const SQLITE		= "sqlite";
		const SQLITE3		= "sqlite3";
		const SQLSRV		= "sqlsrv";


		/**
		 * Registered instances
		**/
		protected static $instance	= array( );
		protected static $default	= null;



		/**
		 *
		 * Relay static call to the default connection
		 *
		 * @param	string	$method	Method name
		 * @param	array	$params	Call params
		 * @return	mixed
		 * @throws	DatabaseError
		 * @see		__callStatic
		**/
		public static function __callStatic ( $method, array $params = null )
		{
			// Access default connector
			$database = &self::$default;

			// If the database holder is not object
			if ( !is_object( $database ) )
			{
				throw new Exceptions\DatabaseError("Call to a method '{$method}' on non-object");
			}


			// Prepare callback
			$callback = array( $database, $method );

			// If there is no such method to call
			if ( !method_exists( $database, $method ) )
			{
				throw new Exceptions\DatabaseError("Call to undeclared method {$database}::{$method}");
			}


			// Return results of the relay call to the connector
			return call_user_func_array( $callback, $params );
		}


		/**
		 *
		 * Escape value for SQL
		 *
		 * @param	mixed	$value	Value to escape
		 * @return	mixed
		 * @throws	DatabaseError
		 * @see		Escape
		**/
		public static function Escape ( $value )
		{
			// Connect to the MySQL database using Escape user
			$link = mysqli_connect('localhost','escape');

			// If the value is boolean
			if ( is_bool( $value ) )
			{
				return $value;
			}

			// If the value is NULL
			if ( is_null( $value ) )
			{
				return $value;
			}

			// If the value is numeric
			if ( is_numeric( $value ) )
			{
				return $value;
			}

			// If the value is not scalar
			if ( !is_scalar( $value ) )
			{
				throw new Exceptions\DatabaseError("Cannot escape value because it's not scalar");
			}


			// Escape values for MySQL database
			$value = $link->real_escape_string( $value );
			$value = str_replace( '\r\n', "\r\n", $value );
			$value = addcslashes( $value, '`' );

			return $value;
		}


		/**
		 *
		 * Create new connector instance
		 *
		 * @param	Auth	$auth	Auth params
		 * @param	mixed	$index	Index name
		 * @return	Connector
		 * @throws	DatabaseError
		 * @see		Factory
		**/
		public static function &Factory ( Security\Auth $auth, $index = 0 )
		{
			// If there already is an instance using this index
			if ( array_key_exists( $index, self::$instance ) )
			{
				// Destroy instance handler from holder
				unset( self::$instance[ $index ] );
			}


			// Create new instance of the connector
			// and try to connect to selected database
			$connector = new Connector( $auth );

			// If the connection could not be established
			if ( !$connector->connected( ) )
			{
				throw new Exceptions\DatabaseError("Could not connect to '{$auth->driver}' database.");
			}


			// Register connector to the instance holder
			self::Register( $connector, $index, true );

			// Return back instance
			return $connector;
		}


		/**
		 *
		 * Create new connector instance [From DSN]
		 *
		 * @param	string	$dsn	DSN Auth params
		 * @param	mixed	$index	Index name
		 * @return	Connector
		 * @throws	DatabaseError
		 * @see		FactoryDSN
		**/
		public static function &FactoryDSN ( $dsn, $index = null )
		{
			// Return new instance of the DB_Connector from DSN string
			return self::Factory( new Auth( $dsn ), $index );
		}


		/**
		 *
		 * Register new connector
		 *
		 * @param	mixed		$index		Index name
		 * @param	Connector	$params		Connector instance
		 * @param	bool		$default	Default connection?
		 * @return	void
		 * @throws	InvalidArgument
		 * @see		Register
		**/
		public static function Register ( Connector &$connector, $index = 0, $default = false )
		{
			// Inxed must be scalar value
			if ( !is_scalar( $index ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'scalar', $index, 2 );
			}


			// Save the instance into the holder
			self::$instance[ $index ] = &$connector;

			// If this is default connection
			if ( $default == true )
			{
				// Select it as default
				self::Select( $index );
			}
		}


		/**
		 *
		 * Get registered connector instance
		 *
		 * @param	mixed	$index	Index name
		 * @return	Connector
		 * @throws	InvalidArgument, DatabaseError
		 * @see		Get
		**/
		public static function &Get ( $index = null )
		{
			// Inxed must be scalar value
			if ( !is_scalar( $index ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'scalar', $index, 1 );
			}

			// If the index is empty
			if ( is_null( $index ) )
			{
				// Get default index name
				return self::$default;
			}

			// If there is no instance on defined index name
			if ( !array_key_exists( $index, self::$instance ) )
			{
				throw new Exceptions\DatabaseError("There is no connection named '{$index}'.");
			}


			// Return link to the index instance
			return self::$instance[ $index ];
		}


		/**
		 *
		 * Select default connection
		 *
		 * @param	mixed	$index	Index name
		 * @return	void
		 * @throws	InvalidArgument, DatabaseError
		 * @see		Select
		**/
		public static function Select ( $index = 0 )
		{
			// Inxed must be scalar value
			if ( !is_scalar( $index ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'scalar', $index, 1 );
			}

			// If there is no such connection in the holder
			if ( !array_key_exists( $index, self::$instance ) )
			{
				throw new Exceptions\DatabaseError("There is no connection named '{$index}'.");
			}


			// Assign new default instance by the index
			self::$default = &self::$instance[ $index ];
		}


		/**
		 *
		 * Kill connector instance
		 *
		 * @param	mixed	$index	Index name
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		Kill
		**/
		public static function Kill ( $index )
		{
			// Inxed must be scalar value
			if ( !is_scalar( $index ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'scalar', $index, 1 );
			}

			// If there is no instance on defined index name
			if ( !array_key_exists( $index, self::$instance ) )
			{
				return false;
			}


			// Clear the index for later use
			unset( self::$instance[ $index ] );

			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

