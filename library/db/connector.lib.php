<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		Connector library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	CONNECTOR LIBRARY


	namespace WebCore\DB;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB\Driver;
	use \WebCore\DB\Result;
	use \WebCore\Security;


	final class Connector
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Connection properties
		**/
		protected $auth	= array( );


		/**
		 * Event history
		**/
		protected $history	= array( );


		/**
		 * Driver instance
		**/
		protected $driver	= null;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	auth	$auth	Connection params
		 * @return	void
		 * @throws	DatabaseError
		 * @see		__construct
		**/
		public function __construct ( Security\Auth $auth )
		{
			// If the auth is not valid
			if ( !$auth->validate( ) )
			{
				throw new Exceptions\DatabaseError("Invalid auth credentials for connector");
			}


			// Prepend driver name with class scope
			$driver = "\WebCore\DB\Driver\\{$auth->scheme}";

			// If there is no such driver
			if ( !class_exists( $driver ) )
			{
				throw new Exceptions\DatabaseError("Driver '{$driver}' could not be found.");
			}


			// Assign driver to the holder
			$this->driver = new $driver( $this, $auth );

			// If the connection has failed
			if ( !$this->connected( ) )
			{
				throw new Exceptions\DatabaseError("Could not connect to '{$driver}' database.");
			}


			// Assign connection parameters
			$this->auth = $auth;

			// If there is any password assigned
			if ( !empty( $auth->pass ) )
			{
				// Hide the password from our eyes by replacing every character with *
				$this->auth->pass = str_pad( '', strlen( $auth->pass ), '*' );
			}


			return null;
		}


		/**
		 *
		 * <<magic>> Destructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__destruct
		**/
		public function __destruct ( )
		{
			// Close connection if
			// there is any opened
			return $this->close( );
		}


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			// Return name of the class
			return get_class( $this );
		}


		/**
		 *
		 * Connection status
		 *
		 * @param	void
		 * @return	bool
		 * @see		connected
		**/
		public function connected ( )
		{
			// If there is no instance of driver
			if ( !is_object( $this->driver ) )
			{
				return false;
			}

			// If the driver is not connected
			if ( !$this->driver->connected( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Select database
		 *
		 * @param	string	$name	Database name
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		database
		**/
		public function database ( $name )
		{
			// If the name is not a string
			if ( !is_string( $name ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $name, 1 );
			}

			// If we are not connected
			if ( !$this->connected( ) )
			{
				return false;
			}

			// If we were unable to select database
			if ( !$this->driver->database( $name ) )
			{
				return false;
			}


			// Store database name in parameters
			$this->auth['database'] = $name;

			return true;
		}


		/**
		 *
		 * Escape value for SQL
		 *
		 * @param	mixed	$value	Value to escape
		 * @return	mixed
		 * @throws	DatabaseError
		 * @see		escape
		**/
		public function escape ( $value )
		{
			return DB\DB::Escape( $value );
		}


		/**
		 *
		 * Prepare statement
		 *
		 * @param	string	$statement	Statement name
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		prepare
		**/
		public function prepare ( $statement )
		{
			// If the prepare is wrong
			if ( !is_string( $statement ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $statement, 1 );
			}


			// Create instance for new SQL event holder
			$this->history[] = new DB_Event( $statement );

			// If we are not connected
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Flush remaining statements and results
			$this->flush( );

			// Relay call to the driver object to prepare statement
			if ( !$this->driver->prepare( $statement ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Set statement parameter
		 *
		 * @param	string	$key	Parameter name
		 * @param	mixed	$value	Parameter value
		 * @param	int		$type	Parameter type
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		param
		**/
		public function param ( $key, $value, $type )
		{
			// If the parameter is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}


			// Try to bind new value for prepared statement call
			$result = $this->driver->param( $key, $value, $type );

			// If binding has failed
			if ( $result === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Execute prepared statement
		 *
		 * @param	bool	$results	Get results response?
		 * @return	mixed
		 * @throws	DatabaseError
		 * @see		execute
		**/
		public function execute ( $results = true )
		{
			// Execute prepared database statement
			$result = $this->driver->execute( $results );

			// Finalize SQL event holder
			$this->event( )->done( );

			return $result;
		}


		/**
		 *
		 * Begin transaction
		 *
		 * @param	void
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		begin
		**/
		public function begin ( )
		{
			// Execute BEGIN SQL query from the driver right now
			return $this->query( $this->driver->begin( ) );
		}


		/**
		 *
		 * Commit transaction
		 *
		 * @param	void
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		commit
		**/
		public function commit ( )
		{
			// Execute COMMIT SQL query from the driver right now
			return $this->query( $this->driver->commit( ) );
		}


		/**
		 *
		 * Rollback transaction
		 *
		 * @param	void
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		rollback
		**/
		public function rollback ( )
		{
			// Execute ROLLBACK SQL query from the driver right now
			return $this->query( $this->driver->rollback( ) );
		}


		/**
		 *
		 * Execute database query
		 *
		 * @param	SQL		$query	Database query
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		query
		**/
		public function query ( SQL $query )
		{
			// Compile query into the SQL string
			$query = $query->compile( $this->auth );

			// If the sql is not a string
			if ( !is_string( $query ) )
			{
				throw new Exceptions\DatabaseError("Failed to compile SQL query.");
			}


			// Create instance for new SQL event holder
			$this->history[] = new Event( $query );

			// If we are not connected
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Flush the memory before query
			$this->flush( );

			// if the query could not be executed
			$result = $this->driver->query( $query );

			// Finalize SQL event and sent stats numbers
			$this->event( )->done
			(
				$result,			// State of the query
				$this->numrows( ),	// Rows selected
				$this->affected( )	// Rows affected
			);


			return $result;
		}


		/**
		 *
		 * Parse results from query
		 *
		 * @param	int		$index		Select index of the row
		 * @return	Result
		 * @see		results
		**/
		public function results ( $index = null )
		{
			// If there are no rows
			if ( !$this->numrows( ) )
			{
				return array( );
			}


			// Access the results form the driver
			$rows = $this->driver->results( );

			// Assign results from the driver
			$this->event( )->result( $rows );

			// If specified index exists in the holder
			if ( $rows->offsetExists( $index ) )
			{
				return $rows->offsetGet( $index );
			}


			return $rows;
		}


		/**
		 *
		 * Number of selected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		numrows
		**/
		public function numrows ( )
		{
			// Get the count of gathered rows
			$numrows = $this->driver->numrows( );

			// If the query has failed
			if ( $numrows === false )
			{
				return false;
			}


			return (int) $numrows;
		}


		/**
		 *
		 * Number of affected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		affected
		**/
		public function affected ( )
		{
			// Get the count of affected rows
			$affected = $this->driver->affected( );

			// If we could not get number of affected rows
			if ( $affected === false )
			{
				return false;
			}


			return (int) $affected;
		}


		/**
		 *
		 * Get last insert Id
		 *
		 * @param	void
		 * @return	int
		 * @see		last_id
		**/
		public function last_id ( )
		{
			// If we are not connected
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Get the last insert Id from AI
			$id = $this->driver->last_id( );

			// If we have failed
			if ( $id === false )
			{
				return false;
			}


			return (int) $id;
		}


		/**
		 *
		 * Access authentication params
		 *
		 * @param	void
		 * @return	Security\Auth
		 * @see		auth
		**/
		public function auth ( )
		{
			return $this->auth;
		}


		/**
		 *
		 * Access SQL history
		 *
		 * @param	void
		 * @return	array
		 * @see		history
		**/
		public function history ( )
		{
			return $this->history;
		}


		/**
		 *
		 * Access last SQL event from history
		 *
		 * @param	int		$index	Event index
		 * @return	Event
		 * @see		event
		**/
		public function event ( $index = null )
		{
			// If there is no specified index
			if ( is_null( $index ) )
			{
				return end( $this->history );
			}

			// If there is no such event on specified index
			if ( !array_key_exists( $index, $this->history( ) ) )
			{
				return false;
			}


			// Return specific event by index
			return $this->history[ $index ];
		}


		/**
		 *
		 * Frees memory from last result
		 *
		 * @param	void
		 * @return	bool
		 * @see		flush
		**/
		public function flush ( )
		{
			// If we are not connected
			if ( !$this->connected( ) )
			{
				return false;
			}

			// If we could not flush the memory
			if ( !$this->driver->flush( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Closes database connection
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		public function close ( )
		{
			// If we are not connected
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Free up memory
			$this->flush( );

			// If the connection could not be closed
			if ( !$this->driver->close( ) )
			{
				return false;
			}


			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

