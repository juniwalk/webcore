<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		Row library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	ROW LIBRARY


	namespace WebCore\DB\Result;


	use \WebCore\Core\Exceptions;


	class Row implements \ArrayAccess, \IteratorAggregate, \Countable
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	array	$params		Connection params
		 * @return	void
		 * @throws	DatabaseError
		 * @see		__construct
		**/
		public function __construct ( array $values )
		{
			// If there are no values
			if ( empty( $values ) )
			{
				throw new Exceptions\DatabaseError("Values for '{$this}' cannot be empty.");
			}

			// Iterate through each value in an array
			foreach ( $values as $key => $value )
			{
				// Save the value
				$this->$key = $value;
			}


			return null;
		}


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			return get_class( $this );
		}



		/**
		 *
		 * <<interface>> Get the count of the fields
		 *
		 * @param	void
		 * @return	int
		 * @see		count
		**/
		public function count ( )
		{
			return sizeof( (array) $this );
		}


		/**
		 *
		 * <<interface>> Access array iterator
		 *
		 * @param	void
		 * @return	ArrayIterator
		 * @see		getIterator
		**/
		public function getIterator ( )
		{
			return new \ArrayIterator( $this );
		}


		/**
		 *
		 * <<interface>> Set property value
		 *
		 * @param	string	$key	Property name
		 * @param	mixed	$value	Property value
		 * @return	void
		 * @see		offsetSet
		**/
		public function offsetSet ( $key, $value )
		{
			$this->$key = $value;
		}


		/**
		 *
		 * <<interface>> Get property value
		 *
		 * @param	string	$key	Property name
		 * @return	mixed
		 * @see		offsetGet
		**/
		public function offsetGet ( $key )
		{
			return $this->$key;
		}


		/**
		 *
		 * <<interface>> Does property exists?
		 *
		 * @param	string	$key	Property name
		 * @return	bool
		 * @see		offsetExists
		**/
		public function offsetExists ( $key )
		{
			return isset( $this->$key );
		}


		/**
		 *
		 * <<interface>> Unset property
		 *
		 * @param	string	$key	Property name
		 * @return	void
		 * @see		offsetUnset
		**/
		public function offsetUnset ( $key )
		{
			unset( $this->$key );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

