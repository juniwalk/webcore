<?php /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		Result library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	RESULT LIBRARY


	namespace WebCore\DB\Result;


	use \WebCore\Core\Exceptions;


	class Result implements \ArrayAccess, \IteratorAggregate, \Countable
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Holder
		**/
		protected $values	= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		public function __construct ( ) { }


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		public function __toString ( )
		{
			return get_class( $this );
		}


		/**
		 *
		 * <<magic>> Getter
		 *
		 * @param	string	$key	Index key
		 * @return	mixed
		 * @see		__get
		**/
		public function __get ( $key )
		{
			// If there is no such key in holder
			if ( !$this->offsetExists( $key ) )
			{
				throw new \Exception("Undefined property: WebCore\DB\Result\Result::\${$key}");
			}


			// Get the offset value from holder
			return $this->offsetGet( $key );
		}


		/**
		 *
		 * Get raw data
		 *
		 * @param	void
		 * @return	array
		 * @see		raw
		**/
		public function raw ( )
		{
			return $this->values;
		}


		/**
		 *
		 * Insert new row
		 *
		 * @param	\WebCore\DB\Result\Row	$row	Result row
		 * @return	void
		 * @see		Push
		**/
		public function push ( Row $row )
		{
			$this->values[ ] = $row;
		}



		/**
		 *
		 * Rewrite result keys using property data
		 *
		 * @param	string	$key	Property name
		 * @return	void
		 * @throws	Exception|InvalidArgument|DatabaseError
		 * @see		assoc
		**/
		public function assoc ( $key )
		{
			// Access first row for tests
			$row = current( $this->values );

			// If the property is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If the property does not exists in the row
			if ( !property_exists( $row, $key ) )
			{
				throw new \Exception("Undefined property: WebCore\DB\Result\Row::\${$key}");
			}

			// If the value is not scalar
			if ( !is_scalar( $row->$key ) )
			{
				throw new Exceptions\DatabaseError("Cannot associate result rows using '{$key}' - value is not scalar");
			}


			// Temporary holder
			$values = array( );

			// Iterate through each row in holder
			foreach ( $this->values as &$row )
			{
				// Save the row into new holder
				$values[ $row->$key ] = &$row;
			}


			// Assign new values holder
			$this->values = $values;
		}


		/**
		 *
		 * Rewrite result keys using property data
		 *
		 * @param	string	$key	Property name
		 * @return	void
		 * @throws	Exception|InvalidArgument|DatabaseError
		 * @see		group
		**/
		public function group ( $key )
		{
			// if there are no values
			if ( empty( $this->values ) )
			{
				return null;
			}


			// Access first row for tests
			$row = current( $this->values );

			// If the property is not a string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If the property does not exists in the row
			if ( !property_exists( $row, $key ) )
			{
				throw new \Exception("Undefined property: WebCore\DB\Result\Row::\${$key}");
			}

			// If the value is not scalar
			if ( !is_scalar( $row->$key ) )
			{
				throw new Exceptions\DatabaseError("Cannot associate result rows using '{$key}' - value is not scalar");
			}


			// Temporary holder
			$values = array( );

			// Iterate through each row in holder
			foreach ( $this->values as &$row )
			{
				// Save the row into new holder
				$values[ $row->$key ][ ] = &$row;
			}


			// Assign new values holder
			$this->values = $values;
		}


		/**
		 *
		 * Convert data into array
		 *
		 * @param	void
		 * @return	self
		 * @see		toArray
		**/
		public function toArray ( )
		{
			// Temporary holder
			$values = array( );

			// Iterate through each row in holder
			foreach ( $this->values as $row )
			{
				// Save the row into new holder
				$values[ ] = (array) $row;
			}


			// Assign new values holder
			$this->values = $values;

			return $this;
		}



		/**
		 *
		 * <<interface>> Get the count of the fields
		 *
		 * @param	void
		 * @return	int
		 * @see		count
		**/
		final public function count ( )
		{
			return sizeof( $this->values );
		}


		/**
		 *
		 * <<interface>> Access array iterator
		 *
		 * @param	void
		 * @return	ArrayIterator
		 * @see		getIterator
		**/
		final public function getIterator ( )
		{
			return new \ArrayIterator( $this->values );
		}


		/**
		 *
		 * <<interface>> Set property value
		 *
		 * @param	string	$key	Property name
		 * @param	mixed	$value	Property value
		 * @return	void
		 * @see		offsetSet
		**/
		final public function offsetSet ( $key, $value )
		{
			// If the key is null
			if ( is_null( $key ) )
			{
				// Push the value with created key
				$this->values[ ] = $value; return;
			}

			// If the key is not scalar
			if ( !is_scalar( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'scalar', $key, 1 );
			}


			// Insert the value by desired key
			$this->values[ $key ] = $value;
		}


		/**
		 *
		 * <<interface>> Get property value
		 *
		 * @param	string	$key	Property name
		 * @return	DB_Result_Row
		 * @see		offsetGet
		**/
		final public function offsetGet ( $key )
		{
			return $this->values[ $key ];
		}


		/**
		 *
		 * <<interface>> Does property exists?
		 *
		 * @param	string	$key	Property name
		 * @return	bool
		 * @see		offsetExists
		**/
		final public function offsetExists ( $key )
		{
			return array_key_exists( $key, $this->values );
		}


		/**
		 *
		 * <<interface>> Unset property
		 *
		 * @param	string	$key	Property name
		 * @return	void
		 * @see		offsetUnset
		**/
		final public function offsetUnset ( $key )
		{
			unset( $this->values[ $key ] );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

