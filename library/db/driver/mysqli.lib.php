<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		MySQLi library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	 MYSQLI LIBRARY


	namespace WebCore\DB\Driver;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB;
	use \WebCore\DB\Result;
	use \WebCore\Security;


	final class MySQLi extends Driver
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Database quotes
		**/
		const QUOTES		= "`%s`";



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	\WebCore\DB\Connector	$parent	Parent connector
		 * @param	\WebCore\Security\Auth	$auth	Connection params
		 * @return	void
		 * @throws	DatabaseError
		 * @see		__construct
		**/
		public function __construct ( DB\Connector &$parent, Security\Auth $auth )
		{
			// If the MySQLi extension is disabled
			if ( !extension_loaded('mysqli') )
			{
				throw new Exceptions\DatabaseError("PHP extension 'mysqli' is not loaded");
			}


			// Assign link to the parent object
			$this->parent = &$parent;

			// If the login is not valid
			if ( !$auth->validate( ) )
			{
				throw new Exceptions\DatabaseError("Invalid login credentials for connector");
			}


			// Access basic connection credentials
			$host = $auth->host;	// Server name
			$port = $auth->port;	// Server port
			$user = $auth->user;	// User name
			$pass = $auth->pass;	// Password
			$path = $auth->path;	// Database

			// Access the type of connection we will use
			if ( (bool) $auth->persistent )
			{
				// Set persistent flag
				$host = "p:{$host}";
			}


			// Initialize new connection to MySQL database using MySQLi
			$this->link = new \MySQLi( $host, $user, $pass, null, $port );

			// Try to select desired database on the server
			if ( !empty( $path ) AND !$this->database( $path ) )
			{
				throw new Exceptions\DatabaseError("Could not select '{$path}' database.");
			}


			// Set UTF-8 charset encoding
			$this->link->set_charset('utf8');

			return true;
		}


		/**
		 *
		 * Connection status
		 *
		 * @param	void
		 * @return	bool
		 * @see		connected
		**/
		public function connected ( )
		{
			// If the link is not an object
			if ( !is_object( $this->link ) )
			{
				return false;
			}


			// Ping the server to check the state
			return $this->link->ping( );
		}


		/**
		 *
		 * Select database
		 *
		 * @param	string	$name	Database name
		 * @return	bool
		 * @see		database
		**/
		public function database ( $name )
		{
			return $this->link->select_db( $name );
		}


		/**
		 *
		 * Check for any error
		 *
		 * @param	void
		 * @return	string
		 * @see		error
		**/
		public function error ( $error = null )
		{
			// Get last occured MySQLi error
			$code = $this->link->errno;
			$text = $this->link->error;

			// If there is no error
			if ( empty( $text ) )
			{
				return null;
			}


			// Return error with its code
			return parent::error("{$code}: {$text}");
		}


		/**
		 *
		 * Prepare statement
		 *
		 * @param	string	$statement	Statement name
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		prepare
		**/
		public function prepare ( $statement )
		{
			throw new Exceptions\NotImplemented( __METHOD__ );
		}


		/**
		 *
		 * Set statement parameter
		 *
		 * @param	string	$key	Parameter name
		 * @param	mixed	$value	Parameter value
		 * @param	int		$type	Parameter type
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		param
		**/
		public function param ( $key, $value, $type )
		{
			throw new Exceptions\NotImplemented( __METHOD__ );
		}


		/**
		 *
		 * Execute prepared statement
		 *
		 * @param	bool	$results	Get results response?
		 * @return	mixed
		 * @throws	InvalidArgument
		 * @see		execute
		**/
		public function execute ( $results = true )
		{
			throw new Exceptions\NotImplemented( __METHOD__ );
		}


		/**
		 *
		 * Begin transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		begin
		**/
		public function begin ( )
		{
			return DB\SQL::Set("START TRANSACTION;");
		}


		/**
		 *
		 * Commit transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		commit
		**/
		public function commit ( )
		{
			return DB\SQL::Set("COMMIT;");
		}


		/**
		 *
		 * Rollback transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		rollback
		**/
		public function rollback ( )
		{
			return DB\SQL::Set("ROLLBACK;");
		}



		/**
		 *
		 * Select database
		 *
		 * @param	string	$name	Database name
		 * @return	bool
		 * @throws	DB_Exception
		 * @see		database
		**/
		public function query ( $sql )
		{
			// If the sql is not a string
			if ( !is_string( $sql ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $sql, 1 );
			}


			// Send the query to the MySQLi database
			$this->result = $this->link->query( $sql );

			// If any error has occured
			if ( $this->error( ) )
			{
				return false;
			}

			// If the query has failed
			if ( $this->result === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Parse results from query
		 *
		 * @param	void
		 * @return	Result
		 * @see		results
		**/
		public function results ( )
		{
			// Prepare result holder
			$rows = new Result\Result( );

			do
			{
				// Fetch each row as an array and save it to holder
				while ( $row = $this->result->fetch_assoc( ) )
				{
					// Save row into holder as a Row
					$rows->push( new Result\Row( $row ) );
				}

				// If there are no more results stored
				if ( !$this->link->more_results( ) )
				{
					continue;
				}

				// If we could not switch to the next result
				if ( !$this->link->next_result( ) )
				{
					continue;
				}


				// Access the result stored in the holder
				$this->result = $this->link->store_result( );
			}


			// Do the cycle while there are results
			while ( $this->link->more_results( ) );

			return $rows;
		}


		/**
		 *
		 * Number of selected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		numrows
		**/
		public function numrows ( )
		{
			// If there are no results
			if ( !$this->result( ) )
			{
				return false;
			}


			// Return the count of selected rows
			return $this->result->num_rows;
		}


		/**
		 *
		 * Number of affected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		affected
		**/
		public function affected ( )
		{
			// If there are no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Return the count of affected rows
			return $this->link->affected_rows;
		}


		/**
		 *
		 * Get last insert Id
		 *
		 * @param	void
		 * @return	int
		 * @see		last_id
		**/
		public function last_id ( )
		{
			return $this->link->insert_id;
		}


		/**
		 *
		 * Frees memory from last result
		 *
		 * @param	void
		 * @return	bool
		 * @see		flush
		**/
		public function flush ( )
		{
			// If there is any result
			if ( $this->result( ) )
			{
				// Free it from the memory handler
				$this->result->free_result( );
			}

			// If htere is any statement
			if ( $this->statement( ) )
			{
				// Free it from the memory handler
				$this->statement->free_result(  );
			}


			// Free the result and statement
			$this->result = null;
			$this->statement = null;

			return true;
		}


		/**
		 *
		 * Closes database connection
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		public function close ( )
		{
			return $this->link->close( );
		}


		/**
		 *
		 * Checks last statement initialization
		 *
		 * @param	void
		 * @return	bool
		 * @see		statement
		**/
		public function statement ( )
		{
			return is_object( $this->statement );
		}


		/**
		 *
		 * Checks last query result
		 *
		 * @param	void
		 * @return	bool
		 * @see		result
		**/
		public function result ( )
		{
			return is_object( $this->result );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

