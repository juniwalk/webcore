<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		SQLite3 library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	 SQLITE3 LIBRARY


	namespace WebCore\DB\Driver;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB;
	use \WebCore\DB\Result;
	use \WebCore\Security;


	final class SQLite3 extends Driver
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Database quotes
		**/
		const QUOTES		= "`%s`";



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	\WebCore\DB\Connector	$parent	Parent connector
		 * @param	\WebCore\Security\Auth	$auth	Connection params
		 * @return	void
		 * @throws	DatabaseError
		 * @see		__construct
		**/
		public function __construct ( DB\Connector &$parent, Security\Auth $auth )
		{
			// Open database for read and write, optionaly create new one
			$flags = SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE;

			// If the SQLite3 extension is disabled
			if ( !extension_loaded('sqlite3') )
			{
				throw new Exceptions\DatabaseError("PHP extension 'sqlite3' is not loaded");
			}


			// Assign link to the parent object
			$this->parent = &$parent;

			// If the login is not valid
			if ( !$auth->validate( ) )
			{
				throw new Exceptions\DatabaseError("Invalid login credentials for connector");
			}


			// Try to open the SQLite3 database file
			$this->link = new \SQLite3( "{$auth->host}/{$auth->path}", $flags, $auth->pass );

			// If any error has occured
			if ( $this->error( ) )
			{
				return false;
			}

			// If we were unable to connect
			if ( !$this->connected( ) )
			{
				throw new Exceptions\DatabaseError("Could not connect to '{$this}' server");
			}


			return true;
		}


		/**
		 *
		 * Connection status
		 *
		 * @param	void
		 * @return	bool
		 * @see		connected
		**/
		public function connected ( )
		{
			return is_object( $this->link );
		}


		/**
		 *
		 * Select database
		 *
		 * @param	string	$path	Path to database file
		 * @return	bool
		 * @see		database
		**/
		public function database ( $path )
		{
			// If database name is not string
			if ( !is_string( $path ) )
			{
				return false;
			}


			// Try to open selected database
			$this->link = new \SQLite3( $path );

			// If any error has occured
			if ( $this->error( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Check for any error
		 *
		 * @param	void
		 * @return	string
		 * @see		error
		**/
		public function error ( $error = null )
		{
			// Get last occured SQLite3 error
			$code = $this->link->lastErrorCode( );
			$text = $this->link->lastErrorMsg( );

			// If there is no error
			if ( empty( $code ) )
			{
				return null;
			}


			// Return error with its code
			return parent::error("{$code}: {$text}");
		}


		/**
		 *
		 * Prepare statement
		 *
		 * @param	string	$statement	Statement name
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		prepare
		**/
		public function prepare ( $statement )
		{
			// If the prepare is wrong
			if ( !is_string( $statement ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $statement, 1 );
			}


			// Try to initialize statement call
			$this->statement = $this->link->prepare( $statement );

			// If any error has occured
			if ( $this->error( ) )
			{
				return false;
			}

			// If we could not initialize statement
			if ( !$this->statement( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Set statement parameter
		 *
		 * @param	string	$key	Parameter name
		 * @param	mixed	$value	Parameter value
		 * @param	int		$type	Parameter type
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		param
		**/
		public function param ( $key, $value, $type )
		{
			// Is value null?
			$null = false;

			// If the parameter is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If the type is not integer
			if ( !is_integer( $type ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'integer', $type, 3 );
			}

			// If there is no statement
			if ( !$this->statement( ) )
			{
				return false;
			}


			// Make sure there is a : in front
			$key = ":".ltrim( $key, ':' );

			// If the value is null
			if ( is_null( $value ) )
			{
				$null = true;
			}


			// Try to bind new value for prepared statement call
			$result = $this->statement->bindValue( $key, $value, $type );

			// If binding has failed
			if ( $result === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Execute prepared statement
		 *
		 * @param	bool	$results	Get results response?
		 * @return	mixed
		 * @throws	DB_Exception
		 * @see		execute
		**/
		public function execute ( $results = true )
		{
			// If there is no statement
			if ( !$this->statement( ) )
			{
				return false;
			}


			// Execute prepared statement on the SQLite3 file
			$this->result = $this->statement->execute( );

			// If there was an error
			if ( $this->error( ) )
			{
				return false;
			}

			// If the procedure failed
			if ( !$this->result( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Begin transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		begin
		**/
		public function begin ( )
		{
			return DB\SQL::Set("BEGIN TRANSACTION;");
		}


		/**
		 *
		 * Commit transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		commit
		**/
		public function commit ( )
		{
			return DB\SQL::Set("COMMIT TRANSACTION;");
		}


		/**
		 *
		 * Rollback transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		rollback
		**/
		public function rollback ( )
		{
			return DB\SQL::Set("ROLLBACK TRANSACTION;");
		}


		/**
		 *
		 * Select database
		 *
		 * @param	string	$name	Database name
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		database
		**/
		public function query ( $sql )
		{
			// If the sql is not a string
			if ( !is_string( $sql ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $sql, 1 );
			}

			// If the SQL is result-less
			if ( $this->is_exec( $sql ) )
			{
				// Execute the query without result
				return $this->link->exec( $sql );
			}


			// Send the query to the SQLite database
			$this->result = $this->link->query( $sql );

			// If any error has occured
			if ( $this->error( ) )
			{
				return false;
			}

			// If query has failed
			if ( !$this->result( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Parse results from query
		 *
		 * @param	void
		 * @return	Result
		 * @see		results
		**/
		public function results ( )
		{
			// Prepare result holder
			$rows = new Result\Result( );

			// Fetch each row as an array and save it to holder
			while ( $row = $this->result->fetchArray( SQLITE3_ASSOC ) )
			{
				// Save row into holder as a Row
				$rows->push( new Result\Row( $row ) );
			}


			return $rows;
		}


		/**
		 *
		 * Number of selected rows
		 *
		 * @param	void
		 * @return	bool
		 * @see		numrows
		**/
		public function numrows ( )
		{
			// If there are no results
			if ( !$this->result( ) )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Number of affected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		affected
		**/
		public function affected ( )
		{
			// If there are no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Return the count of affected rows
			return $this->link->changes( );
		}


		/**
		 *
		 * Get last insert Id
		 *
		 * @param	void
		 * @return	int
		 * @see		last_id
		**/
		public function last_id ( )
		{
			return $this->link->lastInsertRowID( );
		}


		/**
		 *
		 * Frees memory from last result
		 *
		 * @param	void
		 * @return	bool
		 * @see		flush
		**/
		public function flush ( )
		{
			$this->result = null;
			$this->statement = null;

			return true;
		}


		/**
		 *
		 * Closes MSSQL connection
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		public function close ( )
		{
			return $this->link->close( );
		}


		/**
		 *
		 * Checks last statement initialization
		 *
		 * @param	void
		 * @return	bool
		 * @see		statement
		**/
		public function statement ( )
		{
			return is_object( $this->statement );
		}


		/**
		 *
		 * Checks last query result
		 *
		 * @param	void
		 * @return	bool
		 * @see		result
		**/
		public function result ( )
		{
			return is_object( $this->result );
		}


		/**
		 *
		 * Is the SQL result-less?
		 *
		 * @param	string	$sql	Database query
		 * @return	bool
		 * @see		is_exec
		**/
		protected function is_exec ( $sql )
		{
			// Is current query result-less or it is not?
			return preg_match( '/^(create|insert|update|delete)/i', $sql );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

