<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		Driver library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	 DRIVER LIBRARY


	namespace WebCore\DB\Driver;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB;
	use \WebCore\Security;


	abstract class Driver
	{
		/**
		 * Version holder
		**/
		const VERSION			= 1;


		/**
		 * Parent holder
		**/
		protected $parent		= null;


		/**
		 * Resource holders
		**/
		protected $link			= null;
		protected $result		= null;
		protected $statement	= null;


		/**
		 * Properties
		**/
		public static $params	= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	\WebCore\DB\Connector	$parent	Parent connector
		 * @param	\WebCore\Security\Auth	$auth	Connection params
		 * @return	void
		 * @throws	DatabaseError
		 * @see		__construct
		**/
		abstract public function __construct ( DB\Connector &$parent, Security\Auth $auth );


		/**
		 *
		 * <<magic>> To String
		 *
		 * @param	void
		 * @return	string
		 * @see		__toString
		**/
		final public function __toString ( )
		{
			// Return name of the class
			return get_class( $this );
		}


		/**
		 *
		 * Connection status
		 *
		 * @param	void
		 * @return	bool
		 * @see		connected
		**/
		abstract public function connected ( );


		/**
		 *
		 * Select database
		 *
		 * @param	string	$name	Database name
		 * @return	bool
		 * @see		database
		**/
		abstract public function database ( $name );


		/**
		 *
		 * Check for any error
		 *
		 * @param	string	$error	Error string
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		error
		**/
		public function error ( $error )
		{
			// If we are not connected
			if ( !$this->connected( ) )
			{
				return false;
			}

			// If there is no error
			if ( empty( $error ) )
			{
				return false;
			}


			// Set the SQL error to the parent event
			$this->parent->event( )->error( $error );

			// If the Error class is loaded
			//if ( class_exists( "Error" ) )
			//{
				// Trigger error notification
			//	return Error::Query( $error );
			//}


			// Throw DB Exception with the message
			throw new Exceptions\DatabaseError( $error );
		}


		/**
		 *
		 * Prepare statement
		 *
		 * @param	string	$statement	Statement name
		 * @return	bool
		 * @throws	DatabaseError
		 * @see		prepare
		**/
		abstract public function prepare ( $statement );


		/**
		 *
		 * Set statement parameter
		 *
		 * @param	string	$key	Parameter name
		 * @param	mixed	$value	Parameter value
		 * @param	int		$type	Parameter type
		 * @return	bool
		 * @see		param
		**/
		abstract public function param ( $key, $value, $type );


		/**
		 *
		 * Execute prepared statement
		 *
		 * @param	bool	$results	Get results response?
		 * @return	mixed
		 * @throws	DatabaseError
		 * @see		execute
		**/
		abstract public function execute ( $results = true );


		/**
		 *
		 * Begin transaction
		 *
		 * @param	void
		 * @return	bool
		 * @see		begin
		**/
		abstract public function begin ( );


		/**
		 *
		 * Commit transaction
		 *
		 * @param	void
		 * @return	bool
		 * @see		commit
		**/
		abstract public function commit ( );


		/**
		 *
		 * Rollback transaction
		 *
		 * @param	void
		 * @return	bool
		 * @see		rollback
		**/
		abstract public function rollback ( );


		/**
		 *
		 * Execute database query
		 *
		 * @param	mixed	$sql	Database query
		 * @return	bool
		 * @see		query
		**/
		abstract public function query ( $sql );


		/**
		 *
		 * Parse results from query
		 *
		 * @param	void
		 * @return	array
		 * @see		results
		**/
		abstract public function results ( );


		/**
		 *
		 * Number of selected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		numrows
		**/
		abstract public function numrows ( );


		/**
		 *
		 * Number of affected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		affected
		**/
		abstract public function affected ( );


		/**
		 *
		 * Frees memory from last result
		 *
		 * @param	void
		 * @return	bool
		 * @see		flush
		**/
		abstract public function flush ( );


		/**
		 *
		 * Closes database connection
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		abstract public function close ( );


		/**
		 *
		 * Checks last statement initialization
		 *
		 * @param	void
		 * @return	bool
		 * @see		statement
		**/
		abstract public function statement ( );


		/**
		 *
		 * Checks last query result
		 *
		 * @param	void
		 * @return	bool
		 * @see		result
		**/
		abstract public function result ( );
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

