<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-6-18 13:59
	* @file:		MySQL library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	 MYSQL LIBRARY


	namespace WebCore\DB\Driver;


	use \WebCore\Core\Exceptions;
	use \WebCore\DB;
	use \WebCore\DB\Result;
	use \WebCore\Security;


	final class MySQL extends Driver
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Database quotes
		**/
		const QUOTES		= "`%s`";



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	\WebCore\DB\Connector	$parent	Parent connector
		 * @param	\WebCore\Security\Auth	$auth	Connection params
		 * @return	void
		 * @throws	DatabaseError
		 * @see		__construct
		**/
		public function __construct ( DB\Connector &$parent, Security\Auth $auth )
		{
			// If the MySQL extension is disabled
			if ( !extension_loaded('mysql') )
			{
				throw new Exceptions\DatabaseError("PHP extension 'mysql' is not loaded");
			}


			// Assign link to the parent object
			$this->parent = &$parent;

			// If the login is not valid
			if ( !$auth->validate( ) )
			{
				throw new Exceptions\DatabaseError("Invalid login credentials for connector");
			}


			// Access basic connection credentials
			$host = $auth->host;	// Server name
			$user = $auth->user;	// User name
			$pass = $auth->pass;	// Password
			$path = $auth->path;	// Database

			// If there is a server port defined
			if ( is_numeric( $auth->port ) )
			{
				// Add server port to the IP address
				$host = "{$host}:{$auth->port}";
			}


			// Access the type of connection we will use
			$persistent = (bool) $auth->persistent;

			// Switch the connection type
			switch ( $persistent )
			{
				// Persistent
				case true:

					// Try to connect to the MySQL server using provided credentials
					$this->link = mysql_pconnect( $host, $user, $pass, false );

				break;

				// Non-persistent
				case false:

					// Try to connect to the MySQL server using provided credentials
					$this->link = mysql_connect( $host, $user, $pass, true );

				break;
			}

			// If we were unable to connect
			if ( !$this->connected( ) )
			{
				throw new Exceptions\DatabaseError("Could not connect to '{$this}' server");
			}

			// Try to select desired database on the server
			if ( !empty( $path ) AND !$this->database( $path ) )
			{
				throw new Exceptions\DatabaseError("Could not select '{$path}' database.");
			}


			return true;
		}


		/**
		 *
		 * Connection status
		 *
		 * @param	void
		 * @return	bool
		 * @see		connected
		**/
		public function connected ( )
		{
			return is_resource( $this->link );
		}


		/**
		 *
		 * Select database
		 *
		 * @param	string	$name	Database name
		 * @return	bool
		 * @see		database
		**/
		public function database ( $name )
		{
			return mysql_select_db( $name, $this->link );
		}


		/**
		 *
		 * Check for any error
		 *
		 * @param	void
		 * @return	string
		 * @see		error
		**/
		public function error ( $error = null )
		{
			// Get last occured MySQL error
			$code = mysql_errno( $this->link );
			$text = mysql_error( $this->link );

			// If there is no error
			if ( empty( $text ) )
			{
				return null;
			}


			// Return error with its code
			return parent::error("{$code}: {$text}");
		}


		/**
		 *
		 * Prepare statement
		 *
		 * @param	string	$statement	Statement name
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		prepare
		**/
		public function prepare ( $statement )
		{
			throw new Exceptions\NotImplemented( __METHOD__ );
		}


		/**
		 *
		 * Set statement parameter
		 *
		 * @param	string	$key	Parameter name
		 * @param	mixed	$value	Parameter value
		 * @param	int		$type	Parameter type
		 * @return	bool
		 * @throws	InvalidArgument
		 * @see		param
		**/
		public function param ( $key, $value, $type )
		{
			throw new Exceptions\NotImplemented( __METHOD__ );
		}


		/**
		 *
		 * Execute prepared statement
		 *
		 * @param	bool	$results	Get results response?
		 * @return	mixed
		 * @throws	InvalidArgument
		 * @see		execute
		**/
		public function execute ( $results = true )
		{
			throw new Exceptions\NotImplemented( __METHOD__ );
		}


		/**
		 *
		 * Begin transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		begin
		**/
		public function begin ( )
		{
			return DB\SQL::Set("START TRANSACTION;");
		}


		/**
		 *
		 * Commit transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		commit
		**/
		public function commit ( )
		{
			return DB\SQL::Set("COMMIT;");
		}


		/**
		 *
		 * Rollback transaction
		 *
		 * @param	void
		 * @return	SQL
		 * @see		rollback
		**/
		public function rollback ( )
		{
			return DB\SQL::Set("ROLLBACK;");
		}


		/**
		 *
		 * Select database
		 *
		 * @param	string	$name	Database name
		 * @return	bool
		 * @throws	DB_Exception
		 * @see		database
		**/
		public function query ( $sql )
		{
			// If the sql is not a string
			if ( !is_string( $sql ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $sql, 1 );
			}


			// Send the query to the MySQL database
			$this->result = mysql_query( $sql, $this->link );

			// If any error has occured
			if ( $this->error( ) )
			{
				return false;
			}

			// If the query has failed
			if ( $this->result === false )
			{
				return false;
			}


			return true;
		}


		/**
		 *
		 * Parse results from query
		 *
		 * @param	void
		 * @return	Result
		 * @see		results
		**/
		public function results ( )
		{
			// Prepare result holder
			$rows = new Result\Result( );

			// Fetch each row as an array and save it to holder
			while ( $row = mysql_fetch_assoc( $this->result ) )
			{
				// Save row into holder as a Row
				$rows->push( new Result\Row( $row ) );
			}


			return $rows;
		}


		/**
		 *
		 * Number of selected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		numrows
		**/
		public function numrows ( )
		{
			// If there are no results
			if ( !$this->result( ) )
			{
				return false;
			}


			// Return the count of selected rows
			return mysql_num_rows( $this->result );
		}


		/**
		 *
		 * Number of affected rows
		 *
		 * @param	void
		 * @return	int
		 * @see		affected
		**/
		public function affected ( )
		{
			// If there are no connection
			if ( !$this->connected( ) )
			{
				return false;
			}


			// Return the count of affected rows
			return mysql_affected_rows( $this->link );
		}


		/**
		 *
		 * Get last insert Id
		 *
		 * @param	void
		 * @return	int
		 * @see		last_id
		**/
		public function last_id ( )
		{
			throw new Exceptions\NotImplemented( __METHOD__ );
		}


		/**
		 *
		 * Frees memory from last result
		 *
		 * @param	void
		 * @return	bool
		 * @see		flush
		**/
		public function flush ( )
		{
			// If there is not any result
			if ( !$this->result( ) )
			{
				return false;
			}


			return mysql_free_result( $this->result );
		}


		/**
		 *
		 * Closes database connection
		 *
		 * @param	void
		 * @return	bool
		 * @see		close
		**/
		public function close ( )
		{
			return mysql_close( $this->link );
		}


		/**
		 *
		 * Checks last statement initialization
		 *
		 * @param	void
		 * @return	bool
		 * @see		statement
		**/
		public function statement ( )
		{
			throw new Exceptions\NotImplemented( __METHOD__ );
		}


		/**
		 *
		 * Checks last query result
		 *
		 * @param	void
		 * @return	bool
		 * @see		result
		**/
		public function result ( )
		{
			return is_resource( $this->result );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

