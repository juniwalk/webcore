<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2012-8-30 13:35
	* @file:		WebCore library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	WEBCORE LIBRARY


	namespace WebCore\Debug;


	use \WebCore\Core;
	use \WebCore\Web;


	abstract class Profiler
	{
		/**
		 * Version holder
		**/
		const VERSION			= 2;


		/**
		 * Time holders
		**/
		private static $start	= array( );
		private static $ticks	= array( );
		private static $stop	= array( );



		/**
		 *
		 * <<magic>> Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		final private function __construct ( ) { }


		/**
		 *
		 * Start the Profiling
		 *
		 * @param	mixed	$name	Name of the profiling
		 * @return	void
		 * @see		Start
		**/
		public static function Start ( $name )
		{
			// If the timer has already started
			if ( !empty( self::$start[ $name ] ) )
			{
				return null;
			}

			// If the timer was stopped already
			if ( !empty( self::$stop[ $name ] ) )
			{
				return null;
			}


			// Save the start time into the holder
			self::$start[ $name ] = self::Time( );
		}


		/**
		 *
		 * Tick the profiler
		 *
		 * @param	void
		 * @return	int
		 * @see		tick
		**/
		public static function Tick ( $name, $message = null )
		{
			// Access web site instance
			$site = &Web\Site::Factory( );

			// If the timer has not started yet
			if ( empty( self::$start[ $name ] ) )
			{
				return false;
			}

			// If the timer was stopped already
			if ( !empty( self::$stop[ $name ] ) )
			{
				return null;
			}


			// Access current time and last tick
			$time = self::Time( );
			$tick = self::$start[ $name ];

			// If there already are another ticks
			if ( !empty( self::$ticks[ $name ] ) )
			{
				// Get the last tick time from holder
				$tick = end( self::$ticks[ $name ] );
			}


			// Save the time of the current tick
			self::$ticks[ $name ][] = $time;

			// Return the runtime script time
			$value = round( $time - $tick, 6 );

			// if there is tick message and vrbose is enabled
			if ( is_string( $message ) AND $site->verbose )
			{
				Debug::Dump(array( $message => $value ));
			}


			return $value;
		}


		/**
		 *
		 * Stop the profiler
		 *
		 * @param	void
		 * @return	bool
		 * @see		stop
		**/
		public static function Stop ( $name, $message = null )
		{
			// Access web site instance
			$site = &Web\Site::Factory( );

			// If the timer has not started yet
			if ( empty( self::$start[ $name ] ) )
			{
				return false;
			}

			// If the timer was stopped already
			if ( !empty( self::$stop[ $name ] ) )
			{
				return null;
			}


			// Save the stop time into the holder
			self::$stop[ $name ] = self::Time( );

			// Access the values of the Profiler
			$stop = self::$stop[ $name ];
			$start = self::$start[ $name ];

			// Return the runtime script time
			$value = round( $stop - $start, 6 );

			// if there is tick message and vrbose is enabled
			if ( is_string( $message ) AND $site->verbose )
			{
				Debug::Dump(array( $message => $value ));
			}


			return $value;
		}


		/**
		 *
		 * Current microtime
		 *
		 * @param	void
		 * @return	float
		 * @see		Time
		**/
		private static function Time ( )
		{
			// Return microtime as float
			return (float) microtime( true );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

