<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-8-29 14:46
	* @file:		Log library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	LOG LIBRARY


	namespace WebCore\Debug;


	use \WebCore\Core;
	use \WebCore\Data;
	use \WebCore\DB;
	use \WebCore\Exceptions;
	use \WebCore\User;
	use \WebCore\Web;


	final class Log
	{
		/**
		 * Version holder
		**/
		const VERSION		= 1;


		/**
		 * Properties
		**/
		private $id			= null;
		public $user		= null;
		public $task		= null;
		public $entry		= null;
		public $status		= null;
		public $notify		= null;
		private $host		= null;
		private $timestamp	= null;
		public $data		= null;



		/**
		 *
		 * Constructor
		 *
		 * @param	void
		 * @return	void
		 * @see		__construct
		**/
		private function __construct ( )
		{
			$this->user = User\Session::Factory( )->user;
			$this->host = Web\Request::Host( );
			$this->timestamp = (string) Core\Date::NOW( );
			$this->data = array( );
		}


		/**
		 *
		 * Factory method
		 *
		 * @param	string	$alias	Logger alias
		 * @return	self
		 * @see		Factory
		**/
		public static function &Factory ( $alias )
		{
			// Create instances holder
			static $logs = array( );

			// If there is no instance holder in the list
			if ( !array_key_exists( $alias, $logs ) )
			{
				// Create instance holder
				$logs[ $alias ] = null;
			}


			// Access the user instance
			$log = &$logs[ $alias ];

			// If there is no instance
			if ( is_null( $log ) )
			{
				// Create new instance
				$log = new self( );
			}


			return $log;
		}



		/**
		 *
		 * <<magic>> Getter
		 *
		 * @param	string	$key	User property
		 * @return	mixed
		 * @see		__get
		**/
		final public function __get ( $key )
		{
			// If property name is not string
			if ( !is_string( $key ) )
			{
				throw new Exceptions\InvalidArgument( __METHOD__, 'string', $key, 1 );
			}

			// If there is no such property of the User
			if ( !property_exists( $this, $key ) )
			{
				throw new Exceptions\UndefinedProp( $key, __CLASS__ );
			}


			return $this->$key;
		}


		/**
		 *
		 * Create copy of the object
		 *
		 * @param	void
		 * @return	array
		 * @see		copy
		**/
		final public function copy ( )
		{
			// Empty holder
			$copy = array( );

			// Iterate trough each property
			foreach ( $this as $key => $value )
			{
				$copy[ $key ] = $value;
			}


			return $copy;
		}


		/**
		 *
		 * Write log entry
		 *
		 * @param	void
		 * @return	bool
		 * @see		write
		**/
		public function write ( )
		{
			// Access the WebApp database
			$db = DB\DB::Get('webapp');

			// We failed to connect to DB
			if ( !$db->connected( ) )
			{
				return false;
			}


			// Make copy of the class
			$entry = $this->copy( );

			// If there are data prepared
			if ( !empty( $entry['data'] ) )
			{
				// Write data into the INI string
				$entry['data'] = Data\INI::Write( $this->data );
			}

			// If the data are not string
			if ( !is_string( $entry['data'] ) )
			{
				// Set them to null
				$entry['data'] = null;
			}


			// Build SQL query to insert new log entry into database
			$insert = DB\SQL::Set("INSERT INTO page_log ([#keys]) VALUES ([#values]);");
			$insert->data( $entry );

			// If the query failed to execute
			if ( !$db->query( $insert ) )
			{
				return false;
			}

			// if nothing was affected
			if ( !$db->affected( ) )
			{
				return false;
			}


			return true;
		}



		/**
		 *
		 * Clear the session
		 *
		 * @param	void
		 * @return	bool
		 * @see		clear
		**/
		protected function clear ( )
		{
			// Iterate trough each property
			foreach ( $this as &$value )
			{
				$value = null;
			}


			return true;
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

