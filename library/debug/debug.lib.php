<?php declare(encoding='UTF-8'); /**


	* @author:		Design Point (dpoint.cz) & Web7 (web7.cz)
	* @contact:		info@dpoint.cz
	* @created:		2013-2-4 9:07
	* @file:		Debug library
	* @copyright:	(c) 2013 Design Point & Web7


	*///////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////	DEBUG LIBRARY


	namespace WebCore\Debug;


	use \Tracy\Debugger;

	use \WebApp;
	use \WebCore\Web;


	abstract class Debug
	{
		/**
		 * Version holder
		**/
		const VERSION			= 2;



		/**
		 *
		 * Dump the value in debug mode
		 *
		 * @param	void
		 * @return	void
		 * @see		Dump
		**/
		public static function Dump ( )
		{
			// If debug mode is disabled
			if ( !self::Enabled( ) )
			{
				return null;
			}


			// Get the list of arguments
			$values = func_get_args( );

			// If there are no arguments
			if ( empty( $values ) )
			{
				return null;
			}


			// Render values
			foreach ( $values as $value )
			{
				//var_dump( $value );
				Debugger::Dump( $value );
			}
		}


		/**
		 *
		 * Write the value in debug mode
		 *
		 * @param	void
		 * @return	void
		 * @see		Write
		**/
		public static function Write ( )
		{
			// If debug mode is disabled
			if ( !self::Enabled( ) )
			{
				return null;
			}


			// Get the list of arguments
			$values = func_get_args( );

			// If there are no arguments
			if ( empty( $values ) )
			{
				return null;
			}


			// Render values
			echo "<pre>";
			call_user_func( "print_r", $values );
			echo "</pre>";
		}


		/**
		 *
		 * Output the values in debug mode and die
		 *
		 * @param	void
		 * @return	void
		 * @see		Shutdown
		**/
		public static function Shutdown ( )
		{
			// Call the renderer function passing the arguments and exit
			call_user_func_array( __CLASS__."::Dump", func_get_args( ) ); exit(0);
		}


		/**
		 *
		 * Is the Debug enabled?
		 *
		 * @param	bool	$override	Override request option?
		 * @return	bool
		 * @see		Enabled
		**/
		public static function Enabled ( $override = false )
		{
			// Access web site instance
			$site = &Web\Site::Factory( );

			// Id debug is enabled
			if ( $site->debug )
			{
				return true;
			}


			// Get the debug override from the query
			Web\Request::Query( 'debug', $debug, null );

			// If the debug has been overriden
			if ( (bool) $debug )
			{
				return true;
			}


			return false;
		}


		/**
		 *
		 * Enable debug mode
		 *
		 * @param	void
		 * @return	void
		 * @see		Enable
		**/
		public static function Enable ( )
		{
			// Enable debug in the Web\Site instance
			Web\Site::Factory( )->debug = true;
			//Debugger::Enable( Debugger::DEVELOPMENT );
		}


		/**
		 *
		 * Disable debug mode
		 *
		 * @param	void
		 * @return	void
		 * @see		Disable
		**/
		public static function Disable ( )
		{
			// Disable debug in the Web\Site instance
			Web\Site::Factory( )->debug = false;
			//Debugger::Enable( Debugger::PRODUCTION );
		}
	}


	////////////////////////////////	END OF CODE
	////////////////////////////////////////////////////////////////////////////////////

